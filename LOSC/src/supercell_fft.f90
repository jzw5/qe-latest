! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE supercell_fft 
    ! FFT type descriptor for k-sampled/supercell LOSC.
    ! Templated from exx_fft_create() subroutine in PW/src/exx.f90

    USE kinds,                      ONLY : DP
    USE noncollin_module,           ONLY : noncolin, npol
    USE io_global,                  ONLY : ionode, stdout

    USE fft_types,                  ONLY : fft_type_descriptor
    USE stick_base,                 ONLY : sticks_map, sticks_map_deallocate


    IMPLICIT NONE
    SAVE

    ! Might need to store some buffers for recalculating coefficients?

    ! Custom FFT grid
    TYPE (fft_type_descriptor)          :: dfftlosc
    LOGICAL                             :: supercell_fft_initialized = .FALSE.
    ! Parameters for G-vectors associated to dfftlosc
    !   glosc: G-vectors in the LOSC grid
    !   gglosc: |G|^2 in LOSC grid
    !   gstart_losc: = 2 if gglosc(1) = 1; else gstart_losc = 1
    !   npwlosc: Number of plane waves in (gamma-only) LOSC grid
    !   ngmlosc: Number of G-vectors in the LOSC grid
    !   ecutlosc: LOSC grid energy cutoff
    !REAL(DP), DIMENSION(:), POINTER     :: gglosc
    !REAL(DP), DIMENSION(:,:), POINTER   :: glosc
    REAL(DP), POINTER                   :: gglosc(:)
    REAL(DP), POINTER                   :: glosc(:,:)
    INTEGER                             :: gstart_losc, npwlosc, ngmlosc_g
    REAL(DP)                            :: ecutlosc

    ! Parallelization stuff (10/11 note: not sure I understand)
    !   ibnd_start (ibnd_end): band start (end) indices for bgrp parallelization
    !   ibnd_buff_start (ibnd_buff_end): buffer indices for bgrp parallelization
    INTEGER                             :: ibnd_start = 0, ibnd_end = 0 
    INTEGER                             :: ibnd_buff_start, ibnd_buff_end

    CONTAINS

    SUBROUTINE supercell_fft_create()
        ! Use Monkhorst-Pack sampled k-points to create an FFT grid, dfftlosc,
        ! on the associated Born-von Karman supercell.

        !use gvecw, only: gcutw, gkcut
        USE constants,              ONLY : tpi
        USE gvecw,                  ONLY : ecutwfc, gcutw, gkcut
        USE gvect,                  ONLY : ecutrho, ngm, g, gg, gstart, mill
        USE mp_world,               ONLY : world_comm
        USE control_flags,          ONLY : gamma_only
        USE gvect,                  ONLY : ecutrho
        USE recvec_subs,            ONLY : ggen, ggens
        USE pw2losc,                ONLY : nks, nkstot, xk
        USE sys,                    ONLY : nrdm, scnrdm, nkdm, nksz, spin_offset
        USE cell_base,              ONLY : at, bg, alat, tpiba2, omega
        USE sys,                    ONLY : atlosc, bglosc, alatlosc, tpiba2losc, omegalosc
        USE fft_base,               ONLY : smap
        USE fft_types,              ONLY : fft_type_init
        USE mp_bands,               ONLY : nproc_bgrp, intra_bgrp_comm, nyfft
        USE command_line_options,   ONLY : nmany_
            ! No band parallelization for now
        !USE klist,                  ONLY : nks, xk ! number kpts, their coords
        USE mp_pools,               ONLY : inter_pool_comm
        USE mp,                     ONLY : mp_max, mp_sum
        
        IMPLICIT NONE

        INTEGER                         :: ik, ngmlosc, i
        INTEGER, ALLOCATABLE            :: ig_l2glosc(:), mill_losc(:,:)
        INTEGER, EXTERNAL               :: n_plane_waves
        REAL(DP)                        :: gcut_losc, k1
        LOGICAL                         :: lpara

        integer :: idbg(10)

        idbg(1) = nproc_bgrp
        idbg(2) = nyfft

        IF (supercell_fft_initialized) RETURN

        !call set_supercell_base()

        ! Initialize custom LOSC grid

        ! Set up FFT descriptors, including parallelization variables
        !lpara = (nproc_bgrp > 1)  ! Currently assume no band parallelization
        lpara = .false. ! don't want parallel FFT right now

        dfftlosc%nr1    = scnrdm(1)
        dfftlosc%nr2    = scnrdm(2)
        dfftlosc%nr3    = scnrdm(3)

        ! In tpiba2losc units, gglosc = |G|^2 < gcut_losc
        gcut_losc = ecutrho/tpiba2losc

        CALL fft_type_init(dfft=dfftlosc, smap=smap, pers="rho", &
                           lgamma=gamma_only, lpara=lpara, &
                           comm=intra_bgrp_comm, at=atlosc, bg=bglosc, &
                           gcut_in=gcut_losc, nyfft=nyfft, nmany=nmany_)

        if( (dfftlosc%nr1.ne.scnrdm(1)) .and. (dfftlosc%nr2.ne.scnrdm(2))  &
            .and. (dfftlosc%nr3.ne.scnrdm(3)) )then
            write(6,*) 'ERR(supercell_fft_create): Unfolded FFT grid misaligned'
            write(6,*) 'Note: fft_type_init does not allow prime factors > 5'
            write(6,'(a,3(i4,2x))') 'k-mesh : ', nkdm(:)
            write(6,'(a,3(i4,2x))') 'UC grid: ', nrdm(:)
            write(6,'(a,3(i4,2x))') 'Wanted doubled: ', scnrdm(:)
            write(6,'(a,3(i4,2x))') 'Created: ', dfftlosc%nr1, dfftlosc%nr2, dfftlosc%nr3
            call errore('SUPERCELL_FFT','Unfolded FFT grid misaligned',1)
        end if

        ngmlosc_g = dfftlosc%ngm
        ngmlosc = dfftlosc%ngm
        gstart_losc = 2 !gstart

        ALLOCATE( glosc(3,dfftlosc%ngm) )
        ALLOCATE( gglosc(dfftlosc%ngm) )
        ALLOCATE( mill_losc(3,dfftlosc%ngm) )
        ALLOCATE(ig_l2glosc(dfftlosc%ngm) )

        write(6,'(/,4x,"LOSC unit cell info:",/)')
        write(6, '(3(5x," atlosc(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i, atlosc(1:3,i), i=1,3)
        write(6, '(3(5x," bglosc(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i, bglosc(1:3,i), i=1,3)
        WRITE(stdout,*) "alatlosc = ", alatlosc
        WRITE(stdout,*) "ngmlosc_g = ", ngmlosc_g
        WRITE(stdout,*) "ngmlosc = ", ngmlosc
        WRITE(stdout,*) "nr1,2,3 = ", dfftlosc%nr1, dfftlosc%nr2, dfftlosc%nr3
        WRITE(stdout,*) "nnr = ", dfftlosc%nnr
        write(6,*)

        CALL ggen( dfftp=dfftlosc, gamma_only=gamma_only, at=atlosc, &
                   bg=bglosc, gcutm=gcut_losc, ngm_g=ngmlosc_g, &
                   ngm=ngmlosc, g=glosc, gg=gglosc, mill=mill_losc, &
                   ig_l2g=ig_l2glosc, gstart=gstart_losc )
        
        DEALLOCATE(mill_losc)
        DEALLOCATE(ig_l2glosc)

        gstart_losc = gstart
        npwlosc = n_plane_waves(ecutwfc/tpiba2losc, nks, xk, glosc, ngmlosc)
        CALL mp_sum(ngmlosc_g, intra_bgrp_comm)
        
        ! Define clock labels, enabling the new FFT
        dfftlosc%rho_clock_label = 'dfftlosc'
        dfftlosc%wave_clock_label = 'dfftlosc_wave'

        WRITE( stdout, '(/5x,"LOSC grid: ",i8," G-vectors", 5x, &
        &   "FFT dimensions: (",i4,",",i4,",",i4,")")') ngmlosc_g, &
        &   dfftlosc%nr1, dfftlosc%nr2, dfftlosc%nr3

        ! Fix logical flag at the end
        supercell_fft_initialized = .TRUE.

    END SUBROUTINE supercell_fft_create


    SUBROUTINE supercell_fft_rho_r2g(rhor, rhog)
        ! use unfolded supercell FFT on rhor and store in rhog
        USE fft_interfaces, ONLY: fwfft
        ! Params
        COMPLEX(DP), INTENT(IN)    :: rhor(:)
        COMPLEX(DP), INTENT(INOUT) :: rhog(:) ! INOUT since not alloc'd here
        integer  :: ig

        CALL fwfft('Rho', rhor, dfftlosc )
        do ig = 1, dfftlosc%ngm
            rhog(ig) = rhor(dfftlosc%nl(ig))
        end do

    END SUBROUTINE supercell_fft_rho_r2g

END MODULE supercell_fft