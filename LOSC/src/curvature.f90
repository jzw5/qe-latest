! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

module curvature
  use kinds, only : dp
  use sys, only: nrdm, scnrdm, nkdm, nksz, nrsz, rsgr
  use sys, only: mcvec, mcvec1d, mmcvec1d
  use sys, only: mmcsz, mmcoff, pscshsz , pscshoff, pscsh
  use utils, only: losc_cart2frac, losc_frac2cart, losc_cart2frac_rec, &
                      losc_frac2cart_rec!, ulist
  use utils, only: mpmsz
  use wannier_density, only: losc_get_dwfn0

! easier way to map 3d -> 1d arrays (use a 3d pointer to the 1d array)
! https://stackoverflow.com/questions/12005254/

! @TODO: function for random xyz to closest grid coord 
!        (include mod super cell coord)
! @TODO: make the real space grid coords an array, this also means only need
!        to change non-cubic in 1 place

  implicit none
  save

  ! dirac_const = C_x * 2 / 3; 
  !    C_x = (3/4) * (6/pi)^(1/3); 
  !    Multiplied later by tau = 6 * (1 - 2**(-1/3))
  REAL(dp), EXTERNAL            :: get_clock
  real(dp), parameter           :: dirac_const = 0.620350490899400017_dp
  real(dp), parameter           :: dirac_xpow  = 2.0_dp / 3.0_dp
  real(dp), parameter           :: sqrt_pow    = 0.5_dp
  real(dp), parameter           :: rtol        = 1.0e-8_dp

    ! Use pointers for things to be calculated using MPI (to flatten to 1D)
  real(dp), pointer             :: kappam(:,:,:) ! (LOSC1) curvature matrix
  real(dp), target, allocatable :: kmtar(:)      ! kappam target
  real(dp), pointer             :: kappa2(:,:,:) ! LOSC2 curvature
  real(dp), target, allocatable :: k2tar(:)      ! kappa2 target
  real(dp), pointer             :: ovsqrt(:,:,:) ! LWF overlap (\rho_i\rho_j)^sqrt
  real(dp), target, allocatable :: ovstar(:)     ! ovsqrt target
  real(dp), pointer             :: kjm(:,:,:)    ! coulomb array pointer
  real(dp), target, allocatable :: kjtar(:)      ! coulomb target
  real(dp), pointer             :: kxm(:,:,:)    ! kappaX array pointer
  real(dp), target, allocatable :: kxtar(:)      ! kappaX target

  contains

  SUBROUTINE init_curvature(nwan, nuc)
    IMPLICIT NONE

    INTEGER, INTENT(IN)       :: nwan     ! number of Wannier functions
    INTEGER, INTENT(IN)       :: nuc      ! number of unit cells 
    INTEGER                   :: i1d      ! unfolded 1d size

    i1d = nwan * nwan * nuc

    ! LOSC2 curvature matrix
    ALLOCATE ( k2tar(i1d) )
    kappa2(1:nwan,1:nwan,1:nuc) => k2tar(1:i1d)

    ! Coulomb
    ALLOCATE ( kjtar(i1d) )
    kjtar(:) = 0.0_dp
    kjm(1:nwan,1:nwan,1:nuc) => kjtar(1:i1d)

    ! Exchange
    ALLOCATE ( kxtar(i1d) )
    kxm(1:nwan,1:nwan,1:nuc) => kxtar(1:i1d)
    kxtar(:) = 0.0_dp

    ! Overlap
    ALLOCATE ( ovstar(i1d) ) 
    ovsqrt(1:nwan,1:nwan,1:nuc) => ovstar(1:i1d)

    ! LOSC1 curvature matrix
    ALLOCATE ( kmtar(i1d) )
    kappam(1:nwan,1:nwan,1:nuc) => kmtar(1:i1d)

    RETURN
  END SUBROUTINE init_curvature 

  SUBROUTINE dealloc_curvature()
    IMPLICIT NONE

    kappam => NULL()
    IF (ALLOCATED(kmtar)) DEALLOCATE ( kmtar )
    kappa2 => NULL()
    IF (ALLOCATED(k2tar)) DEALLOCATE ( k2tar )
    kjm => NULL()
    IF (ALLOCATED(kjtar)) DEALLOCATE ( kjtar )
    kxm => NULL()
    IF (ALLOCATED(kxtar)) DEALLOCATE ( kxtar )
    ovsqrt => NULL()
    IF (ALLOCATED(ovstar)) DEALLOCATE ( ovstar )

    RETURN
  END SUBROUTINE dealloc_curvature

  SUBROUTINE compute_curvature(iprint, limdei)
    USE wan2losc,       ONLY : nwan
    USE sys,            ONLY : pscshsz, pscsh, pscshoff, limde, kctol
    USE occupation,     ONLY : print_diagonal_occupation
    ! Calculate LOSC1 and LOSC2 curvature

    IMPLICIT NONE

    INTEGER, INTENT(IN)     :: iprint             ! printing flag
    INTEGER, INTENT(IN)     :: limdei(2, nwan, nwan, pscshsz)
    REAL(dp)                :: zeta = 8.0_dp      ! smoothing factor for erf
    REAL(dp)                :: ovdiag, erfov, ercov, kapl1
    INTEGER                 :: ib0, ib1, isc1, i1
    LOGICAL                 :: ldecut, lmscut, lsccut

    ! LOSC1 curvature
    do isc1 = 1, pscshsz

        lmscut = mirror_cut( 1, 1, isc1 ) ! check UC symmetry cutoff

        if( (iprint.gt.0) .and. (.not.lmscut) ) then
          CALL print_diagonal_occupation(isc1)
        end if

        doKapPr1: do ib0 = 1, nwan
        doKapPr2: do ib1 = 1, nwan
          lsccut = scb2_cut( ib0, ib1, isc1 )
          lmscut = mirror_cut( ib0, ib1, isc1 )
          ldecut = abs(limde(ib0,ib1,isc1)) .gt. kctol
          ! Use mirror symmetry to get other half of unit cells and home cell
          if (.NOT. lmscut) then
              kappam(ib0,ib1,isc1) = kjm(ib0, ib1, isc1) - kxm(ib0, ib1, isc1)
          else
              i1 = pscshsz - isc1 + 1 ! mirror cell
              kjm(ib0,ib1,isc1) = kjm(ib1,ib0,i1)
              kxm(ib0,ib1,isc1) = kxm(ib1,ib0,i1)
              kappam(ib0,ib1,isc1) = kappam(ib1,ib0,i1)
              ovsqrt(ib0,ib1,isc1) = ovsqrt(ib1,ib0,i1)
              cycle doKapPr2
          end if

          CALL print_curvature_debug(iprint, ib0, ib1, isc1, lsccut, limdei)

        end do doKapPr2
        end do doKapPr1

      end do

      do isc1 = 1, pscshsz
          do ib0 = 1, nwan
          do ib1 = 1, nwan
              if( (isc1.eq.pscshoff) .and. (ib0.eq.ib1) )then
                  kappa2(ib0,ib0,isc1) = kappam(ib0,ib0,isc1)
              else
                ovdiag = sqrt(abs(kappam(ib1,ib1,pscshoff) * &
                                  kappam(ib0,ib0,pscshoff)))
                erfov =  erf(zeta * ovsqrt(ib1, ib0, isc1))
                ercov = erfc(zeta * ovsqrt(ib1, ib0, isc1))
                kapl1 = kappam(ib1, ib0, isc1)
                kappa2(ib1, ib0, isc1) = (erfov * ovdiag) + (ercov * kapl1)
              end if
          end do
          end do
      end do

      ! LOSC2 curvature correction
      
      RETURN
  END SUBROUTINE compute_curvature


  !@TODO: get the WRITE statement to work
  SUBROUTINE fill_coulomb(J_array)
    ! Fill the Coulomb array with J[rho_i, rho_j]
    ! Serial calculation
    USE wan2losc,     ONLY : nwan
    USE sys,          ONLY : ib2dwf, id2uc, pscshsz
    USE coulomb,      ONLY : kernel
    IMPLICIT NONE

    REAL(dp), INTENT(OUT), TARGET :: J_array(:,:,:)
    INTEGER               :: iw1, iw2, isc    ! DLWF and supercell indices
    INTEGER               :: i1d              ! 1d unfolded index

    CALL start_clock('kjmetric')

    IF (.NOT. ALLOCATED(kernel)) THEN
      CALL errore('LOSC', 'Coulomb not initialized!', -1)
    END IF

    WRITE(6,'(2x,a)',advance='no') 'Calculating KappaJ...'; call flush(6)
    WRITE(6,'(/,1x,i0,a)',advance='no') nwan**2, ' pairs, on pair: '; call flush(6)
    DO iw1 = 1, nwan
      DO iw2 = 1, nwan
        WRITE(6,'(1x,i0)',advance='no') iw2+(iw1-1)*nwan; CALL flush(6)
        IF( MOD(iw2+(iw1-1)*nwan,24).eq.0 ) WRITE(6,*) ! newline

        DO isc = 1, pscshsz
            i1d = iw2 + (iw1-1)*nwan + (isc-1)*nwan*nwan
            J_array(iw2, iw1, isc) = kjmetric(i1d)
        END DO

        ! Correct FFT phase shifts
        ib2dwf(:) = 0 
        id2uc(:) = 0
      END DO
    END DO

    WRITE(6,*) ; CALL stop_clock('kjmetric')
    WRITE(6, 9000 ) get_clock( 'LOSC' ) ! print clock
    9000 FORMAT(' total cpu time is ',F10.1,' secs' )

    RETURN
  END SUBROUTINE fill_coulomb 


  SUBROUTINE fill_exchange(X_array)
    USE wan2losc,     ONLY : nwan
    USE sys,          ONLY : pscshsz
    IMPLICIT NONE

    REAL(dp), INTENT(OUT), TARGET :: X_array(:,:,:)
    INTEGER               :: iw1, iw2, isc    ! DLWF and supercell indices
    INTEGER               :: i1d              ! 1d unfolded index

    CALL start_clock('kxmetric')

    WRITE(6,'(2x,a)',advance='no') 'Calculating KappaX...'; CALL flush(6)
    WRITE(6,'(/,1x,i0,a)',advance='no') nwan**2, ' pairs, on pair: '; CALL flush(6)

    DO iw1 = 1, nwan
      DO iw2 = 1, nwan
        WRITE(6,'(1x,i0)',advance='no') iw2+(iw1-1)*nwan; CALL flush(6)
        IF( MOD(iw2+(iw1-1)*nwan,24).eq.0 ) WRITE(6,*) ! newline
        DO isc = 1, pscshsz
            i1d = iw2 + (iw1-1)*nwan + (isc-1)*nwan*nwan
            X_array(iw2, iw1, isc) = kxmetric(i1d)
        END DO
      END DO
    END DO

    WRITE(6,*) ; CALL stop_clock('kxmetric')
    WRITE(6, 9000 ) get_clock( 'LOSC' ) ! print clock
    9000 FORMAT(' total cpu time is ',F10.1,' secs' )

    RETURN
  END SUBROUTINE fill_exchange


  SUBROUTINE fill_overlap(S_array)
    USE wan2losc,     ONLY : nwan
    USE sys,          ONLY : pscshsz
    IMPLICIT NONE

    REAL(dp), INTENT(OUT), TARGET :: S_array(:,:,:)
    INTEGER               :: iw1, iw2, isc    ! DLWF and supercell indices
    INTEGER               :: i1d              ! 1d unfolded index

    call start_clock('ovsmetric')

    write(6,'(2x,a)',advance='no') 'Calculating OvSqrt...'; call flush(6)
    write(6,'(/,1x,i0,a)',advance='no') nwan**2, ' pairs, on pair: '; call flush(6)

    do iw1 = 1, nwan
      do iw2 = 1, nwan
        write(6,'(1x,i0)',advance='no') iw2+(iw1-1)*nwan; call flush(6)
        if( MOD(iw2+(iw1-1)*nwan,24).eq.0 ) write(6,*) ! newline
        do isc = 1, pscshsz
            i1d = iw2 + (iw1-1)*nwan + (isc-1)*nwan*nwan
            S_array(iw2, iw1, isc) = ovsmetric(i1d)
        end do
      end do
    end do
    write(6,*) ; call stop_clock('ovsmetric')
    WRITE(6, 9000 ) get_clock( 'LOSC' ) ! print clock
    9000 FORMAT(' total cpu time is ',F10.1,' secs' )

    RETURN
  END SUBROUTINE fill_overlap 


  SUBROUTINE mpi_fill_curvature(J_array, X_array, S_array, iasz, iwsz)
    USE io_global,        ONLY : ionode
    USE wan2losc,         ONLY : nwan
    USE sys,              ONLY : pscshsz
    USE utils,            ONLY : mpmsz, mpi_queue, mpi_qcalc
    USE wannier_density,  ONLY : losc_alloc_dwf, losc_dealloc_dwf

    IMPLICIT NONE

    ! MPI uses flattened (1-D) curvature arrays
    REAL(dp), INTENT(OUT)   :: J_array(:), X_array(:), S_array(:)
    INTEGER, INTENT(IN)     :: iasz, iwsz   ! array and world sizes

    INTEGER                 :: i1           ! 1d array size

    ! Flattened array size, set MPI queue chunk size
    i1 = nwan*nwan*pscshsz
    mpmsz = nint ( real(iasz,dp) / real(iwsz,dp) * 0.25_dp )
    mpmsz = max( mpmsz, 2 )

    ! Calculate Coulomb part of curvature
    call start_clock('kjmetric')
    if ( ionode ) then
        write(6,'(5x,"Starting qcalc(kjmetric)")')
        call mpi_queue( iwsz, iasz, J_array )
        WRITE(6, 9000 ) get_clock( 'LOSC' ) ! print clock
    else
        ! probably won't work without setting ib2dwf(:) = id2uc(:) = 0 inside
        ! kjmetric!
        call mpi_qcalc( kjmetric )
    end if
    call stop_clock('kjmetric')

    ! Calculate exchange part of curvature
    call start_clock('kxmetric')
    if ( ionode ) then
        write(6,'(5x,"Starting qcalc(kxmetric)")')
        call mpi_queue( iwsz, iasz, X_array )
        WRITE(6, 9000 ) get_clock( 'LOSC' ) ! print clock
    else
        call losc_alloc_dwf(2)
        call mpi_qcalc( kxmetric )
    end if
    call stop_clock('kxmetric')

    ! Calculate sqrt overlap for LOSC2
    call start_clock('ovsmetric')
    if ( ionode ) then
        write(6,'(5x,"Starting qcalc(ovsmetric)")')
        call mpi_queue( iwsz, iasz, S_array )
        WRITE(6, 9000 ) get_clock( 'LOSC' ) ! print clock
    else
        call mpi_qcalc( ovsmetric )
    end if
    call stop_clock('ovsmetric')

    RETURN

    9000 FORMAT(' total cpu time is ',F10.1,' secs' )
  END SUBROUTINE mpi_fill_curvature


  function kjmetric(in1) result(ret)
    ! J(i,j,R) metric
    use sys, only: ikjtyp
    use coulomb, only: coulomb_planewave
    ! Params
    integer, intent(in) :: in1 ! indices
    ! Return
    real(dp) :: ret
    ! Locals
    integer   :: i3(3)

    ret = 0.0_dp
    if( .not.kap1to3(in1,i3) ) return

    ret = coulomb_planewave( i3(1), i3(2), i3(3), ikjtyp )
    
    return
  end function kjmetric


  function kxmetric(in1) result(ret)
    ! kappaX(i,j,R) metric
    use sys, only : losctau
    use overlap,  only : overlap_pow_pbc
    ! Params
    integer, intent(in) :: in1 ! indices
    ! Return
    real(dp) :: ret
    ! Locals
    integer   :: i3(3)

    ret = 0.0_dp
    if( .not.kap1to3(in1,i3) ) return

    ret = losctau * dirac_const * overlap_pow_pbc(i3(1), i3(2), i3(3), dirac_xpow)

    return
  end function kxmetric
      
      
  function ovsmetric(in1) result(ret)
    ! \int sqrt(\rho_i \rho_j)
      use overlap,    only : overlap_pow_pbc
      ! Params
      integer, intent(in) :: in1 ! indices
      ! Return
      real(dp) :: ret
      ! Locals
      integer   :: i3(3)
    
      ret = 0.0_dp
      if( .not.kap1to3(in1,i3) ) return
    
      ret = overlap_pow_pbc( i3(1), i3(2), i3(3), sqrt_pow )
    
      return
  end function ovsmetric


  SUBROUTINE closest_centers(cen1, cen2, iw1, iw2, iwu)
    ! Get correct relative centers by finding closest clone of iw2's center
    ! relative to iw1, then offset by iwu if present
    USE wan2losc,               ONLY : lwfcen
    USE sys,                    ONLY : pscsh
    USE wannier_density,        ONLY : closest_clone

    IMPLICIT NONE

    ! Parameters
    REAL(dp), INTENT(OUT)           :: cen1(3), cen2(3) ! relative centers
    INTEGER, INTENT(IN)             :: iw1, iw2         ! DLWF band 1, 2
    INTEGER, INTENT(IN), OPTIONAL   :: iwu              ! unit cell offset

    cen1(:) = lwfcen(:,iw1)
    cen2(:) = lwfcen(:,iw2)

    IF (iw1 .ne. iw2) THEN
        CALL closest_clone( cen1, cen2 )
    END IF

    IF (PRESENT(iwu)) THEN
        cen2(:) = cen2(:) + pscsh(:,iwu)
    END IF

    RETURN
END SUBROUTINE closest_centers


  function scb2_cut(iw1,iw2,iwu) result(ret)
      ! True if the two WF centers are more than SC/2 distance away in 
      ! any dimension. iwu index moves iw2 WF
      use wan2losc, only: lwfcen
      ! Params
      integer, intent(in) :: iw1, iw2 ! Wannier band index
      integer, intent(in) :: iwu      ! Wannier unit cell for iw2
      ! Locals
      logical  :: ret
      real(dp) :: cen1(3)
      real(dp) :: cen2(3)
      real(dp) :: c1m2(3)    ! relative centers: c1-c2

      call closest_centers(cen1, cen2, iw1, iw2, iwu)
      c1m2(1:3) = cen1(1:3) - cen2(1:3)
      c1m2(1:3) = abs( losc_cart2frac(c1m2) )  ! convert to abs fractional for comparison
      cen1(1:3) = 0.5_dp * real(nkdm(1:3),dp)  ! set cen1 to SC/2
      cen1(1:3) = (1.0_dp-1.0e-8_dp) * cen1(1:3) ! give some numerical tolerance

      ret = (c1m2(1).gt.cen1(1)) .or. &
            (c1m2(2).gt.cen1(2)) .or. (c1m2(3).gt.cen1(3))

      return
  end function scb2_cut


  function mirror_cut(iw1, iw2, iwu) result(ret)
      ! True if cutoff from mirror symmetry of curvature: 
      !    K[i,j,-R] = K[j,i,+R], where R index moves the 2nd index WF
      ! 1) Only do half of the possible R's, in flattened indices use the 
      !    rule only calculate for the half before the home cell.
      ! 2) At R=0 (home cell), this means matrix is symmetric, use the rule 
      !    only calculate for ib1 <= ib2

    USE sys,          ONLY :  pscshoff
      ! Params
      integer, intent(in) :: iw1, iw2 ! Wannier band index
      integer, intent(in) :: iwu      ! Wannier unit cell for iw2
      ! Locals
      logical  :: ret

      ret = (iwu.eq.pscshoff) .and. (iw1.gt.iw2) ! triangluar half of home cell
      ret = ret .or. (iwu.gt.pscshoff)           ! half of pscsh != home
      ret = .false. ! @DEBUG kappa symmetry off

      return
  end function mirror_cut


  function kap1to3(iin, i3o) result(ret)
  ! translate 1D index back to 3D (nwan, nwan, pscshsz)
  ! manages cutoffs by returning false. can cutoff from symmetry of R,
  ! limde (occupation coefficients too small), or relative centers > SC/2
    USE wan2losc,     ONLY : nwan
    USE sys,          ONLY : limde, kctol !, lwfcen !, nrdm
    implicit none
    ! Params
    integer, intent(in)   :: iin    ! 1D input index
    integer, intent(out)  :: i3o(3) ! 3D output index
    ! Return
    logical :: ret
    ! Locals
    logical  :: rcut       ! whether c1m2 > SC/2
    logical  :: lcut       ! limde cutoff 
    logical  :: mcut       ! mirror symm cutoff, k_{ij}^{R,0} = k_{ji}^{-R,0}
    integer  :: ld1, ld12  ! leading dims

    ret = .true.

    ld1  = nwan
    ld12 = nwan*nwan

    ! 1st index stays with R=0
    ! 2nd index moves with R (Wann unit cell)
    ! 3rd index is R, the unit cell (pscsh(:,:) 2nd index)
    i3o(3) = (iin-1) / ld12
    i3o(2) = ( (iin-1) - i3o(3)*ld12 ) / ld1
    i3o(1) = iin - i3o(3)*ld12 - i3o(2)*ld1 - 1
    i3o(1:3) = i3o(1:3) + 1

    rcut = scb2_cut( i3o(1), i3o(2), i3o(3) )            ! SC>2 cutoff
    !rcut = .false. ! @ DEBUG: ignore SC/2 cutoff

    lcut = limde(i3o(1),i3o(2),i3o(3)) .lt. kctol   ! limde cutoff (dE coefficients)
    !lcut = .false.   ! @DEBUG: ignore limde cutoff

    mcut = mirror_cut( i3o(1), i3o(2), i3o(3) )

    if ( lcut .or. rcut .or. mcut )then
        ret = .false.
    end if

    return
  end function kap1to3


  SUBROUTINE print_cutoffs(occupm)
    USE constants,          ONLY : tpi
    USE pw2losc,            ONLY : nks, xk
    USE wan2losc,           ONLY : nwan, u_wan
    USE sys,                ONLY : spin_offset, nksz
    USE sys,                ONLY : pscshsz, pscsh, pscshoff

    IMPLICIT NONE

    COMPLEX(dp), INTENT(IN)     :: occupm(nwan, nwan, pscshsz)  ! lambda_ij^RT

    INTEGER                     :: isc1, ik, i1, ib1, ib2
    REAL(dp)                    :: r1, r2
    REAL(dp)                    :: rsc1(3)
    COMPLEX(dp)                 :: eikrmt

    write(6,'(2x,"-- kappam cutoff factors --")')
    write(6,'(5x,"unit cells: isc <R_1 R_2 R_3>")')
    do isc1 = 1, pscshsz
        rsc1(1:3) = pscsh(1:3,isc1)
        write(6,'(5x,i6,1x,"<",2(f8.4,1x),f8.4,">")') isc1, rsc1(1:3)
    end do

    do ik = 1 + spin_offset, nksz + spin_offset

        write(6,1381) ik, xk(1:3,ik)
        write(6,'(5x,3(a6,1x),a)') "deband", "iband2", "iband1", "factors"

        do i1 = 1, nwan

            do ib1 = 1, nwan
            do ib2 = 1, nwan
                write(6,'(5x,3(i6,1x))',advance='no') i1, ib2, ib1
                do isc1 = 1, pscshsz

                    if ( (ib1.eq.ib2) .and. (isc1.eq.pscshoff) ) then
                        r2 = abs( u_wan(i1,ib1,ik)**2 ) * &
                             ( 0.5_dp - occupm(ib1,ib1,isc1) )
                    else
                        r1 = tpi * sum( xk(1:3,ik) * pscsh(1:3,isc1) )
                        eikrmt = cmplx(cos(r1), sin(r1))
                        r2 = real( eikrmt * conjg(u_wan(ib1,i1,ik)) * &
                               u_wan(ib2,i1,ik) * occupm(ib2,ib1,isc1) )
                    end if

                    write(6,'(g14.7,1x)',advance='no') r2
                end do
                write(6,'()') ! newline
            end do
            end do

        end do
    end do

    RETURN

    1381 format(/,5x,"Output for kpt ", i4,",   k = <",2(F7.4,1x),f7.4,">")
  END SUBROUTINE print_cutoffs

  SUBROUTINE print_curvature_debug(iprint, ib0, ib1, isc, lsccut, limdei)
    USE wan2losc,     ONLY : nwan
    USE sys,          ONLY : pscshsz, pscshoff, limde

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: iprint           ! print verbosity
    INTEGER, INTENT(IN)   :: ib0, ib1, isc    ! band and supercell offset index
    LOGICAL, INTENT(IN)   :: lsccut           ! beyond supercell/2 cutoff?
    INTEGER, INTENT(IN)   :: limdei(2, nwan, nwan, pscshsz)

    IF ( iprint .le. 0 ) RETURN

    IF ( (isc.eq.pscshoff) .and. (ib0.eq.ib1) ) then
        WRITE(6,1395) ib1, ib0, kjm(ib0, ib1, isc), kxm(ib0, ib1, isc), &
                      limde(ib0,ib1,isc), limdei(1:2,ib0,ib1,isc), lsccut
    ELSE
        WRITE(6,1396) ib1, ib0, kjm(ib0, ib1, isc), kxm(ib0, ib1, isc), &
                      limde(ib0,ib1,isc), limdei(1:2,ib0,ib1,isc), lsccut
    END IF
    
    1395 FORMAT(3x,'* ',i6,1x,i6,2x,2(g20.12,1x),g10.3,1x,2(i4,1x),l)
    1396 FORMAT(5x,i6,1x,i6,2x,2(g20.12,1x),g10.3,1x,2(i4,1x),l)
  END SUBROUTINE print_curvature_debug

end module curvature