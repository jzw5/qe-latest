! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE wannier_density
    ! Functions and subroutines handling the Wannier function density.

    USE kinds,              ONLY : dp

    IMPLICIT NONE

    ! Scratch file name seed
    CHARACTER(LEN=64)   :: scrfil

    CONTAINS

    SUBROUTINE construct_dlwf(wffil, odir)
        ! Save the real-space dually localized Wannier function (DLWF) densities
        !   \rho_i^R(r) to disk
        USE constants,      ONLY : tpi
        USE cell_base,      ONLY : omega
        USE fft_base,       ONLY : dffts

        ! I/O modules
        USE io_files,       ONLY : prefix, diropn, nwordwfc, iunwfc
        USE io_global,      ONLY : ionode 

        ! Bloch wavefunctions
        USE wavefunctions,  ONLY : evc, deallocate_wavefunctions

        ! LOSC modules
        USE pw2losc,        ONLY : nbnd, nspin, isk
        USE pw2losc,        ONLY : ngk, npwx, xk, nks, nkstot
        USE wan2losc,       ONLY : u_loc, lwfcen, seedname, nwan, nbwan
        USE sys,            ONLY : ikjtyp, nrsz, rsgr, mcvec1d, dis_bndi
        use sys,            ONLY : nksz, spin_offset

        IMPLICIT NONE

        ! Externals
        INTEGER,            EXTERNAL :: find_free_unit

        ! Parameters
        CHARACTER(LEN=256), INTENT(IN)  :: odir     ! QE ouput directory
        CHARACTER(LEN=64),  INTENT(IN)  :: wffil    ! scr file for wavefunction seedname

        ! Locals
        CHARACTER(LEN=256) :: filename    ! for opening files
        INTEGER            :: lrwfcr      ! length of wfcr
        INTEGER            :: oun, iuwfcr, ios ! input unit: wfcr
        LOGICAL            :: exst        ! exist

        CHARACTER(LEN=8)   :: date
        INTEGER            :: values(8)

        INTEGER   :: is, isoff
        INTEGER   :: ir1, ik, ig, iuc
        INTEGER   :: ib1, ib2, ii1, ii2
        INTEGER   :: npw
        INTEGER   :: inr1, inr12
        INTEGER   :: nr3(3)

        COMPLEX(dp) :: eikr
        REAL(dp)    :: r1, rwf, iwf, rnorm

        INTEGER,     ALLOCATABLE :: ucol(:)    ! indices of non-zero u_loc elements
        COMPLEX(dp), ALLOCATABLE :: utmp(:)    ! non-zero elements of u_loc
        COMPLEX(dp), ALLOCATABLE :: evc_lok(:) ! loc. u_nk in G-space
        COMPLEX(dp), ALLOCATABLE :: unkgr(:,:) ! unk on grid with k fixed
        COMPLEX(dp), ALLOCATABLE :: clwf(:)    ! LWF for 1 unit cell (needed for fast calc)
        REAL(dp),    ALLOCATABLE :: dwfn(:,:)  ! density of LWF_n

        CALL start_clock('construct_dlwf')

        nr3(1) = dffts%nr1
        nr3(2) = dffts%nr2
        nr3(3) = dffts%nr3

        ALLOCATE( ucol(nbnd+1) )
        ALLOCATE( utmp(nbnd+1) )
        ALLOCATE( unkgr(nrsz,nwan) )
        ALLOCATE( evc_lok(npwx) )         ! local orbitals

        filename = 'wfc_r'
        iuwfcr = find_free_unit()
        lrwfcr = 2 * dffts%nr1x*dffts%nr2x*dffts%nr3x * nspin 
        exst = .FALSE.

        inr1 = dffts%nr1
        inr12 = inr1 * dffts%nr2

        CALL diropn (iuwfcr, filename, lrwfcr, exst)
        oun = find_free_unit()
        CALL date_and_time(DATE=date, VALUES=values)

        ! evc index 1 spin offset
        !is = 1 
        !isoff = (is - 1) * npwx 
        CALL start_clock('pw2losc:LBO')
        doEvcReadIk: DO ik = 1 + spin_offset, nksz + spin_offset
            isoff = (isk(ik) - 1) * npwx
            npw = ngk(ik)
            CALL davcio (evc, 2*nwordwfc, iunwfc, ik, -1) ! read wfc

            doEvcReadIb: DO ib1 = 1, nwan
            ! ib1 slowest index and not outermost, but it will
            ! be outermost later when evaluated the most
                evc_lok(:) = cmplx(0.0_dp)
                DO ib2 = 1, nbwan 
                    ii2 = dis_bndi(ib2)
                    DO ig = 1, npw !x
                        evc_lok(ig) = evc_lok(ig) + & 
                            u_loc(ib2, ib1, ik) * evc(ig + isoff, ii2)
                    END DO
                END DO

                CALL losc_init_lbo(evc_lok(:), unkgr(:,ib1), ik, ib1)

                WRITE(filename,'(2(a),i0.4,a)') trim(wffil), "_losc_lun", ib1, ".bin"
                IF( ik.eq.1 ) THEN
                    OPEN(oun, file=trim(filename), action='write', & 
                        form='unformatted', status='unknown', iostat=ios)
                    IF( ios.ne. 0)THEN
                        CALL errore('LOSC','error opening file '//trim(filename),3)
                    END IF
                ELSE
                    OPEN(oun, file=trim(filename), access='append', &
                        form='unformatted', status='old')
                END IF
                WRITE(oun) unkgr(:,ib1)
                CLOSE(oun)

            END DO doEvcReadIb

        END DO doEvcReadIk
        CALL stop_clock('pw2losc:LBO')
        
        !CALL deallocate_wavefunctions()
        DEALLOCATE( ucol )
        DEALLOCATE( utmp )
        DEALLOCATE( evc_lok )
        DEALLOCATE( unkgr )
        CLOSE(iuwfcr)


        CALL start_clock('pw2losc:SavD')
        ! Save LWF density to disk 
        ALLOCATE( dwfn(nrsz, nksz) )
        ALLOCATE( clwf(nrsz) )
        ALLOCATE( unkgr(nrsz,nksz) )

        doCalcLwfDens: DO ib1 = 1, nwan

            ! Get LWF ib1's unk on the grid
            CALL start_clock('read_unkgr')
            WRITE(filename,'(2(a),i0.4,a)') trim(wffil), "_losc_lun", ib1, ".bin"
            OPEN(oun, file=trim(filename), action='read', & 
                form='unformatted', status='unknown')
            DO ik = 1 + spin_offset, nksz + spin_offset
                READ(oun) unkgr(:,ik)
            END DO
            CLOSE(oun)
            CALL stop_clock('read_unkgr')

            CALL start_clock('make_DWF')
            rnorm = 0.0_dp
            doDwfUc: DO iuc = 1, nksz

                CALL start_clock('make_DWF1')
                ! use unkgr to fill in clwf
                clwf = cmplx(0.0_dp,kind=dp) 
                DO ik = 1 + spin_offset, nksz + spin_offset
                    DO ir1 = 1, nrsz
                        r1 = xk(1,ik) * (rsgr(1,ir1)+mcvec1d(1,iuc)) + &
                            xk(2,ik) * (rsgr(2,ir1)+mcvec1d(2,iuc)) + &
                            xk(3,ik) * (rsgr(3,ir1)+mcvec1d(3,iuc))
                        r1 = r1 * tpi
                        !eikr = cmplx( cos(r1), sin(r1) )
                        eikr = exp( cmplx(0.0_dp,r1,kind=dp) )
                        clwf(ir1) = clwf(ir1) + eikr * unkgr(ir1,ik)
                    END DO
                END DO
                CALL stop_clock('make_DWF1')

                CALL start_clock('make_DWF2')
                ! use clwf to fill in dwfn
                r1 = 1.0_dp / real(nksz,dp)
                DO ir1 = 1, nrsz
                    rwf = real(clwf(ir1)) * r1
                    iwf = aimag(clwf(ir1)) * r1
                    rwf = rwf * rwf
                    iwf = iwf * iwf
                    rwf = rwf + iwf
                    rnorm = rnorm + rwf
                    dwfn(ir1,iuc) = rwf ! + iwf
                END DO
                CALL stop_clock('make_DWF2')

            END DO doDwfUc

            ! re-norm dwfn to correct bad norm before, also corrects floating point errors
            rnorm = REAL(nrsz,dp) / ( rnorm * omega )
            WRITE(6,'(1x,"prenorm(",i0,")=",g20.12)') ib1, omega / (rnorm*real(nrsz,dp))
            dwfn(:,:) = dwfn(:,:) * rnorm
            CALL stop_clock('make_DWF')


            CALL start_clock('save_DWF')
            ! write LWF ib1's density on the grid
            WRITE(filename,'(2(a),i0.4,a)') trim(wffil), "_wannier_density", ib1, ".bin"
            OPEN(oun, file=trim(filename), action='write', & 
                form='unformatted', status='unknown')
            DO iuc = 1, nksz
                WRITE(oun) dwfn(:,iuc)
            END DO
            CLOSE(oun)
            CALL stop_clock('save_DWF')

        END DO doCalcLwfDens

        DEALLOCATE( unkgr )
        DEALLOCATE( clwf )
        DEALLOCATE( dwfn )

        CALL stop_clock('pw2losc:SavD')
        CALL stop_clock('construct_dlwf')
        RETURN
    END SUBROUTINE construct_dlwf 


    SUBROUTINE calc_local_bloch()
        ! Compute the localized Bloch orbitals, using the
        !   Bloch bands in evc(:,:) and the Wannier unitary u_loc(:,:)
    END SUBROUTINE calc_local_bloch


    FUNCTION wuc_map(iwu, iuc) RESULT(ret)
        ! get the reordered iuc for a given Wannier unit cell index
        USE sys,        ONLY : nkdm, mci, pscshoff, pscsh
        USE utils,      ONLY : losc_cart2frac
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: iwu ! Wannier unit cell index
        INTEGER, INTENT(IN) :: iuc ! desired unit cell in the unfolded supercell
        INTEGER  :: ret   ! return
        INTEGER  :: i3(3) ! 3d int 
  
        IF ( iwu.eq.pscshoff ) THEN ! no mapping
            ret = iuc
            RETURN 
        END IF 
  
        i3 = NINT( losc_cart2frac(pscsh(:,iwu)) ) ! get integer fractional coords 
        i3(1:3) = mci(1:3,iuc) - (i3(1:3)+1)      ! get offset with desired unit cell
        i3(1:3) = MODULO( i3(1:3), nkdm(1:3) )    ! move to home supercell
        ret = 1 + i3(1) + i3(2)*nkdm(1) + i3(3)*nkdm(1)*nkdm(2) ! flatten
  
        ! original method, less clear
        !ret =  1  + MODULO( i3(1), nkdm(1) ) ! move to home supercell & flatten
        !ret = ret + MODULO( i3(2), nkdm(2) )*nkdm(1)
        !ret = ret + MODULO( i3(3), nkdm(3) )*nkdm(1)*nkdm(2)
  
        RETURN 
    END FUNCTION wuc_map


    SUBROUTINE wan_to_home_cell(iprint)
      ! convert lwfcen to fractional and move to home cell
      USE kinds,        ONLY : DP
      USE constants,    ONLY : ANGSTROM_AU
      USE io_global,    ONLY : ionode
      USE cell_base,    ONLY : alat
      USE sys,          ONLY : nkdm
      USE wan2losc,     ONLY : nwan, nkwan, lwfcen
      USE utils,        ONLY : losc_cart2frac, losc_frac2cart
    
      IMPLICIT NONE

      ! Parameters
      INTEGER, INTENT(IN)   :: iprint   ! Print verbosity

      ! Locals
      INTEGER               :: ib1, i1
      REAL(dp)              :: xkw(3)
  
      DO ib1 = 1, nwan
        lwfcen(:,ib1) = lwfcen(:,ib1) * ANGSTROM_AU / alat
        ! @DEBUG print
        !if( ionode .and. iprint.gt.0)then
        !  write(6,'(6x,a, i4," frac: <",2(f12.5,1x),f12.5,">")') trim('X'), &
        !          ib1, losc_cart2frac(lwfcen(1:3,ib1))
        !end if

        ! move these to the 'home' supercell
        xkw = losc_cart2frac(lwfcen(:,ib1))
        do i1 = 1, 3
            !do while( xkw(i1) .lt. (-0.5_dp*nkdm(i1)) )
            do while( xkw(i1) .lt. (-0.0_dp*nkdm(i1)) )
                xkw(i1) = xkw(i1) + real(nkdm(i1),dp)
            end do
            !do while( xkw(i1) .gt. (1.5_dp*nkdm(i1)) )
            do while( xkw(i1) .gt. (1.0_dp*nkdm(i1)) )
                xkw(i1) = xkw(i1) - real(nkdm(i1),dp)
            end do
        end do
        lwfcen(:,ib1) = losc_frac2cart(xkw)

        ! @DEBUG print
        !IF (ionode .and. iprint >= 10) THEN
        !  write(6,'(6x,a, i4," frac: <",2(f12.5,1x),f12.5,">")') trim('X'), &
        !          ib1, losc_cart2frac(lwfcen(1:3,ib1))
        !  WRITE(6,'(6x,a, i4," cart: <",2(f12.5,1x),f12.5,">")') trim('X'), &
        !          ib1, lwfcen(1:3,ib1)
        !END IF
      END DO 
  
      RETURN
    END SUBROUTINE wan_to_home_cell


    SUBROUTINE closest_clone(cen1, cen2)
        ! For cen1, find the closest clone of cen2, clone being a supercell translation
        ! of cen2. This is allowed from the periodicity of the FFT. 
        USE sys,            ONLY : nkdm
        USE utils,          ONLY : losc_cart2frac, losc_frac2cart
        ! Params
        REAL(dp), INTENT(IN)    :: cen1(3) ! reference center, in cartesian units of alat
        REAL(dp), INTENT(INOUT) :: cen2(3) ! second center, same units as cen1
        ! Locals
        INTEGER  :: ii, imov       ! 3D iter, translation tracker
        REAL(dp) :: fc1(3), fc2(3) ! fractional coord centers
        REAL(dp) :: dmov           ! difference in centers
        
        fc1 = losc_cart2frac(cen1)
        fc2 = losc_cart2frac(cen2)
            
        DO ii = 1, 3
        
            dmov = fc2(ii) - fc1(ii)
        
            imov = 0
            DO WHILE( dmov .lt. -0.5_dp*REAL(nkdm(ii),dp) )
                imov = imov + 1
                dmov = dmov + real(nkdm(ii),dp)
            END DO
            DO WHILE( dmov .gt. 0.5_dp*REAL(nkdm(ii),dp) )
                imov = imov - 1
                dmov = dmov - real(nkdm(ii),dp)
            END DO
        
            IF( imov .ne. 0 ) THEN
                fc2(ii) = fc2(ii) + real(imov*nkdm(ii),dp)
            END IF
        END DO
        
        cen2 = losc_frac2cart(fc2)
  
        RETURN        
    end subroutine closest_clone


    SUBROUTINE losc_alloc_dwf(isz)
        USE wan2losc,       ONLY : nwan
        USE sys,            ONLY : ib2dwf, id2uc, idwfsz, nrsz, nksz
        USE sys,            ONLY: dwfn0, dwfft, afft
        USE supercell_fft,  ONLY: ngmlosc_g 
        IMPLICIT NONE
        ! Params
        INTEGER, INTENT(IN) :: isz
      
        IF( idwfsz .ne. isz ) THEN 
      
            CALL losc_dealloc_dwf()
      
            ALLOCATE( dwfn0( nrsz, nksz, isz ) ) ! real-space
            ALLOCATE( ib2dwf( nwan ) )
            ALLOCATE( id2uc( idwfsz ) )
      
            IF( ngmlosc_g.gt.0 ) THEN          ! FFT
              ALLOCATE( dwfft( ngmlosc_g, isz ))
              ALLOCATE( afft(8*nrsz*nksz) )
              !ALLOCATE( dwfft( dfftlosc%ngm, isz ))
              !ALLOCATE( afft(dfftlosc%nnr * nksz) )
      !        allocate( ib2ff(nwan) )
            END IF
      
            idwfsz = isz

        END IF
        ib2dwf(:) = 0                   ! ib2dwf(iwan) == 0: WF iwan not loaded
      !  ib2fft(:) = 0

        RETURN
    END SUBROUTINE losc_alloc_dwf


    SUBROUTINE losc_dealloc_dwf()
        USE sys, ONLY: dwfn0, dwfft, ib2dwf, id2uc, idwfsz, afft!, ib2fft
        IMPLICIT NONE
      
        IF( ALLOCATED(dwfn0) ) DEALLOCATE(dwfn0)   ! real-space data
        IF( ALLOCATED(ib2dwf) ) DEALLOCATE(ib2dwf)
        IF( ALLOCATED(id2uc) ) DEALLOCATE(id2uc)
      
        if( ALLOCATED(afft) ) DEALLOCATE(afft)     ! FFT data
        if( ALLOCATED(dwfft) ) DEALLOCATE(dwfft)
      !  if( allocated(ib2fft) ) deallocate(ib2fft)
      
        idwfsz = 0

        RETURN
    END SUBROUTINE losc_dealloc_dwf


    SUBROUTINE dwf_save_g()
        ! Save all Wannier orbitalets in reciprocal space to disk.
        !   Templated from pw2losc.f90's pw2losc_read() routine.
        !   Note: saves home-cell Wannier orbitalets only
        USE wan2losc,               ONLY : nwan
        USE sys,                    ONLY : dwfft
        USE supercell_fft,          ONLY : ngmlosc_g
        INTEGER, EXTERNAL               :: find_free_unit

        ! Params
        CHARACTER(len=256)              :: filename
        INTEGER                         :: oun, ibnd

        CALL start_clock("dwf_save_g")
        oun = find_free_unit()
        CALL losc_alloc_dwf(1)
        DO ibnd = 1, nwan
            CALL losc_load_dwf(ibnd, 1)     ! Load WF ibnd into dwfn0(:,:,1)
            CALL wannier_density_fft(1)    ! put into dwfft(:,1); home unit cell

            ! Save rho_{ibnd}^{0} = dwfft(:,1) to disk
            WRITE(filename,'(2(a),i0.4,a)') &
                TRIM(scrfil), "_wannier_density", ibnd, "_G.bin"
            OPEN(oun, FILE=TRIM(filename), ACTION='write', & 
                FORM='unformatted', STATUS='unknown')
            WRITE(oun) dwfft(:,1)
            CLOSE(oun)
        END DO
        CALL losc_dealloc_dwf()
        CALL stop_clock("dwf_save_g")

        RETURN
    END SUBROUTINE dwf_save_g


    SUBROUTINE dwf_shift_g(id, iwu)
        ! Convert dwfft(:,id) from rho^0(G) to rho^R(G) via the shift theorem:
        !   FT(rho(r-R)) = exp(-i*G*R) * rho(G)
        ! iwu is the UC offset index encoding R
        USE constants,          ONLY : tpi
        USE sys,                ONLY : dwfft, nkdm, id2uc , pscsh
        USE supercell_fft,      ONLY : glosc, ngmlosc_g

        INTEGER, INTENT(IN)         :: id, iwu
        
        INTEGER                     :: ib, ig
        REAL(dp)                    :: G, R
        REAL(dp)                    :: tpibnk(3)
        REAL(dp)                    :: GR
        REAL(dp)                    :: relshift(3)

        CALL start_clock("dwf_shift_g")
        tpibnk(:) = tpi / REAL(nkdm(:),dp)      ! Correct units

        ! Shift relative to already-loaded offset
        IF (iwu .EQ. id2uc(id)) THEN            ! already shifted
            CALL stop_clock("dwf_shift_g")
            RETURN
        END IF

        relshift = tpibnk(:) * ( pscsh(:,iwu) - pscsh(:,id2uc(id)) )
        GR = 0.0_dp
        !$OMP PARALLEL DO PRIVATE(GR)
        DO ig = 2, ngmlosc_g                                ! G = 0: no change
            GR = SUM(glosc(:,ig) * relshift(:))
            dwfft(ig,id) = dwfft(ig,id) * CMPLX(COS(GR), -SIN(GR), KIND=dp)
        END DO
        !$OMP END PARALLEL DO

        id2uc(id) = iwu

        CALL stop_clock("dwf_shift_g")

        RETURN
    END SUBROUTINE dwf_shift_g


    SUBROUTINE delete_binaries()
        USE utils,                      ONLY : mpioun
        USE wan2losc,                   ONLY : nwan
        IMPLICIT NONE
      
        CHARACTER(LEN=256)                  :: filename
        INTEGER                             :: iuwfcr, iband, ios
        INTEGER, EXTERNAL                   :: find_free_unit
        LOGICAL                             :: found
    
        WRITE(6,*) "DEBUG: delete_binaries has been called"
        iuwfcr = find_free_unit()
        do iband = 1, nwan
            write(filename,'(2(a),i0.4,a)') trim(scrfil), "_wannier_density", iband, ".bin"
            inquire(file=trim(filename), exist=found)
            if( found ) then
                open(iuwfcr, file=trim(filename), action='write', & 
                     form='unformatted', status='old',iostat=ios)
                if( ios.eq.0 )then
                    close(iuwfcr, status='delete', iostat=ios)
                else if( mpioun.gt.0 )then
                    write(mpioun,'("ERR(ionodescr) opening ",a," ios=",i0)') &
                          trim(filename), ios
                end if
            end if
        end do
      END SUBROUTINE delete_binaries


    SUBROUTINE dwf_delete(lplot)
        ! Delete plane-wave binary files
        USE wan2losc,               ONLY : nwan
        LOGICAL, INTENT(IN), OPTIONAL   :: lplot
        INTEGER, EXTERNAL               :: find_free_unit
        
        CHARACTER(len=256)              :: name_r, name_g, name_glosc, name_p, &
                                           name_gglosc, name_ngm, name_gstart
        INTEGER                         :: iwan, ioun
        LOGICAL                         :: lplot_

        lplot_ = .FALSE.
        IF (PRESENT(lplot)) lplot_ = lplot

        CALL start_clock("dwf_delete")
        ioun = find_free_unit()

        DO iwan = 1, nwan
            ! Real- and reciprocal-space orbitalet densities
            WRITE(name_r,'(2(a),i0.4,a)') &
                TRIM(scrfil), "_wannier_density", iwan, ".bin"
            WRITE(name_g,'(2(a),i0.4,a)') &
                TRIM(scrfil), "_wannier_density", iwan, "_G.bin"

            OPEN(ioun, FILE=TRIM(name_r), STATUS="unknown")
            CLOSE(ioun, STATUS="delete")
            OPEN(ioun, FILE=TRIM(name_g), STATUS="unknown")
            CLOSE(ioun, STATUS="delete")

            ! Localized u_nk for plotting LOSC band structure
            IF (.NOT. lplot_) THEN
                WRITE(name_p,'(2(a),i0.4,a)') &
                    TRIM(scrfil), "_losc_lun", iwan, ".bin"
                OPEN(ioun, FILE=TRIM(name_p), STATUS="unknown")
                CLOSE(ioun, STATUS="delete")
            END IF
        END DO

        ! Supercell FFT variables
        WRITE(name_ngm, '(2(a))') TRIM(scrfil), "_fft_ngmlosc_g.bin"
        WRITE(name_gstart, '(2(a))') TRIM(scrfil), "_fft_gstart_losc.bin"
        WRITE(name_glosc, '(2(a))') TRIM(scrfil), "_fft_glosc.bin"
        WRITE(name_gglosc, '(2(a))') TRIM(scrfil), "_fft_gglosc.bin"

        OPEN(ioun, FILE=TRIM(name_glosc), STATUS="unknown")
        CLOSE(ioun, STATUS="delete")
        OPEN(ioun, FILE=TRIM(name_gglosc), STATUS="unknown")
        CLOSE(ioun, STATUS="delete")
        OPEN(ioun, FILE=TRIM(name_ngm), STATUS="unknown")
        CLOSE(ioun, STATUS="delete")
        OPEN(ioun, FILE=TRIM(name_gstart), STATUS="unknown")
        CLOSE(ioun, STATUS="delete")
        CALL stop_clock("dwf_delete")

        RETURN
    END SUBROUTINE dwf_delete


    SUBROUTINE supercell_fft_write()
        ! Write dfftlosc parameters to disk (MPI hack)
        USE supercell_fft,     ONLY : dfftlosc, ngmlosc_g, glosc, gglosc, gstart_losc
        INTEGER, EXTERNAL     :: find_free_unit

        INTEGER               :: oun, ig
        CHARACTER(len=256)    :: name_ngm, name_gstart, name_g, name_gg

        oun = find_free_unit()

        WRITE(name_ngm, '(2(a))') TRIM(scrfil), "_fft_ngmlosc_g.bin"
        WRITE(name_gstart, '(2(a))') TRIM(scrfil), "_fft_gstart_losc.bin"
        WRITE(name_g, '(2(a))') TRIM(scrfil), "_fft_glosc.bin"
        WRITE(name_gg, '(2(a))') TRIM(scrfil), "_fft_gglosc.bin"

        OPEN(oun, FILE=TRIM(name_ngm), ACTION="write", &
             FORM="unformatted", STATUS="unknown")
        WRITE(oun) ngmlosc_g
        CLOSE(oun)

        OPEN(oun, FILE=TRIM(name_gstart), ACTION="write", &
             FORM="unformatted", STATUS="unknown")
        WRITE(oun) gstart_losc
        CLOSE(oun)

        OPEN(oun, FILE=TRIM(name_g), ACTION="write", &
             FORM="unformatted", STATUS="unknown")
        DO ig = 1, ngmlosc_g
            WRITE(oun) glosc(:,ig)
        END DO
        CLOSE(oun)

        OPEN(oun, FILE=TRIM(name_gg), ACTION="write", &
             FORM="unformatted", STATUS="unknown")
        WRITE(oun) gglosc(:)
        CLOSE(oun)
        
        RETURN
    END SUBROUTINE supercell_fft_write


    SUBROUTINE supercell_fft_read()
        ! Read dfftlosc parameters from disk for MPI
        USE supercell_fft,  ONLY : ngmlosc_g, glosc, gglosc, gstart_losc
        INTEGER, EXTERNAL       :: find_free_unit

        INTEGER                 :: iun, ig
        CHARACTER(len=256)      :: name_ngm, name_gstart, name_g, name_gg
        
        iun = find_free_unit()

        WRITE(name_ngm, '(2(a))') TRIM(scrfil), "_fft_ngmlosc_g.bin"
        WRITE(name_gstart, '(2(a))') TRIM(scrfil), "_fft_gstart_losc.bin"
        WRITE(name_g, '(2(a))') TRIM(scrfil), "_fft_glosc.bin"
        WRITE(name_gg, '(2(a))') TRIM(scrfil), "_fft_gglosc.bin"

        OPEN(iun, FILE=TRIM(name_ngm), ACTION="read", &
             FORM="unformatted", STATUS="unknown")
        READ(iun) ngmlosc_g
        CLOSE(iun)

        OPEN(iun, FILE=TRIM(name_gstart), ACTION="read", &
             FORM="unformatted", STATUS="unknown")
        READ(iun) gstart_losc
        CLOSE(iun)

        !IF (.NOT. ALLOCATED(glosc)) ALLOCATE(glosc(3,ngmlosc_g))
        !IF (.NOT. ALLOCATED(gglosc)) ALLOCATE(gglosc(ngmlosc_g))

        OPEN(iun, FILE=TRIM(name_g), ACTION="read", &
             FORM="unformatted", STATUS="unknown")
        DO ig = 1, ngmlosc_g
            READ(iun) glosc(:,ig)
        END DO
        CLOSE(iun)

        OPEN(iun, FILE=TRIM(name_gg), ACTION="read", &
             FORM="unformatted", STATUS="unknown")
        READ(iun) gglosc(:)
        CLOSE(iun)

        RETURN
    END SUBROUTINE supercell_fft_read


    FUNCTION losc_get_dwfn0(ibin, lG) RESULT(ret)
        USE pw2losc,            ONLY : nks
        USE wan2losc,           ONLY : nwan
        USE sys,                ONLY : dwfn0, ib2dwf, idwfsz
        IMPLICIT NONE
        ! Params
        INTEGER, INTENT(IN)             :: ibin     ! LWF band index
        LOGICAL, INTENT(IN), OPTIONAL   :: lG       ! real or reciprocal?
        ! Return
        INTEGER                         :: ret
        ! Locals
        INTEGER                         :: i1
        LOGICAL                         :: lG_

        lG_ = .FALSE.                               ! default: real grid
        IF (PRESENT(lG)) lG_ = lG
      
        IF( idwfsz.lt.1 ) THEN
            WRITE(6,*) 'ERR(losc_get_dwfn0): dwfn0 unallocated'
            CALL errore('LOSC', 'ERR(losc_get_dwfn0): dwfn0 unallocated', 2)
        END IF
      
        ret = ib2dwf(ibin)     ! find if already loaded
        IF( ret.gt.0 ) RETURN  ! if so return
      
        ! find lowest loaded index
        ret = 0
        DO i1 = 1, nwan
            IF( ib2dwf(i1).gt.ret ) ret = ib2dwf(i1)
        END DO
      
        IF( ret.eq.0 ) THEN
            ret = 1
        ELSE IF( ret.eq.idwfsz ) THEN
            ! find current band loaded in last dwfn0 index
            DO i1 = 1, nwan
                IF( ib2dwf(i1).eq.idwfsz ) THEN
                    ! change it to not loaded
                    ib2dwf(i1) = 0
                    EXIT 
                END IF
            END DO
        ELSE
            ret = ret + 1
        END IF
      
        ! set dwfn0 index in ib2dwf map
        ib2dwf(ibin) = ret
        IF (lG_) THEN
            CALL losc_load_dwf_g(ibin, ret)
        ELSE
            CALL losc_load_dwf(ibin,ret)
        END IF

        RETURN 
    END FUNCTION losc_get_dwfn0


    SUBROUTINE losc_get_dwf2(ib1, ib2, lG)
        ! return dwfn0 3rd dim index for 2 band index inputs
        USE pw2losc,        ONLY : nks
        USE wan2losc,       ONLY : nwan
        USE sys,            ONLY : dwfn0, ib2dwf, idwfsz
        IMPLICIT NONE
        ! Params
        INTEGER, INTENT(INOUT) :: ib1, ib2        ! LWF band index
        LOGICAL, INTENT(IN), OPTIONAL   :: lG     ! are we doing real or G grid?
        ! Return
        INTEGER   :: ret
        ! Locals
        INTEGER   :: ret1, ret2
        INTEGER   :: iun, i1, ios
        LOGICAL   :: lG_

        lG_ = .FALSE.
        IF (PRESENT(lG)) lG_ = lG
        
        IF( idwfsz.ne.2 ) THEN
            WRITE(6,*) 'ERR(losc_get_dwf2): dwfn0 wrong size'
            CALL errore('LOSC', 'ERR(losc_get_dwf2): dwfn0 wrong size', 2)
        END IF
        
        ret1 = ib2dwf(ib1)
        ret2 = ib2dwf(ib2)
        
        IF( (ret1.ne.0) .and. (ret2.ne.0) .and. (.not. lG_) ) THEN
            ib1 = ret1
            ib2 = ret2
            RETURN
        END IF
        
        IF( ret1.eq.0 ) THEN
            IF( ret2.eq.0 ) THEN
                ret1 = losc_get_dwfn0(ib1, lG_)
            ELSE ! replace the non-ib2 one
                ret1 = 1
                IF ( ret2.eq.1 ) ret1 = idwfsz
                DO i1 = 1, nwan
                    IF( ib2dwf(i1).eq.ret1 ) THEN
                        ib2dwf(i1) = 0
                        EXIT
                    END IF
                END DO
        
                ib2dwf(ib1) = ret1
                ! open and read dwfn0
                IF (lG_) THEN
                    CALL losc_load_dwf_g(ib1, ret1)
                ELSE
                    CALL losc_load_dwf(ib1,ret1)
                END IF
            END IF
        END IF
        
        IF( ret2.eq.0 ) THEN
            ret2 = 1
            IF ( ret1.eq.1 ) ret2 = idwfsz
            DO i1 = 1, nwan
                IF( ib2dwf(i1).eq.ret2 ) THEN
                    ib2dwf(i1) = 0
                    EXIT
                END IF
            END DO
        
            ib2dwf(ib2) = ret2
            ! open and read dwfn0
            IF (lG_) THEN
                CALL losc_load_dwf_g(ib2,ret2)
            ELSE
                CALL losc_load_dwf(ib2,ret2)
            END IF
        END IF

        ib1 = ret1
        ib2 = ret2
        RETURN
    END SUBROUTINE losc_get_dwf2


    SUBROUTINE losc_get_dwf2_g(ib1, ib2, iuc, id1, id2)
        ! Load planewave Wannier density representations into dwfft;
        !   band ib1 -> dwfft(:,id1), band ib2 -> dwfft(:,id2)
        ! Then, shift dwfft(:,ib2) by the unit cell offset iuc.
        USE sys,       ONLY : idwfsz, dwfft, ib2dwf, id2uc , pscshoff

        INTEGER, INTENT(IN)     :: ib1, ib2     ! band indices
        INTEGER, INTENT(IN)     :: iuc          ! unit cell index
        INTEGER, INTENT(INOUT)  :: id1, id2     ! dwfft(:,:) second index

        IF (idwfsz .NE. 2) THEN
            WRITE(6,*) "ERROR(losc_get_dwf2_g): idwfsz wrong size. Exiting..."
            RETURN
        END IF

        ! 2021-09-13: Needs logic to check what is already loaded
        id1 = 1
        id2 = 2

        IF ( (ib1 .EQ. ib2) .AND. (iuc .EQ. pscshoff) ) THEN
            ! diagonal in band and unit cell
            CALL losc_load_dwf_g(ib1, id1)
            id2 = id1
            id2uc(id2) = pscshoff
        ELSE IF ( ib1 .EQ. ib2 ) THEN
            ! diagonal in band, off-diagonal in unit cell
            CALL losc_load_dwf_g(ib1, id1)
            dwfft(:,id2) = dwfft(:, id1)
            CALL dwf_shift_g(id2, iuc)
        ELSE IF ( iuc .EQ. pscshoff ) THEN
            ! off-diagonal in band, diagonal in unit cell
            CALL losc_load_dwf_g(ib1, id1)
            CALL losc_load_dwf_g(ib2, id2)
        ELSE
            ! off-diagonal in band and unit cell
            CALL losc_load_dwf_g(ib1, id1)
            CALL losc_load_dwf_g(ib2, id2)
            CALL dwf_shift_g(id2, iuc)
        END IF

        RETURN

    END SUBROUTINE losc_get_dwf2_g


    SUBROUTINE losc_load_dwf(ib,id)
        ! load a band (ib) into dwfn0 3rd index (id)
        ! due to MPI weirdness, it will spin on trying to open the file 100 times
        ! before giving up
        USE constants,  ONLY : pi
        USE cell_base,  ONLY : alat
        USE pw2losc,    ONLY : nks, nkstot
        USE wan2losc,   ONLY : lwfcen
        USE sys,        ONLY: nrdm, nkdm, nksz, spin_offset, afft, dwfn0
        USE utils,      ONLY: losc_frac2cart, mpioun, nodenam
        
        IMPLICIT NONE

        ! Externals
        INTEGER,            EXTERNAL :: find_free_unit
        ! Params
        INTEGER, INTENT(IN) :: ib ! Wannier band
        INTEGER, INTENT(IN) :: id ! dwfn0 3rd index
        ! Locals
        INTEGER  :: iun, ios, i1  ! IO vars
        INTEGER  :: iuc, ir1      ! grid iters
        REAL(dp) :: r1, gpow, gnorm ! GTO vars
        REAL(dp) :: rwf, xp(3), gcen(3), scen(3)
        CHARACTER(LEN=256) :: filename    ! for opening files
        CHARACTER(LEN=256) :: charbuff

        REAL(dp)                    :: cens1(3), cens2(3)
        INTEGER, ALLOCATABLE        :: moms1(:,:), moms2(:,:)
        REAL(dp), ALLOCATABLE       :: pows1(:), wts1(:), pows2(:), wts2(:)

        REAL(dp), ALLOCATABLE       :: cens(:,:)
        INTEGER, ALLOCATABLE        :: moms(:,:)
        REAL(dp), ALLOCATABLE       :: pows(:), wts(:)

        INTEGER                     :: igto, n

        COMPLEX(dp), ALLOCATABLE    :: dwfsc(:)

        CALL start_clock("losc_load_dwf")

        iun = find_free_unit()
        WRITE(filename,'(2(a),i0.4,a)') trim(scrfil), "_wannier_density", ib, ".bin"
        ios = 1
        i1 = 0
        DO WHILE( (ios.ne.0) .and. (i1.lt.100) )
            i1 = i1 + 1
            OPEN(iun, file=trim(filename), action='read', & 
                form='unformatted', status='old', iostat=ios)
            IF( (ios.ne.0) .and. (mpioun.gt.0) ) THEN 
                WRITE(mpioun,'("WARN(get_dwf) open loop for ib ",i0," on ",a)')&
                    ib, trim(nodenam)
            END IF
        END DO

        IF( ios .ne. 0 ) THEN
            charbuff = 'ERR(losc_load_dwf)'//trim(filename)//' on '//trim(nodenam)
            CALL errore('LOSC', charbuff, abs(ios))
        END IF

        DO i1 = 1 + spin_offset, nksz + spin_offset
            READ(iun) dwfn0(:,i1,id)
        END DO
        CLOSE(iun)
    
        CALL stop_clock("losc_load_dwf")
    END SUBROUTINE losc_load_dwf

        
    SUBROUTINE losc_load_dwf_g(ibin, id)
        ! Read one Wannier orbitalet in reciprocal space from disk into memory
        USE sys,               ONLY : dwfft, id2uc , pscshoff
        USE utils,               ONLY : mpioun, nodenam
        INTEGER, EXTERNAL               :: find_free_unit

        INTEGER, INTENT(IN)             :: ibin         ! Wannier index
        INTEGER, INTENT(IN)             :: id           ! dwfft(:,id)

        CHARACTER(len=256)          :: filename, charbuff
        INTEGER                     :: iun, ios, i1


        CALL start_clock("losc_load_dwf_g")
        iun = find_free_unit()
        ios = 1
        i1 = 0
        WRITE(filename,'(2(a),i0.4,a)') &
            TRIM(scrfil), "_wannier_density", ibin, "_G.bin"
        
        ! Try and open with mpi
        DO WHILE( (ios.ne.0) .and. (i1.lt.100) )
            i1 = i1 + 1
            OPEN(iun, file=trim(filename), action='read', & 
                form='unformatted', status='old', iostat=ios)
            IF( (ios.ne.0) .and. (mpioun.gt.0) ) THEN 
                WRITE(mpioun,'("WARN(get_dwf) open loop for ib ",i0," on ",a)') &
                    ibin, trim(nodenam)
            END IF
        END DO
    
        IF( ios .ne. 0 ) THEN
            charbuff = 'ERR(losc_load_dwf_g)'//&
                trim(filename)//' on '//trim(nodenam)
            CALL errore('LOSC', charbuff, abs(ios))
        END IF
    
        READ(iun) dwfft(:,id)
        CLOSE(iun)

        id2uc(id) = pscshoff                 ! dwf_g saved as home UC

        CALL stop_clock("losc_load_dwf_g")
        RETURN
    END SUBROUTINE losc_load_dwf_g


    SUBROUTINE wannier_density_fft(idi,ido,iwu)
        ! wrapper for supercell_fft_rho_r2g using dwfn0 input and dwfft output
        ! if ido not supplied, use idi for dwfft 2nd index
        ! if iwu not supplied, use home cell
        USE sys, ONLY: dwfft, afft
        USE supercell_fft, ONLY: supercell_fft_rho_r2g 
        ! Params
        INTEGER,           INTENT(IN) :: idi ! FFT input, dwfn0 3rd index
        INTEGER, OPTIONAL, INTENT(IN) :: ido ! FFT output, dwfft 2nd index
        INTEGER, OPTIONAL, INTENT(IN) :: iwu ! Wannier unit cell
        ! Locals
        INTEGER  :: ig

        CALL start_clock("wannier_density_fft")
    
        IF( present(iwu) ) THEN
            CALL dwf_uc2sc( afft, idi, iwu )
        ELSE
            CALL dwf_uc2sc( afft, idi )
        END IF
    
        IF( present(ido) ) THEN
            CALL supercell_fft_rho_r2g( afft, dwfft(:,ido))
        ELSE
            CALL supercell_fft_rho_r2g( afft, dwfft(:,idi))
        END IF
    
        !ib2fft() can set here, but alone it won't track iwu...
        CALL stop_clock("wannier_density_fft")
        RETURN 
    END SUBROUTINE wannier_density_fft


    SUBROUTINE dwf_uc2sc(dwfsc, id, isc) 
        ! Map dwfn0(:,:,n) to a supercell Wannier density array dwfsc(:,n)
        !   (for fixed n)
        ! Modified from usc2sc.f90, by A. Mahler
        USE sys,     ONLY : nrdm, scnrdm, dwfn0, nkdm, nksz, nrsz, mci
        USE sys,     ONLY : pscsh, pscshoff
        IMPLICIT NONE
        ! Params
        COMPLEX(dp), ALLOCATABLE, INTENT(INOUT) :: dwfsc(:) ! output array
        INTEGER,                  INTENT(IN)    :: id       ! dwfn0 3rd index
        INTEGER,     OPTIONAL,    INTENT(IN)    :: isc      ! unit cell index
        ! Locals
        INTEGER                 :: ix, iy, iz       ! Coordinates in UC
        INTEGER                 :: isx, isy, isz    ! Coordinates in SC
        INTEGER                 :: iux, iuy, iuz    ! Unit cell indices (3D)
        INTEGER                 :: iug, isg, iuc    ! UC grid/SC grid/UC (1D)
        INTEGER                 :: ig1, ig12        ! UC grid leading dim.
        INTEGER                 :: uc1, uc12        ! UC index leading dim.
        INTEGER                 :: sg1, sg12        ! SC grid leading dim.
        INTEGER                 :: iuct, iscl       ! UC translated, iwu local
        ! Note: ix and isx are fast indices; iz and isz slow indices

        logical :: ltest
        ltest = .false.
        IF( PRESENT(isc) )THEN
            iscl = isc
        else
            iscl = pscshoff
        end if

        ! leading dimensions
        ig1 = nrdm(1)             ! unit cell grid
        ig12 = nrdm(1) * nrdm(2)
        uc1 = nkdm(1)             ! unit cells
        uc12 = nkdm(1) * nkdm(2)
        sg1 = ig1 * uc1           ! supercell grid
        sg12 = ig12 * uc12

        if( .not.ltest )then

        DO iuz = 1, nkdm(3)
        DO iuy = 1, nkdm(2)
        DO iux = 1, nkdm(1)
            ! Which unit cell are we in?
            iuc = iux + uc1 * (iuy - 1) + uc12 * (iuz - 1)
            iuct = wuc_map(iscl,iuc) ! translate for non-home cell

            DO iz = 1, nrdm(3)
                DO iy = 1, nrdm(2)
                    DO ix = 1, nrdm(1)
                        ! 1D grid index within the unit cell
                        iug = ix + ig1 * (iy - 1) + ig12 * (iz - 1)

                        ! 3D coordinates within supercell
                        isx = ix + nrdm(1) * (iux - 1)
                        isy = iy + nrdm(2) * (iuy - 1)
                        isz = iz + nrdm(3) * (iuz - 1)

                        ! 1D supercell grid index
                        isg = isx + sg1 * (isy - 1) + sg12 * (isz - 1)

                        ! Finally, set the respective array element
                        !dwfsc(isg) = dwfn0(iug,iuc,id)
                        dwfsc(isg) = dwfn0( iug, iuct, id)
                    END DO
                END DO
            END DO
        END DO
        END DO
        END DO

        else ! 0-based index test

        DO iuz = 0, nkdm(3)-1
        DO iuy = 0, nkdm(2)-1
        DO iux = 0, nkdm(1)-1
            ! Which unit cell are we in?
            iuc = iux + uc1*iuy + uc12*iuz + 1

            DO iz = 0, nrdm(3)-1
            DO iy = 0, nrdm(2)-1
            DO ix = 0, nrdm(1)-1
                ! 1D grid index within the unit cell
                iug = ix + ig1*iy + ig12*iz + 1

                ! 3D coordinates within supercell
                isx = ix + nrdm(1)*iux
                isy = iy + nrdm(2)*iuy
                isz = iz + nrdm(3)*iuz

                ! 1D supercell grid index
                isg = isx + sg1*isy + sg12*isz + 1
                if(isg.gt.nrsz*nksz)then
                    write(6,*) 'ERR dwfsc err lim'
                end if

                ! Finally, set the respective array element
                dwfsc(isg) = dwfn0(iug,iuc,id)
            END DO
            END DO
            END DO
        END DO
        END DO
        END DO

        end if

        RETURN
    END SUBROUTINE dwf_uc2sc


    SUBROUTINE dwf_sc2uc(dwfsc, id, isc) 
        ! Map dwfsc supercell mapping to unit cell mapping and store in dwfn0(:,:,id)
        USE sys,  ONLY : nrdm, scnrdm, dwfn0, nkdm, nksz, nrsz, pscshoff
        IMPLICIT NONE
        ! Params
        COMPLEX(dp), ALLOCATABLE, INTENT(IN) :: dwfsc(:) ! output array
        INTEGER,                  INTENT(IN) :: id       ! dwfn0 3rd index
        INTEGER,     OPTIONAL,    INTENT(IN) :: isc      ! unit cell index
        ! Locals
        INTEGER                 :: ix, iy, iz       ! Coordinates in UC
        INTEGER                 :: isx, isy, isz    ! Coordinates in SC
        INTEGER                 :: iux, iuy, iuz    ! Unit cell indices (3D)
        INTEGER                 :: iug, isg, iuc    ! UC grid/SC grid/UC (1D)
        INTEGER                 :: ig1, ig12        ! UC grid leading dim.
        INTEGER                 :: uc1, uc12        ! UC index leading dim.
        INTEGER                 :: sg1, sg12        ! SC grid leading dim.
        INTEGER                 :: iscl, iuct       ! isc local, iuc translated by isc
        ! Note: ix and isx are fast indices; iz and isz slow indices
    
    
        IF( PRESENT(isc) )THEN
            iscl = isc
        ELSE
            iscl = pscshoff
        END IF
    
        ! Sizes
        ig1 = nrdm(1)
        ig12 = nrdm(1) * nrdm(2)
    
        uc1 = nkdm(1)
        uc12 = nkdm(1) * nkdm(2)
    
        sg1 = ig1 * uc1
        sg12 = ig12 * uc12
    
        DO iuz = 1, nkdm(3)
        DO iuy = 1, nkdm(2)
        DO iux = 1, nkdm(1)
            ! Which unit cell are we in?
            iuc = iux + uc1 * (iuy - 1) + uc12 * (iuz - 1)
            iuct = wuc_map(iscl,iuc)
    
            DO iz = 1, nrdm(3)
                DO iy = 1, nrdm(2)
                    DO ix = 1, nrdm(1)
                        ! 1D grid index within the unit cell
                        iug = ix + ig1 * (iy - 1) + ig12 * (iz - 1)
    
                        ! 3D coordinates within supercell
                        isx = ix + nrdm(1) * (iux - 1)
                        isy = iy + nrdm(2) * (iuy - 1)
                        isz = iz + nrdm(3) * (iuz - 1)
    
                        ! 1D supercell grid index
                        isg = isx + sg1 * (isy - 1) + sg12 * (isz - 1)
    
                        ! Finally, set the respective array element
                        dwfn0( iug, iuct, id) = dwfsc(isg)
                    END DO
                END DO
            END DO
        END DO
        END DO
        END DO
    
        RETURN
    END SUBROUTINE dwf_sc2uc


    function exp_pos_pbc(ib1) result(ret)
        ! expectation of the position for PBC in fractional coordinates
        USE constants,  ONLY : tpi
        USE cell_base,  ONLY : omega, alat
        USE wan2losc,   ONLY : lwfcen
        USE pw2losc,    ONLY : nks, nkstot
        USE sys,        ONLY : dwfn0, nrsz, nkdm, rsgr, mcvec1d, nksz, spin_offset
        USE utils,      ONLY : losc_frac2cart

        ! Params
        integer, intent(in) :: ib1 !, ib2 ! LWF band index
        ! Return
        real(dp) :: ret(3)
        ! Locals
        integer  :: iuc, iuct, ir1, msc, id1
        real(dp) :: rd, cen1(3), cen2(3), gpow, scen(3)
        real(dp) :: expbc(3), ecos(3), esin(3), tpibl(3), th(3), goff(3)
        real(dp) :: xp(3), r2, r2int, cr2, sr2, dat, xrng(3,2), xpme(3), scc(3,3)

        scen(1:3) = 0.5_dp * nkdm(1:3)     ! supercell center (fractional)
        scen = losc_frac2cart(scen)
        dat = omega / (alat**3)
        tpibl(:) = tpi / nkdm(:) ! 2pi / SC periodicity, alat units

        id1 = losc_get_dwfn0(ib1)
        cen1 = lwfcen(:,ib1)
        call closest_clone(scen, cen1)

        ecos = 0.0_dp
        esin = 0.0_dp
        do iuc = 1, nksz
            iuct = iuc
            do ir1 = 1, nrsz
                xp = rsgr(:,ir1) + mcvec1d(:,iuc)
                th(:) = tpibl * xp(:)
                ecos(:) = ecos(:) + cos(th(:))*dwfn0(ir1,iuct,id1)
                esin(:) = esin(:) + sin(th(:))*dwfn0(ir1,iuct,id1)
            end do
        end do
        ecos = ecos * dat / real(nrsz,dp) ! use det(at) instead of omega for alat units
        esin = esin * dat / real(nrsz,dp)
        expbc = atan2(esin,ecos) / tpi * nkdm(:) ! expectation in cartesian units of alat
        call closest_clone(scen, expbc)
        ret = expbc
        !ret = ecos

        return
    end function exp_pos_pbc


    function exp_var_pbc(ib1) result(ret)
        ! expectation of the spatial variance for PBC in Bohr along primitive cell directions
        USE constants,  ONLY : tpi
        USE cell_base,  ONLY : omega, alat
        USE wan2losc,   ONLY : lwfcen
        USE pw2losc,    ONLY : nks, nkstot
        USE sys,        ONLY : ib2dwf, dwfn0, nrsz, nkdm, rsgr, mcvec1d, nksz
        USE utils,      ONLY : losc_frac2cart, losc_cart2frac
        ! Params
        integer, intent(in) :: ib1 !, ib2 ! LWF band index
        ! Return
        real(dp) :: ret(3) ! var in primitive cell directions 1,2,3
        ! Locals
        integer  :: iuc, iuct, ir1, msc, id1, ix
        real(dp) :: rd, cen1(3), cen2(3), gpow, scen(3)
        real(dp) :: expbc(3), ecos(3), esin(3), tpibl(3), th(3), goff(3)
        real(dp) :: xp(3), r2(3), r2int(3), cr2, sr2, dat, xrng(3,2), xpme(3), scc(3,3)

        id1 = losc_get_dwfn0(ib1)
        scen(1:3) = 0.5_dp * nkdm(1:3)     ! supercell center (fractional)
        scen = losc_frac2cart(scen)
        expbc = lwfcen(:,ib1)
        call closest_clone(scen,expbc)

        do ix = 1, 3
            xpme = 0.0_dp
            xpme(ix) = 1.0_dp * real(nkdm(ix),dp)
            scc(:,ix) = losc_frac2cart( xpme )
        end do

        r2int = 0.0_dp
        do iuc = 1, nksz
            iuct = iuc
            do ir1 = 1, nrsz
                xp = rsgr(:,ir1) + mcvec1d(:,iuc)
                xpme = losc_cart2frac(xp - expbc)
                do ix = 1, 3 ! this is crazy inefficient
                    if( abs(xpme(ix)) .gt. 0.5_dp*nkdm(ix) )then
                        xp(:) = xp(:) - sign(scc(:,ix),xpme(ix))
                    end if
                end do
                r2 = (xp-expbc)**2
                r2int = r2int + r2*dwfn0(ir1,iuct,id1)
            end do
        end do
        r2int = r2int * alat * alat
        r2int = r2int * omega / real(nrsz,dp)
        ret = r2int

        return 
    end function exp_var_pbc


    subroutine losc_init_lbo(evck, evcr, ik, ib)
        ! normalizes evck, FFT to real space & store in evcr
        ! optionally checks for phase stats (using directional stats)
          use gvect,     only : g
          use cell_base, ONLY : omega!, at, alat, bg !, tpiba2
          use fft_base,  only : dffts
          use pw2losc,   only : ngk
          implicit none
          ! Params
          complex(dp), intent(inout) :: evck(:) ! evc_lok @ an ik and ib
          complex(dp), intent(inout) :: evcr(:) ! evck FFT'd to real-space grid
          integer, intent(in) :: ik
          integer, optional, intent(in) :: ib
          ! Locals
          integer :: i1, ig, nrsz
          integer :: ibl ! locals of optionals
          real(dp) :: gnorm1, rnorm1
          real(dp) :: r1, mprev
          real(dp) :: rre, rim, rarg, rmag
          real(dp) :: rcos, rsin
          real(dp) :: amen, avar, astd ! arg stats (phase)
          real(dp) :: veden ! volume element density
          ! temps for evck norm on grid
    
          CALL start_clock( 'init_lbo' )
    
          nrsz = dffts%nnr
          veden = omega / real(nrsz,dp)
    
          ibl = 0
          if ( present(ib) ) ibl = ib
    
          r1 = 0.0_dp          ! normalize the LBO
          !do ig = 1, ngk(ik)
          !    r1 = r1 + abs(evck(ig))**2
          !end do
          gnorm1 = 1.0_dp !/ sqrt( omega * r1 ) 
          !*real(dffts%nnr,dp) ) ! * real(nks, dp) )
          !evck(1:ngk(ik)) = evck(1:ngk(ik)) * cmplx(gnorm1)
    
          ! transform on to FFT real grid
          call losc_wf_g2r(evck, evcr, ik)
    
          rnorm1 = 0.0_dp
          rcos = 0.0_dp
          rsin = 0.0_dp
    
          do i1 = 1, nrsz
    
              rnorm1 = rnorm1 + abs(evcr(i1))**2
              rmag = abs(evcr(i1))
              rre = real(evcr(i1))
              rim = aimag(evcr(i1))
              rcos = rcos + rre/rmag
              rsin = rsin + rim/rmag
    
          end do
          rcos = rcos / real(nrsz, dp)
          rsin = rsin / real(nrsz, dp)
          rmag = sqrt( rcos*rcos + rsin*rsin ) ! average vector length
          avar = 1.0_dp - rmag
          astd = sqrt( -2.0_dp * log(rmag) )
          amen = atan2( rsin, rcos )
    
          rnorm1 = 1.0_dp / ( sqrt(rnorm1) * veden )
          evcr(:) = evcr(:) * rnorm1
    
          CALL stop_clock( 'init_lbo' )
      end subroutine losc_init_lbo
    
    
      subroutine losc_wf_g2r(psig, psir, ik)
          USE fft_interfaces, ONLY : invfft
          use fft_base,       ONLY : dffts
          USE pw2losc,        ONLY : ngk, igk_k
          implicit none
          ! Params
          integer,     intent(in)    :: ik         ! k-point
          complex(dp), intent(in)    :: psig(:) ! wfc in PW basis
          complex(DP), intent(inout) :: psir(:) !dffts%nnr) ! 
          ! Locals
          integer :: ig
    
          CALL start_clock( 'losc_wf_g2r' )
    
          psir = cmplx(0.d0) ! perform the fourier transform
          do ig = 1, ngk(ik)
            psir(dffts%nl(igk_k(ig,ik))) = psig(ig)
          enddo
          CALL invfft('Wave', psir(:), dffts)
    
          CALL stop_clock( 'losc_wf_g2r' )
      end subroutine losc_wf_g2r


      SUBROUTINE print_wavefunction_info(iprint)
        USE io_files,    ONLY : prefix, tmp_dir, diropn, nwordwfc, iunwfc
        USE io_global,   ONLY : ionode 
        USE fft_base,    ONLY : dffts
        USE pw2losc,     ONLY : nspin, nkstot, nbnd, npwx

        IMPLICIT NONE

        ! Parameters
        INTEGER, INTENT(IN)                 :: iprint       ! Verbosity

        ! Locals
        INTEGER                             :: lrwfcr
        
        
        lrwfcr = 2 * dffts%nr1x*dffts%nr2x*dffts%nr3x * nspin

        WRITE(6,'(5x,2(A,1x))') 'prefix: ', trim(prefix)
        WRITE(6,'(5x,"iunwfc: ",i3,"")') iunwfc
        WRITE(6,'(5x,"length of wfc in real space/per band",i12)') &
                8 * nkstot * lrwfcr
        WRITE(6,'(5x,"length of 1 wfc on real space grid  ",i12)') lrwfcr
        WRITE(6,'(5x,"length of wfcs in G space           ",i12)') &
                2 * nbnd * npwx * nkstot * 8
        WRITE(6,'(5x,"nwordwfc                            ",i12)') nwordwfc

        RETURN
      END SUBROUTINE print_wavefunction_info

END MODULE wannier_density