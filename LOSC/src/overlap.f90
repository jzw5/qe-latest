! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE overlap
	! Methods for calculating real-space overlaps between Wannier densities:
	!       overlap(rho1, rho2, power) = \int\, dr [rho1(r) rho2(r)]^power

	USE kinds,				ONLY : dp

	IMPLICIT NONE

	CONTAINS

	function overlap_pow_pbc( ib1, ib2, isc, rpow ) result(ret)
	! overlap_pow_v9 rewritten to be periodic instead of treating them as aperiodic images
	! since dwfn0 is stored as a grid per unit cell, changing the wannier unit cell index
	! amounts to a reshuffling of the 2nd index in dwfn0
		use constants,        ONLY : tpi
    USE cell_base,        ONLY : at, omega
    USE pw2losc,          ONLY : nks, nkstot
    USE wan2losc,         ONLY : nwan, lwfcen
		use sys,              ONLY : dwfn0, nrsz, nksz, spin_offset, ib2dwf
		use wannier_density,  ONLY: losc_get_dwfn0, losc_get_dwf2, wuc_map
		implicit none
		! Params
		integer,  intent(in):: ib1, ib2 ! LWF band index
		integer,  intent(in):: isc      ! relative super cell index
		real(dp), intent(in):: rpow     ! power to overlap
		! Return
		real(dp) :: ret
		! Locals
		integer  :: iuc, iuct, ir1 ! UC iter, UC translated, grid iter
		integer  :: id1, id2
	
		ret = 0.0_dp
	
		if( ib1.eq.ib2 )then
	
			id1 = losc_get_dwfn0(ib1)
			id2 = id1
	
		else
	
			id1 = ib1
			id2 = ib2
			call losc_get_dwf2(id1,id2)
	
		end if
	
		ret = 0.0_dp
		do iuc = 1, nksz
			iuct = wuc_map(isc,iuc)
			!$OMP PARALLEL DO REDUCTION(+:ret)
			do ir1 = 1, nrsz
					ret = ret + ( dwfn0(ir1,iuc,id1)*dwfn0(ir1,iuct,id2) )**rpow
			end do
			!$OMP END PARALLEL DO
		end do
	
		ret = ret * omega / real(nrsz,dp)
	
		return
	end function overlap_pow_pbc

    function overlap_pow_v11( ib1, ib2, cen1, cen2, isc, rpow, boxlen ) result(ret)
        ! v11: include centers and boxlen as params
        ! now it can be used for localization checking with a variable boxlen,
        ! and passable centers means we can try to handle finding the closest mirror
        ! images in a separate function
      USE cell_base,    ONLY : omega
      USE sys,          ONLY : nrsz, nkdm, mmcvec1d, pscshoff, pscsh
      USE utils,        ONLY : losc_cart2frac
        integer,  intent(in) :: ib1, ib2
        real(dp), intent(in) :: cen1(3), cen2(3)
        integer,  intent(in) :: isc
        real(dp), intent(in) :: rpow
        real(dp), intent(in) :: boxlen(3)
        ! Locals
        real(dp) :: ret

        integer     :: i1, i2         ! iterators
        logical     :: sceq, diag     ! home cell/diagonal bool

        ! centers and ranges
        real(dp)    :: lcen1(3), lcen2(3) ! LWF centers, unit cell adjusted
        real(dp)    :: lwfrng1(3,2)   ! LWF range (3D)
        real(dp)    :: lwfrng2(3,2)   ! LWF range (3D)
        real(dp)    :: ovrng(3,2)     ! overlap range

        ! unit cell finder logic
        logical     :: icom           ! if immc1 & immc2 have a common index
        integer     :: immcsz         ! immc size
        integer, allocatable :: immc1(:) 
        integer, allocatable :: immc2(:) ! which mmcs each LWF lives in

        real(dp)    :: c1m2(3)        ! cen1 - cen2
        real(dp)    :: roff(3)        ! mmc offset

        call start_clock('overlap_pow')

        ret = 0.0_dp
        sceq = .false.
        if( isc.eq.pscshoff ) sceq = .true.

        diag = .false.
        if( sceq .and. (ib1.eq.ib2) ) diag = .true.

        lcen1(1:3) = cen1(1:3) ! local shorthand

        if( .not.diag ) then

                lcen2(1:3) = cen2(1:3) + pscsh(1:3,isc)
                c1m2(1:3) = lcen1(1:3) - lcen2(1:3)
                c1m2 = losc_cart2frac(c1m2)   ! cast to fractional

                ! check if they can possibly overlap first
                do i1 = 1, 3
                    if ( abs(c1m2(i1)) .gt. nkdm(i1) ) then
                            call stop_clock('overlap_pow')
                            return
                    end if
            end do
        end if

        lcen1 = losc_cart2frac(lcen1) ! cast to fractional
        ! set the LWF1 ranges
        lwfrng1(1:3,1) = lcen1(1:3) - (0.5_dp*boxlen(1:3))
        lwfrng1(1:3,2) = lcen1(1:3) + (0.5_dp*boxlen(1:3))

        ! size could be up to +1 in all dims for boxlen=nkdm
        immcsz = product( nkdm(1:3) + 1 )

        allocate( immc1(immcsz) )
        immc1(1:immcsz) = -1
        call losc_box2mmc_v9(lcen1, boxlen, immc1, immcsz)

        if ( .not.diag ) then

                lcen2 = losc_cart2frac(lcen2) ! cast to fractional
                ! set LWF2 ranges
                lwfrng2(1:3,1) = lcen2(1:3) - (0.5_dp*boxlen(1:3))
                lwfrng2(1:3,2) = lcen2(1:3) + (0.5_dp*boxlen(1:3))

                allocate( immc2(immcsz) )
                immc2(1:immcsz) = -1
                call losc_box2mmc_v9(lcen2, boxlen, immc2, immcsz)

                ovrng(1:3,1) = max( lwfrng1(1:3,1), lwfrng2(1:3,1) )
                ovrng(1:3,2) = min( lwfrng1(1:3,2), lwfrng2(1:3,2) )

        else
                ovrng(:,:) = lwfrng1(:,:)
        end if


        doOvPow11: do i1 = 1, immcsz

                if( immc1(i1) .lt. 1 ) exit doOvPow11

                roff(1:3) = mmcvec1d(1:3, immc1(i1))

                if( .not.diag )then ! off-diag case
                        icom = .false.
                        doim2: do i2 = 1, immcsz
                                if ( immc1(i1) .eq. immc2(i2) ) then
                                        icom = .true.
                                        exit doim2
                                end if
                        end do doim2
                        if( .not.icom ) cycle doOvPow11
                end if

                ret = ret + ov_pow_cell_part_v11( immc1(i1), ib1, ib2, isc, ovrng, rpow )

        end do doOvPow11

        ret = ret * ( omega / real(nrsz,dp) )

        if( isnan(ret) ) then
                write(*,*) 'ERR(overlap::overlap_pow): NaN return!'
                call errore('LOSC(overlap_pow)','NaN return detected',4)
        end if

        deallocate( immc1 )
        if ( .not.diag ) deallocate( immc2 )
        call stop_clock('overlap_pow')
        return 
    end function overlap_pow_v11


    function ov_pow_cell_part_v11( iuc, ib1, ib2, rscin, corns, rpow ) result(ret)
    ! v11: diag handling takes same rpow
      USE cell_base,       ONLY : at
      USE pw2losc,         ONLY : nks
      USE sys,             ONLY : nrdm, dwfn0, pscshoff
      USE wannier_density, ONLY : losc_get_dwfn0, losc_get_dwf2
        implicit none
        ! Params
        integer,  intent(in)  :: iuc        ! mmcvec index
        integer,  intent(in)  :: ib1, ib2   ! bands for overlap
        integer,  intent(in)  :: rscin      ! unit cell of band 2
        real(dp), intent(in)  :: corns(3,2) ! overlap box corners
        real(dp), intent(in)  :: rpow       ! overlap power
    !  integer,  intent(inout)  :: idbg
        ! Return
        real(dp) :: ret
        ! Locals
        integer     :: i1, i2, i3      ! 3d grid iters
        integer     :: i2t, i3t, ir1   ! i2/3 *ld1/2, 3d->1d index
        integer     :: id1, id2        ! dwfn0 3rd indices
        integer     :: iucd1, iucd2
        integer     :: inr12
        integer     :: ix(3)
        integer     :: irng(3,2)
        logical     :: diag
        real(dp)    :: dpow ! diag pow


        inr12 = nrdm(1) * nrdm(2)

        call losc_subcell_v9( iuc, corns, irng )

        diag = .false.
        if( (ib1.eq.ib2) .and. (rscin.eq.pscshoff) ) diag = .true.

        id1 = losc_get_dwfn0(ib1)
        iucd1 = losc_mmc2mc_v9(iuc)

        if( diag )then
            dpow = 2.0_dp * rpow
        else
                id1 = ib1
                id2 = ib2
                call losc_get_dwf2(id1, id2)
                iucd2 = losc_mmc2mc_v9(iuc,rscin)
        end if

        ret = 0.0_dp

        if( diag )then
                do i3 = irng(3,1), irng(3,2)
                        i3t = i3*inr12 + 1
                do i2 = irng(2,1), irng(2,2)
                        i2t = i3t + i2*nrdm(1)
                do i1 = irng(1,1), irng(1,2)

                        ir1 = i1 + i2t
                        ret = ret + dwfn0(ir1,iucd1,id1)**dpow

                end do
                end do
                end do
        else
                do i3 = irng(3,1), irng(3,2)
                        i3t = i3*inr12 + 1
                do i2 = irng(2,1), irng(2,2)
                        i2t = i3t + i2*nrdm(1)
                do i1 = irng(1,1), irng(1,2)

                        ir1 = i1 + i2t
                        ret = ret + & 
                            (dwfn0(ir1,iucd2,id2) * dwfn0(ir1,iucd1,id1))**rpow

                end do
                end do
                end do
        end if

        return
    end function ov_pow_cell_part_v11


    subroutine losc_box2mmc_v9(cenin, lenin, iout, isz)
        ! v9: non-cubic
        ! v9: instead of using cartesian, need uplim to be in fractional
      USE cell_base,  ONLY : at
      USE pw2losc,    ONLY : nks, nkstot
      USE sys,        ONLY : nrdm, mmcsz, mmcvec1d
      USE utils,      ONLY : lt_3r, gt_3r, le_3rt, ge_3rt, losc_cart2frac
        implicit none
        ! Params
        real(dp), intent(in)    :: cenin(3) ! center of box, fractional coords
        real(dp), intent(in)    :: lenin(3) ! length of each side, fractional
        integer,  intent(inout) :: iout(:)  ! indices
        integer,  intent(in)    :: isz      ! size of iout
        ! Locals
        integer  :: i1, ii
        real(dp) :: boxrng(3,2) ! 3d box range, 2nd dim 1=close, 2=far
        real(dp) :: upcorn(3)   ! upper corner of current cell
        real(dp) :: locorn(3)
        real(dp) :: uplim(3)
        real(dp) :: grtol(3)    ! real space grid tolerance
        real(dp) :: ptwid(3)    ! real space grid point width, fractional
        logical :: lolt, loge, lole ! shorthand for logicals
        logical :: higt, hige, hile 

        call start_clock('losc_box2mmc')

        ptwid(1:3) = 1.0_dp / real(nrdm(1:3),dp)
        grtol(1:3) = 0.5_dp * ptwid(1:3)
        uplim(1:3) = 1.0_dp - ptwid(1:3) ! @DEBUG: true for non-cubic orthorhombic?
        !uplim(i) = veclen(at(:,i)) - ptwid(i) ? or does ptwid depend on at too?

        do i1 = 1, 3
            boxrng(i1,1) = cenin(i1) - lenin(i1)*0.5_dp
            boxrng(i1,2) = cenin(i1) + lenin(i1)*0.5_dp
        end do

        ii = 0
        dommc1: do i1 = 1, mmcsz

            locorn(1:3) = losc_cart2frac(mmcvec1d(1:3,i1))
            locorn(1:3) = real( nint(locorn(1:3)), dp ) ! remove floating errors
            upcorn(1:3) = locorn(1:3) + uplim(1:3)

            ! for if cell in between range
            higt = gt_3r( boxrng(1:3,2), locorn(1:3) )
            lolt = lt_3r( boxrng(1:3,1), upcorn(1:3) )

            ! for if range is insde a cell
            lole = le_3rt( boxrng(1:3,1), upcorn(1:3), grtol(1:3) )
            loge = ge_3rt( boxrng(1:3,1), locorn(1:3), grtol(1:3) )
            hile = le_3rt( boxrng(1:3,2), upcorn(1:3), grtol(1:3) )
            hige = ge_3rt( boxrng(1:3,2), locorn(1:3), grtol(1:3) )

            if ( (lole.and.loge) .or. & ! lo range is inside current cell
                        (hile.and.hige) .or. & ! hi range is inside current cell
                        (higt.and.lolt) ) then ! cell is in the middle of the range 

                ii = ii + 1
                if ( ii .gt. isz ) then ! error checking
                        ! @TODO: call error function
                        write(6,'("ERR(box2mmc_v9) isz exceeded")')
                        stop
                end if

                iout(ii) = i1

            end if

        end do dommc1
        call stop_clock('losc_box2mmc')
        return
    end subroutine losc_box2mmc_v9
    
    
    subroutine losc_subcell_v9( ucin, cornin, rngout )
    ! v9: non-cubic, cornin is now fractional
    ! given a unit cell and box corners, find the grid indexes that range
    ! that box within that cell
      USE cell_base,    ONLY : at
      USE sys,          ONLY : nrdm, mmcvec1d, rsgr
      USE utils,        ONLY : losc_cart2frac
        implicit none
        ! Params
        integer,  intent(in)  :: ucin            ! unit cell index
        real(dp), intent(in)  :: cornin(3,2)     ! box corners, fractional coords
        integer,  intent(out) :: rngout(3,2)     ! grid point index ranges
        ! Locals
        integer  :: i1        ! dim iter
        integer  :: ir1       ! dummy var for pt2grid 3rd arg
        integer  :: uplo(3)   ! whether an edge is an upper or lower limit
        real(dp) :: upcorn(3) ! upper coner of unit cell
        real(dp) :: locorn(3) ! upper coner of unit cell
        real(dp) :: ptwid(3)  ! real space grid point widtch
        real(dp) :: grtol(3)
        real(dp) :: xp(3)

        ! set upper corner
        !upcorn(1:3) = mmcvec1d(1:3,ucin) + at(1:3,1) + at(1:3,2) + at(1:3,3)

        ptwid(1:3) = 1.0_dp / real(nrdm(1:3),dp)
        grtol(1:3) = 0.5_dp * ptwid(1:3)

        locorn(1:3) = losc_cart2frac(mmcvec1d(1:3,ucin)) 
        locorn(1:3) = real( nint(locorn(1:3)), dp) - grtol(1:3)
        upcorn(1:3) = locorn(1:3) + 1.0_dp

        ! find if any of the overlap box edges lie in this cell
        uplo(1:3) = -1
        do i1 = 1, 3
                if( (cornin(i1,1).lt.upcorn(i1)) .and. &
                        (cornin(i1,1).gt.locorn(i1)) ) uplo(i1) = 0
                if( (cornin(i1,2).lt.upcorn(i1)) .and. &
                        (cornin(i1,2).gt.locorn(i1)) )then
                        if( uplo(i1).eq.0 )then
                                uplo(i1) = 2 ! both corns inside
                        else
                                uplo(i1) = 1
                        end if
                end if
        end do

        ! get the unit cell coords of the corners 
        call losc_pt2grid_v9( cornin(:,1), rngout(:,1), ir1, i1 )
        xp(1:3) = rsgr(1:3,ir1) + mmcvec1d(1:3,i1)
        do i1 = 1, 3 ! fix rounding errors
                if( xp(i1) .lt. (cornin(i1,1)-grtol(i1)) ) rngout(i1,1) = rngout(i1,1) + 1
        end do

        call losc_pt2grid_v9( cornin(:,2), rngout(:,2), ir1, i1 ) ! last 2 dummy args
        xp(1:3) = rsgr(1:3,ir1) + mmcvec1d(1:3,i1)
        do i1 = 1, 3 ! fix rounding errors
                if( xp(i1) .gt. (cornin(i1,2)+grtol(i1)) ) rngout(i1,2) = rngout(i1,2) - 1
        end do

        doCornLims9: do i1 = 1, 3

                if( uplo(i1).eq.-1 ) then       ! whole dimension covered
                        rngout(i1,1) = 0
                        rngout(i1,2) = nrdm(i1)-1

                else if( uplo(i1).eq.0 ) then   ! lower edge in cell
                        rngout(i1,1) = rngout(i1,1) ! keep the same
                        rngout(i1,2) = nrdm(i1)-1

                else if( uplo(i1).eq.1 ) then   ! upper edge in cell
                        rngout(i1,1) = 0
                        rngout(i1,2) = rngout(i1,2) ! keep the same

                else if( uplo(i1).eq.2 ) then   ! lower edge in cell
                        rngout(i1,1) = rngout(i1,1) ! keep the same
                        rngout(i1,2) = rngout(i1,2) ! keep the same

                end if
        end do doCornLims9

        return
    end subroutine losc_subcell_v9	


    subroutine losc_pt2grid_v9(xpin, ixout, irout, immcout)
	! v9: non-cubic, xpin is now in fractional coords
	! translate point xpin to grid coordinates

      USE cell_base,  ONLY : at
      USE sys,        ONLY : nrdm, mmcsz, mmcvec1d, nrsz
		  USE utils,      ONLY : le_3rt, ge_3rt, losc_cart2frac, losc_frac2cart

		implicit none
		! Params
		real(dp), intent(in)  :: xpin(3)  ! coord in fractional at
		integer,  intent(out) :: ixout(3) ! 3D coord
		integer,  intent(out) :: irout    ! 1D coord
		integer,  intent(out) :: immcout  ! mmcvec1d coord
		! Locals
		integer  :: ir1, im     ! 1d grid iter
		integer  :: inr1, inr12 ! nr1, nr1*nr2
		real(dp) :: rd, rdmin
		real(dp) :: xp(3), xpc(3)
		real(dp) :: uplim(3)
		real(dp) :: locorn(3), upcorn(3)
		real(dp) :: ptwid(3), grtol(3)
		logical  :: xle, xge

		call start_clock('losc_pt2grid')

		inr1  = nrdm(1) !dffts%nr1
		inr12 = inr1 * nrdm(2) !dffts%nr2
		ptwid(1:3) = 1.0_dp / real(nrdm(1:3),dp)
		grtol(1:3) = 0.5_dp * ptwid(1:3)

		! upper limit is halfway between upper most grid point 
		! and the origin of the next cell
		! @CUBIC: change at to 1.0
		!uplim(1) = at(1,1) - eqtol(1)
		!uplim(2) = at(2,2) - eqtol(2)
		!uplim(3) = at(3,3) - eqtol(3)

		! @TODO: switch to frac of ats, not actual coords
		immcout = -1
		doimpt2gr: do im = 1, mmcsz
				locorn(1:3) = losc_cart2frac( mmcvec1d(1:3,im) )
				locorn(1:3) = real( nint(locorn(1:3)), dp )
				upcorn(1:3) = locorn(1:3) + 1.0_dp - ptwid(1:3)
				!xp(1:3) = mmcvec1d(1:3,im) + uplim(1:3) ! xp = upper corner
				xge = ge_3rt( xpin, locorn, grtol )
				xle = le_3rt( xpin, upcorn, grtol )
				if ( xge .and. xle ) then
						immcout = im
						exit doimpt2gr
				end if
		end do doimpt2gr

		if ( immcout .eq. -1 ) then
				! This is allowed, in the case where atoms are placed
				! outside the home unit cell boundaries, this will
				! happen at the edge of the mmc
				!write(6,'("ERR(pt2grid), immc not found")')
				ixout(1:3) = -1
				irout = -1
				return
		end if
		
		xpc(1:3) = losc_frac2cart(xpin(1:3)) - mmcvec1d(1:3,immcout) 
		xp(1:3) = losc_cart2frac( xpc(1:3) )
		if( (xp(1).lt.-grtol(1)) .or. (xp(1).gt.(1.0_dp+grtol(1))) ) then
				write(6,*) 'ERR(pt2gr)-xrange'
		end if
		if( (xp(2).lt.-grtol(2)) .or. (xp(2).gt.(1.0_dp+grtol(2))) ) then
				write(6,*) 'ERR(pt2gr)-yrange'
		end if
		if( (xp(3).lt.-grtol(3)) .or. (xp(3).gt.(1.0_dp+grtol(3))) ) then
				write(6,*) 'ERR(pt2gr)-zrange'
		end if
		call flush(6)

		! @working here now... 24OCT19
		! need new translation from xp to ixout

		ixout(1:3) = nint( xp(1:3) * real(nrdm(1:3),dp) )
		irout = ixout(1) + ixout(2)*inr1 + ixout(3)*inr12 + 1

		if ( irout.gt.nrsz ) then
				write(6,*) 'ERR(pt2gr) nrsz range'
		end if
		do im = 1, 3
				if( (ixout(im).lt.0) .or. (ixout(im).ge.nrdm(im)) )then
						write(6,*) 'ERR(pt2gr) ix range'
				end if
		end do

		call stop_clock('losc_pt2grid')

	end subroutine losc_pt2grid_v9


    function losc_mmc2mc_v9(iuc, isc) result(ret)
    ! translate and mmc index to mc, with possible pscsh offset, isc
      USE cell_base,      ONLY : at
      USE pw2losc,        ONLY : nks
      USE sys,            ONLY : nrdm, nkdm, nksz, mcvec1d, mmcvec1d
      USE sys,            ONLY : pscsh, pscshoff
      USE utils,          ONLY : eq_3rt, losc_cart2frac, losc_frac2cart
        implicit none
        ! Params
        integer, intent(in) :: iuc
        integer, optional, intent(in) :: isc
        ! Return
        integer   :: ret
        ! Locals
        integer   :: i1, isloc ! isc local
        integer   :: ix(3)
        real(dp)  :: xc(3)     ! position in cartesian
        real(dp)  :: grtol(3)

        grtol(1:3) = 0.5_dp / real(nrdm(1:3),dp)

        isloc = pscshoff ! home cell
        if( present(isc) ) isloc = isc

        ix(1:3) = nint( losc_cart2frac( mmcvec1d(1:3,iuc)-pscsh(1:3,isloc) ) )
        ix(1:3) = modulo( ix(1:3), nkdm(1:3) )
        xc(1:3) = losc_frac2cart( real(ix(1:3),dp) )

        ret = 0
        do i1 = 1, nksz
                if( eq_3rt( xc, mcvec1d(:,i1), grtol ) )then
                        ret = i1
                        exit
                end if
        end do

        if( (ret.lt.1) .or. (ret.gt.nksz) )then
                call errore('LOSC','(mmc2mc) ret out of range',1)
        end if

        return
    end function losc_mmc2mc_v9

END MODULE overlap