! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE sys
! module specifically for replacing pw.x module variables
! that would have been set in read_file since it doesn't run
! the way I need for MPI runs
  USE kinds,          ONLY : dp
  USE parameters,     ONLY : ntypx

  IMPLICIT NONE
  SAVE

  ! Spin polarization: spin_offset = {0, k1 * k2 * k3} for spin {up, dw}
  INTEGER               :: spin_offset = 0    ! PWSCF k-index array spin offset

  ! Disentangled Bloch eigenvalues and excluded bands
  REAL(dp), ALLOCATABLE :: etd(:,:)           ! eigenvalues disentangled, dim(nw,nk)
  INTEGER,     ALLOCATABLE :: dis_bndi(:)     ! dis band indices (non-excluded bands)

  ! DLWF variables [2022-04-20: MOVE to wannier_density ??]
  COMPLEX(dp), ALLOCATABLE :: unkgr(:,:,:)    ! LWF u_nk on the grid
  REAL(dp),    ALLOCATABLE :: dwfn0(:,:,:)    ! LWF_n density @ R=0 on the grid
  COMPLEX(dp), ALLOCATABLE :: dwfft(:,:)      ! LWF_n density FFT on the SC
  COMPLEX(dp), ALLOCATABLE :: afft(:)         ! array for FFT

  ! supercell_base variables
  REAL(dp)                        :: omegalosc    = 0.0_dp
  REAL(dp)                        :: alatlosc     = 0.0_dp
  REAL(dp)                        :: tpiba2losc   = 0.0_dp
  REAL(dp)                        :: atlosc(3,3), bglosc(3,3)

  ! Born-von Karman cell variables
  ! BvK unit-cell vecs, dim(3,nksx*nksy*nksz)
  INTEGER, ALLOCATABLE          :: mci(:,:)        ! 3D->1D index map for mcvec
  REAL(dp),             POINTER :: mcvec(:,:,:,:)   ! BvK unit cell vectors
  REAL(dp), ALLOCATABLE, TARGET :: mcvec1d(:,:)     ! flattened mcvec
  INTEGER, ALLOCATABLE          :: mmci(:,:)
  REAL(dp), ALLOCATABLE, TARGET :: mmcvec1d(:,:) 
  INTEGER                       :: mmcsz    ! size of multi-multi-cell
  INTEGER                       :: mmcoff   ! home cell offset in mmcvec1d
  INTEGER                       :: pscshsz  ! size of pscsh
  INTEGER                       :: pscshoff ! home cell location in pscsh
    ! periodic super-cell shell, target in case I want to use a 3 index pointer
    ! later, like with mcvec
  REAL(dp), ALLOCATABLE, TARGET :: pscsh(:,:)

    ! dim(nrsz,nks,nbnd), hold each BvK cell in 2nd index, use mmc mapping
  integer                   :: idwfsz = 0      ! size of dwfn0 3rd dim 
  integer,     allocatable  :: ib2dwf(:)       ! map from band to dwfn0 index, dim(nwan)
  integer,     allocatable  :: id2uc(:)       ! map from unit cell to dwfft index, dim(idwfsz)
  integer,     allocatable  :: ib2fft(:)       ! map from band to dwfft index, dim(nwan)

  ! Real and reciprocal grids
  integer                   :: nrdm(3)    ! real space grid dims, from dffts
  integer                   :: nkdm(3)    ! k-dims in 3D, from start_k::nk[1-3]
  integer                   :: scnrdm(3)  ! scnrdm(:) = nkdm(:) * nrdm(:)
  integer                   :: nrsz       ! nrsz = product(nrdm)
  integer                   :: nksz       ! nksz = product(nkdm)
  real(dp), allocatable     :: rsgr(:,:)  ! 1d flattened real grid for 1 UC
  real(dp), allocatable     :: xkg(:,:)   ! k-mesh in fractional rec. basis

  ! Curvature cutoffs: Don't calculate kappa_ij if lambda_ij is small enough
  real(dp),    allocatable :: limde(:,:,:)    ! cutoff factor for kappam

  ! LOSC vars
  real(dp), parameter :: kctol = 0.0_dp   ! @DEBUG
  !real(dp), parameter :: kctol = 1.0e-10_dp   ! kappa cutoff tolerance
  real(dp), parameter :: rtol  = 1.0e-14_dp   ! real(dp) cutoff tolerance
  real(dp)            :: losck = 0.0_dp       ! Coulomb screening parameter
  real(dp)            :: losctau = 0.0_dp     ! Exchange screening factor 
  integer             :: ikjtyp               ! kjmetric eval method (RI, PW, ...)

    ! wvfct vars
  !integer     :: nbnd = -1                    ! number of bands
  !integer     :: npwx = -1                    ! max number of PW for evc
  !integer,  allocatable :: ngk(:)             ! num of PW for each point
  !real(dp), allocatable :: et(:,:)            ! eigenvalues, dim(nb,nk)
  !real(dp), allocatable :: wg(:,:)            ! k point weights

  ! 2022-04-20: MOVED MPI variables to utils
  ! MPI book keeping
  !logical             :: ionodescr  ! the io rank for that node for /scr
  !character(len=64)   :: scrfil     ! scratch files name seed
  !character(len=64)   :: nodenam
  !integer             :: mpioun
  !integer             :: optlvl     ! optimization level, where to store wave funcs

  ! LWF vars [2022-03-31: most MOVED to wan2losc]
  !character(len=80)  :: seedname   ! wan90 seedname
  !integer     :: nexbnd = -1                  ! how many excluded bands: dbnd=nbnd-nexbnd
  !integer     :: dbnd = -1                    ! how many bands used @ disentanglment
  !integer     :: nwan = -1                    ! number of Wannier funcs, <= nbnd
  !integer,     allocatable :: dis_bndi(:)     ! dis band indices (non-excluded bands)
  !logical,     allocatable :: exc_band(:)     ! logicals for excluded bands
  !complex(dp), allocatable :: unkgr(:,:,:)    ! LWF u_nk on the grid
  !real(dp),    allocatable :: dwfn0(:,:,:)    ! LWF_n density @ R=0 on the grid
  !complex(dp), allocatable :: dwfft(:,:)      ! LWF_n density FFT on the SC
  !complex(dp), allocatable :: afft(:)         ! array for FFT

    !! 2022-03-31: These are MOVED to wan2losc module !!
  !real(dp),    allocatable :: lwfcen(:,:)     ! LWF center
  !complex(dp), allocatable :: u_wan(:,:,:)    ! Wannier func transform, dim(nwan,nwan,nk)
  !complex(dp), allocatable :: u_dis(:,:,:)    ! BO disentanglement matrix, dim(nbnd,nwan,nk)
  !complex(dp), allocatable :: u_loc(:,:,:)    ! Combined localization transform, u_wan.u_dis

  ! klist vars (2022-04-20: MOVED to pw2losc)
  !integer     :: nks=0, nkstot=0              ! k point count
  !real(dp)    :: nelec, nelup, neldw          ! num of electrons
  !real(dp), allocatable :: xk(:,:)            ! k point coordinates
  !real(dp), allocatable :: xkg(:,:)           ! k point coordinates in fractional rec. basis
  !real(dp), allocatable :: wk(:)              ! k point weights

  ! supercell FFT vars (2022-04-19: MOVED to supercell_fft)
  !REAL(dp), ALLOCATABLE           :: glosc(:,:)
  !REAL(dp), ALLOCATABLE           :: gglosc(:)
  !INTEGER                         :: ngmlosc_g
  !INTEGER                         :: gstart_losc
   
  ! 2022-04-18: ions_base is in Modules, so shouldn't need to reproduce here
  ! ions_base vars
  !integer     :: nat = -1                     ! atom count
  !integer     :: nsp = -1                     ! species count
  !integer,  allocatable :: ityp(:)            ! atom charge
  !real(dp), allocatable :: tau(:,:)           ! atom positions
  !character(len=3)      :: atm(ntypx)         ! atom names

  ! 2022-04-20: eqtol MOVED to utils
  !real(dp) :: eqtol(3)   ! equal tolerance for real space grid
  
  CONTAINS

  SUBROUTINE losc_setup()
    ! Wrapper for the individual setup routines; called in main program
    USE cell_base,        ONLY : omega, alat, at, bg
    USE pw2losc,          ONLY : nkstot, nspin, nbnd
    USE wan2losc,         ONLY : nwan 

    IMPLICIT NONE

    CALL set_k_grid()
    CALL set_r_grid()

    CALL set_supercell_base(omega, alat, at, bg, nkdm)
    CALL set_supercell_grid()

    CALL set_excluded_bands(nbnd)

  END SUBROUTINE losc_setup


  SUBROUTINE set_k_grid()
    ! Set up the Monkhorst-Pack k-mesh, including xkg(3, nkstot) = k-point
    !   coordinates in fractional reciprocal basis

    USE pw2losc,                ONLY : nkstot, xk
    USE utils,                  ONLY : losc_cart2frac_rec


    IMPLICIT NONE 

    ! Locals
    INTEGER                         :: i1, i2, i3
    REAL(dp), ALLOCATABLE           :: xk1(:)
    REAL(dp)                        :: xkf(3), xk2

    ALLOCATE(xk1(nkstot))
    xk1(:) = HUGE(1.0_dp)           ! absurdly large as initial guess

    IF (ALLOCATED(xkg)) DEALLOCATE(xkg)
    ALLOCATE(xkg(3, nkstot))

    DO i1 = 1, nkstot
      xkg(:, i1) = losc_cart2frac_rec(xk(:, i1))
    END DO
    
    ! Count unique elements in each dimension of xk
    DO i1 = 1, 3

      xkf = losc_cart2frac_rec(xk(:,1))
      nkdm(i1) = 1
      xk1(1) = xkf(1)
      doi2: DO i2 = 2, nkstot
          xkf = losc_cart2frac_rec(xk(:,i2))
          xk2 = xkf(i1)
          doi3: DO i3 = 1, nkdm(i1)
              IF (ABS(xk2-xk1(i3)) < 1.0e-10_dp) THEN
                  CYCLE doi2
              END IF
          END DO doi3
          nkdm(i1) = nkdm(i1) + 1
          xk1(nkdm(i1)) = xk2
      END DO doi2

    END DO

    nksz = PRODUCT(nkdm)

    RETURN
  END SUBROUTINE set_k_grid 


  SUBROUTINE set_r_grid()
    ! Set the real-space grid dimensions

    USE fft_base,           ONLY : dffts

    IMPLICIT NONE

    nrdm(1) = dffts%nr1
    nrdm(2) = dffts%nr2
    nrdm(3) = dffts%nr3

    nrsz = PRODUCT(nrdm)

  END SUBROUTINE set_r_grid


  SUBROUTINE set_spin_offset(nksin, ispin)
    ! Set the offset for looping over pwscf arrays, which are often of the form
    !   arr(1:nkstot), where nkstot = nspin * nksz;
    !   indices 1-nksz are for spin-up, (nksz+1)-nkstot are for spin-dw
    ! Thus we loop from (1 + spin_offset) to (nksz + spin_offset), where
    !   spin_offset = {0 [restricted, up-spin], nksz [down-spin]}
    ! For a spin-polarized calculation, should be run once for each spin.
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: nksin      ! number of k-points in each spin comp.
    INTEGER, INTENT(IN)   :: ispin      ! up: 1; down: 2

    spin_offset = (ispin - 1) * nksin

  END SUBROUTINE set_spin_offset


  SUBROUTINE set_supercell_base( omegain, alatin, atin, bgin, nkdmin )
    USE constants,    ONLY : tpi

    IMPLICIT NONE

    ! Parameters
    REAL(dp), INTENT(IN)  :: omegain, alatin
    REAL(dp), INTENT(IN)  :: atin(3, 3), bgin(3, 3)
    INTEGER,  INTENT(IN)  :: nkdmin(3)

    ! Locals
    INTEGER               :: i
    REAL(dp)              :: k1

    k1 = REAL(nkdmin(1), dp)

    alatlosc = alatin * k1
    tpiba2losc = (tpi / alatlosc)**2.0_dp
    omegalosc = omegain * REAL(PRODUCT(nkdmin))

    DO i = 1, 3
      IF (nkdm(i) == k1) THEN
        atlosc(:, i) = atin(:, i)
        bglosc(:, i) = bgin(:, i)
      ELSE
        atlosc(:, i) = atin(:, i) * REAL(nkdmin(i), dp) / k1
        bglosc(:, i) = bgin(:, i) * k1 / REAL(nkdmin(i), dp)
      END IF
    END DO

  END SUBROUTINE set_supercell_base


  SUBROUTINE set_supercell_grid()
    USE cell_base,              ONLY : at
    ! Set the (Born-von Karman) supercell grid dimensions and associated vars

    IMPLICIT NONE

    ! Locals
    INTEGER                         :: ix(3)
    INTEGER                         :: i1, i2, i3, ii     ! iterators
    INTEGER                         :: inr1, inr12        ! 1, 2d real grid size
    REAL(dp)                        :: obnr(3), atbnr(3)  ! {1,at}/nrdm(:)

    ! BvK grid
    scnrdm(:) = nrdm(:) * nkdm(:)

    mmcsz = 27 * nksz             ! 3 x 3 x 3 grid of shells

    ALLOCATE(mcvec1d(3, nksz))
    mcvec(1:3, 1:nkdm(1), 1:nkdm(2), 1:nkdm(3)) => mcvec1d
    ALLOCATE(mci(3, nksz))

    ii = 0
    DO i3 = 1, nkdm(3)
    DO i2 = 1, nkdm(2)
    DO i1 = 1, nkdm(1)

        ii = ii + 1
        mci(1, ii) = i1
        mci(2, ii) = i2
        mci(3, ii) = i3

        mcvec1d(:, ii) = (real(i1 - 1, dp) * at(:, 1)) + &
                         (real(i2 - 1, dp) * at(:, 2)) + &
                         (real(i3 - 1, dp) * at(:, 3))
    END DO
    END DO
    END DO

    ALLOCATE(mmcvec1d(3, mmcsz))
    ALLOCATE(mmci(3, mmcsz))

    ! fill in mmcvec
    ii = 0
    DO i3 = -nkdm(3) + 1, 2 * nkdm(3)
    DO i2 = -nkdm(2) + 1, 2 * nkdm(2)
    DO i1 = -nkdm(1) + 1, 2 * nkdm(1)
        IF ((i1 == 1) .AND. (i2 == 1) .AND. (i3 == 1)) mmcoff = ii

        ii = ii + 1

        mmci(1, ii) = i1
        mmci(2, ii) = i2
        mmci(3, ii) = i3

        mmcvec1d(:, ii) = (real(i1 - 1, dp) * at(:, 1)) + &
                          (real(i2 - 1, dp) * at(:, 2)) + &
                          (real(i3 - 1, dp) * at(:, 3))

    END DO
    END DO
    END DO

    pscshsz = product(2 * nkdm(1:3) - 1)
    ALLOCATE(pscsh(3, pscshsz))

    ii = 0
    DO i3 = -nkdm(3) + 1, nkdm(3) - 1
    DO i2 = -nkdm(2) + 1, nkdm(2) - 1
    DO i1 = -nkdm(1) + 1, nkdm(1) - 1

        ii = ii + 1
        IF ((i3 == 0) .AND. (i2 == 0) .AND. (i1 == 0)) pscshoff = ii
        pscsh(1:3, ii) = (real(i1, dp) * at(1:3, 1)) + &
                         (real(i2, dp) * at(1:3, 2)) + &
                         (real(i3, dp) * at(1:3, 3))

        CALL losc_vec2uc(pscsh(:, ii), ix)
        ix = ix + 0
        IF ((ix(1) /= i1) .AND. (ix(2) /= i2) .AND. (ix(3) /= i3) ) THEN
            WRITE(6,*) 'ERRRR: Noooope'
        END IF

    END DO
    END DO
    END DO 

    ALLOCATE(rsgr(3, nrsz)) 

    inr1 = nrdm(1) !dffts%nr1
    inr12 = inr1 * nrdm(2) !dffts%nr1 * dffts%nr2
    obnr(1:3) = 1.0_dp / real(nrdm(1:3), dp)
    atbnr(1) = obnr(1) * at(1, 1)
    atbnr(2) = obnr(2) * at(2, 2)
    atbnr(3) = obnr(3) * at(3, 3)

    DO i1 = 1, nrsz
        ix(3) = (i1 - 1) / inr12 
        ix(2) = ((i1 - 1) - ix(3) * inr12 ) / inr1
        ix(1) = i1 - ix(3) * inr12 - ix(2) * inr1 - 1

        rsgr(1:3, i1) = (obnr(1) * real(ix(1), dp) * at(1:3, 1)) + &
                        (obnr(2) * real(ix(2), dp) * at(1:3, 2)) + &
                        (obnr(3) * real(ix(3), dp) * at(1:3, 3))
    END DO

    RETURN
  END SUBROUTINE set_supercell_grid


  subroutine set_excluded_bands(num_band)
    USE wan2losc,     ONLY : lexc, exc_bands, num_exc_bands

    IMPLICIT NONE

    ! Parameters
    INTEGER, INTENT(IN)   :: num_band

    ! Locals
    INTEGER               :: i1, i2, ebsz

    ALLOCATE (dis_bndi(num_band))

    IF (lexc) THEN

      ebsz = SIZE(exc_bands)
      dis_bndi(:) = 0
      i2 = 0
      DO i1 = 1, ebsz
        IF (.NOT. exc_bands(i1)) THEN
          i2 = i2 + 1
          dis_bndi(i2) = i1
        END IF
      END DO

    ELSE

      DO i1 = 1, num_band
        dis_bndi(i1) = i1
      END DO

    END IF

    RETURN
  END SUBROUTINE set_excluded_bands


  SUBROUTINE losc_vec2uc(vecin, i3out)
    USE cell_base, ONLY: bg
    IMPLICIT NONE 
    ! Params
    REAL(dp), INTENT(IN)  :: vecin(3) ! vector in xyz coords
    INTEGER,  INTENT(OUT) :: i3out(3) ! mulitple of at that created vecin
    ! Locals
    INTEGER  :: ii, jj
    REAL(dp) :: r1
    ! convert vec to fractional coords
    ! if vec = \sum_i n_i * at(:,i), extract n_i by 
    ! n_i = \sum_j vec_j * bg(:,j) ?
    ! add optional param that checks for if n_i are integers
    ! after checking fcc Si (so not diag at(3,3)), Mathematica check gives
    ! at.Transpose[bg] = bg.Transpose[at] = Ident
    ! implement this and check by checking the pscsh when it's generated
    ! check on fcc case

    DO ii = 1, 3
        r1 = 0.0_dp
        DO jj = 1, 3
            r1 = r1 + vecin(jj)*bg(jj,ii)
        END DO
        i3out(ii) = NINT(r1)
        IF ( ABS( REAL(i3out(ii),dp) - r1 ) .gt. 1.0e-6_dp ) THEN 
            ! complain
            WRITE(6,*) 'ERR(losc_vec2uc), do we want this to be an int?'
        END IF
    END DO

    RETURN
  END SUBROUTINE losc_vec2uc


  SUBROUTINE sys_print(iprint)

    IMPLICIT NONE

    INTEGER, INTENT(IN)         :: iprint

    IF (iprint > 0) THEN
      WRITE(6,*) "spin_offset = ", spin_offset
      WRITE(6,*) "nkdm = ", nkdm
      WRITE(6,*) "nksz = ", nksz
    END IF

    RETURN
  END SUBROUTINE sys_print 

END MODULE sys

  !subroutine losc_save_sys(oun)
  !  ! save system info to unit oun

  !  USE ions_base,        ONLY : atm, tau, nat, nsp, ityp
  !  USE cell_base,        ONLY : ibrav, alat, omega, at, bg
  !  USE pw2losc,          ONLY : nbnd, nks, nkstot, nbnd, nelec, nelup, neldw
  !  USE pw2losc,          ONLY : npwx, ngk, xk, wk, et, wg
  !  integer, intent(in) :: oun ! output unit

  !  integer           :: ik, ib1, is
  !  integer           :: values(8)
  !  character(len=8)  :: date

  !  call date_and_time(DATE=date, VALUES=values)

  !  ! Header
  !  write(oun,'("LOSC save file written on ",a," at ",i0.2,":",i0.2)') &
  !            date, values(5:6)
  !  ! Basic info
  !  write(oun,'(/,7(1x,a6))') "nbnd", "nks", "nelec", "nelup", &
  !                            "neldw", "nat", "nsp"
  !  write(oun,'(7(1x,i6))') nbnd, nkstot, nint(nelec), nint(nelup), &
  !                          nint(neldw), nat, nsp
  !  write(oun,'(4(1x,a12))') "npwx", "nr1", "nr2", "nr3"
  !  write(oun,'(4(1x,i12))') npwx, nrdm(1), nrdm(2), nrdm(3)

  !  ! Unit cell info
  !  write(oun,'(/,1x,a5,2(1x,a22))') "ibrav", "alat", "omega"
  !  write(oun,'(1x,i5,2(1x,g22.14))') ibrav, alat, omega

  !  write(oun,'(/,"at:")')                     ! unit cell vectors
  !  do is = 1, 3
  !      write(oun,'(3(1x,g22.14))') at(1:3,is)
  !  end do
  !  write(oun,'(/,"bg:")')                     ! BZ unit vectors
  !  do is = 1, 3
  !      write(oun,'(3(1x,g22.14))') bg(1:3,is)
  !  end do

  !  write(oun,'(/,"tau:")')                    ! atom positions
  !  do is = 1, nat
  !      write(oun,'(3(1x,g22.14))') tau(1:3,is)
  !  end do

  !  write(oun,'(/,"ityp:")')                   ! atom types
  !  write(oun,'(8(1x,i3))') ityp(1:nat)

  !  write(oun,'(/,"atm:")')                    ! atom names
  !  do is = 1, nsp
  !      write(oun,'(" ",a)') atm(is)
  !  end do

  !  ! k-point info
  !  write(oun,'(/,"ngk:")')                    ! PW per k-point
  !  write(oun,'(8(1x,i12))') (ngk(ik), ik=1,nkstot)

  !  write(oun,'(/,"xk:")')                     ! k-point coordinates
  !  write(oun,'(3(1x,f20.10))') (xk(1:3,ik), ik=1,nkstot)
  !  write(oun,'(/,"wk:")')                     ! k-point weights
  !  write(oun,'(8(1x,g22.14))') (wk(ik), ik=1,nkstot)

  !  ! Band structure 
  !  write(oun,'(/,"et:")')                     ! KS eigenvalues
  !  do ik = 1, nkstot
  !      write(oun,'("@ kpt ",i0)') ik
  !      write(oun,'(8(1x,g22.14))') (et(ib1,ik), ib1=1,nbnd)
  !  end do
  !  write(oun,'(/,"wg:")') ! KS eigenvalue weights
  !  do ik = 1, nkstot
  !      write(oun,'("@ kpt ",i0)') ik
  !      write(oun,'(8(1x,g22.14))') (wg(ib1,ik), ib1=1,nbnd)
  !  end do

  !end subroutine losc_save_sys


  !subroutine losc_set_ints(nbndin, npwxin, nksin, nkstotin)
  !! set integer values 
  !  integer, intent(in) :: nbndin
  !  integer, intent(in) :: npwxin
  !  integer, intent(in) :: nksin
  !  integer, intent(in) :: nkstotin
  !
  !  nbnd = nbndin
  !  npwx = npwxin
  !  nks  = nksin
  !  nkstot = nkstotin
  !  idwfsz = 0
  !
  !end subroutine losc_set_ints


  !subroutine losc_set_wvfct( ngkin, etin, wgin) !, unkin )
  !  integer,  intent(in) :: ngkin(:)  ! num of PW for each point
  !  real(dp), intent(in) :: etin(:,:) ! eigenvalues
  !  real(dp), intent(in) :: wgin(:,:) ! k point weights
  !  complex(dp), intent(in) :: unkin(:,:,:)
  !
  !  if ( allocated(ngk) ) deallocate( ngk )
  !  allocate ( ngk(nks) )
  !  ngk(:) = ngkin(:)
  !
  !  if ( allocated(et) ) deallocate( et )
  !  allocate ( et(nbnd, nks) )
  !  et(:,:) = etin(:,:)
  !
  !  if ( allocated(wg) ) deallocate( wg )
  !  allocate ( wg(nbnd,nks) )
  !  wg(:,:) = wgin(:,:)
  !
  !  if ( allocated(unkgr) ) deallocate( unkgr )
  !  allocate ( unkgr(nrsz, nks, nbnd) )
  !  unkgr(:,:,:) = unkin(:,:,:)
  !
  !end subroutine losc_set_wvfct


  !subroutine losc_set_klist( nelecin, nelupin, neldwin, xkin, wkin )
  !  real(dp), intent(in) :: nelecin, nelupin, neldwin ! num of electrons
  !  real(dp), intent(in) :: xkin(:,:) ! k point coordinates
  !  real(dp), intent(in) :: wkin(:)   ! k point weights
  !
  !  nelec = nelecin
  !  nelup = nelupin
  !  neldw = neldwin
  !
  !  if ( allocated(xk) ) deallocate( xk )
  !  allocate( xk(3,nks) )
  !  xk(:,:) = xkin(:,:)
  !
  !  if ( allocated(wk) ) deallocate( wk )
  !  allocate( wk(nks) )
  !  wk(:) = wkin(:)
  !
  !end subroutine losc_set_klist


  !subroutine losc_set_cell_base( omegain, alatin, atin, bgin, ibravin )
  !  use constants, only: tpi
  !  real(dp), intent(in) :: omegain, alatin
  !  real(dp), intent(in) :: atin(3,3), bgin(3,3)
  !  integer,  intent(in) :: ibravin
  !
  !  omega = omegain
  !  alat  = alatin
  !
  !  at(:,:) = atin(:,:)
  !  bg(:,:) = bgin(:,:)
  !
  !  ibrav = ibravin
  !
  !  tpiba = tpi / alat
  !  tpiba2 = tpiba * tpiba
  !
  !end subroutine losc_set_cell_base


  !subroutine losc_set_ions_base( natin, nspin, itypin, tauin, atmin )
  !  integer,  intent(in) :: natin      ! atom count
  !  integer,  intent(in) :: nspin
  !  integer,  intent(in) :: itypin(:)  ! atom charge
  !  real(dp), intent(in) :: tauin(:,:) ! atom positions
  !  character(len=*), intent(in) :: atmin(:)   ! atom names

  !  nat = natin
  !  nsp = nspin

  !  if ( allocated(ityp) ) deallocate( ityp )
  !  allocate( ityp(nat) )
  !  ityp(:) = itypin(1:nat)

  !  if ( allocated(tau) ) deallocate( tau )
  !  allocate( tau(3, nat) )
  !  tau(:,:) = tauin(:,1:nat)

  !  atm(1:nsp) = atmin(1:nsp)

  !end subroutine losc_set_ions_base


    ! 2022-04-20: losc_ksetup came from pw2losc.f90
  ! If we just set nkdm in sys for sure, though, I don't think all the checking
  !   is necessary (especially since this routine is lazy anyway...)
!subroutine losc_ksetup(nkdims, kvecs)
!  ! find kmesh 3d dims and verify it's a valid M-P mesh
!  ! @CUBIC ERR: xk is in xyz, for ksetup to work it needs to be
!  ! in frac coords, this is how PW/src/summary.f90 does it:
!!      use kinds, only : dp
!!      use cell_base, only: at
!!      USE klist,   ONLY : xk, nks !, wk, nkstot ! k coords & weights
!      use start_k,    only : nk1, nk2, nk3 ! k-mesh dims
!      use sys,   only : at, xk, nks
!      use utils,      only : losc_cart2frac_rec 
!      implicit none
!      ! Params
!      integer,  intent(out)   :: nkdims(3)
!      real(dp), intent(inout) :: kvecs(:,:)
!      ! Locals
!      integer     :: i1, i2, i3, ii
!      real(dp), allocatable :: xk1(:)
!      real(dp) :: xkf(3), xk2, xk3
!
!      allocate ( xk1(nks) )
!      xk1(:) = huge(1.0_dp) ! unlikely number
!
!      if ( (nk1.ne.0) .and. (nk2.ne.0) .and. (nk3.ne.0) ) then
!          ! can probably assume correct
!          nkdims(1) = nk1
!          nkdims(2) = nk2
!          nkdims(3) = nk3
!      else
!          ! need to find myself, count unique elements in each xk dim
!          do i1 = 1, 3
!
!              xkf = losc_cart2frac_rec(xk(:,1))
!              nkdims(i1) = 1
!              xk1(1) = xkf(1)
!              doi2: do i2 = 2, nks
!                  xkf = losc_cart2frac_rec(xk(:,i2))
!                  xk2 = xkf(i1)
!                  !if ( xk(i1,i2) .ne. xk1(1) ) then
!                  doi3: do i3 = 1, nkdims(i1)
!                      !if ( xk(i1,i2) .eq. xk1(i3) ) cycle doi2
!                      if( abs(xk2-xk1(i3)) .lt. 1.0e-10_dp )then
!                          cycle doi2
!                      end if
!                  end do doi3
!                  nkdims(i1) = nkdims(i1) + 1
!                  xk1(nkdims(i1)) = xk2
!              end do doi2
!
!          end do
!
!      end if
!
!      i1 = nkdims(1)*nkdims(2)*nkdims(3)
!      if ( i1 .ne. nks ) then
!          write(6,*) 'fuuuu2'
!          write(6,'(5x,"The first ",i0," k-points: ")') min(nks,20)
!          do i2 = 1, min(nks,20)
!              write(6,'(5x,"k(",i3,") = <"2(g20.10,x),g20.10,">")') i2, xk(:,i2)
!          end do
!          ! @CUBIC
!          !@ERR! the xk are in xyz but units of alat, need to 
!          ! be in fractional coords for this to work
!          write(6,'("ERR, mismatch in losc_ksetup")')
!          write(6,'("nkdims :",3(i4,x),i0)') nkdims(1:3), i1
!      end if
!
!      !if ( allocated(sccs3) ) deallocate( sccs3 )
!!      allocate( sccs3(3, nkdims(1), nkdims(2), nkdims(3)) )
!
!      deallocate ( xk1 )
!end subroutine losc_ksetup