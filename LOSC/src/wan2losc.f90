! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE wan2losc
  ! Read output from wannier90.x
  USE kinds,       ONLY: dp
 
  IMPLICIT NONE

  ! Implicit in all the wannier90 calculations
  CHARACTER(LEN=80)        :: seedname                ! wannier90 filename base

  ! From seedname.nnkp
  REAL(dp)                  :: atwan(3,3), bgwan(3,3) ! real, reciprocal lattice
  INTEGER                   :: nkwan                  ! number of k-points
  REAL(dp), ALLOCATABLE     :: xkwan(:,:)             ! k-point coordinates
  LOGICAL                   :: lexc                   ! Excluded bands flag
  INTEGER                   :: num_exc_bands          ! Number of excluded bands
  INTEGER, ALLOCATABLE      :: exc_bands(:)           ! excluded band indices

  ! From seedname_u_dis.mat
  LOGICAL                   :: ldis = .FALSE.
  INTEGER                   :: nbwan                  ! number of Bloch bands
  COMPLEX(dp), ALLOCATABLE  :: u_dis(:,:,:)           ! Bloch -> disentangled

  ! From seedname_u.mat
  INTEGER                   :: nwan                   ! number of Wannier funcs
  COMPLEX(dp), ALLOCATABLE  :: u_wan(:,:,:)           ! disentangled -> Wannier

  ! Bloch-to-Wannier matrices for each k-point:
  !   u_dis(nbnd,nwan,nk) - Bloch to disentangled Bloch
  !   u_wan(nwan,nwan,nk) - disentangled Bloch to Wannier
  !   u_loc(nbnd,nwan,nk) - Bloch to Wannier (u_dis * u_wan)
  COMPLEX(dp), ALLOCATABLE  :: u_loc(:,:,:)           ! Bloch -> wannier

  ! From seedname_centres.xyz
  REAL(dp), ALLOCATABLE     :: lwfcen(:,:)

  CONTAINS

  SUBROUTINE set_seed(seednamin)
    ! Set the seedname
    IMPLICIT NONE

    ! Parameters
    CHARACTER(LEN=80), INTENT(IN)   :: seednamin

    seedname = TRIM(seednamin)

    RETURN
  END SUBROUTINE


  SUBROUTINE read_nnkp()
    ! Read the following wannier90 variables from seedname.nnkp:
    !   Real-space lattice        atwan(3,3)
    !   Reciprocal-space lattice  bgwan(3,3)
    !   Number of k-points        nkwan
    !   Coordinates of k-points   xkwan(3,nkwan)
    !   Number of excluded bands  num_exc_bands
    !   Excluded band indices     exc_bands(num_exc_bands)

    USE utils,    ONLY : scan_to_begin

    IMPLICIT NONE

    ! Externals
    INTEGER, EXTERNAL :: find_free_unit

    ! Locals
    INTEGER           :: iun_nnkp, ios
    INTEGER           :: i, j
    LOGICAL           :: lflag

    INQUIRE(file=trim(seedname)//".nnkp", exist=lflag)
    IF (.NOT. lflag) THEN
      CALL errore( 'read_nnkp', 'Could not find the file '&
          &//trim(seedname)//'.nnkp', 1 )
    ENDIF

    iun_nnkp = find_free_unit()
    OPEN(UNIT = iun_nnkp, FILE = trim(seedname)//".nnkp", &
         FORM = "formatted", STATUS = "old", IOSTAT = ios)
    IF (ios /= 0) THEN
      CALL errore("read_nnkp", "Cannot find file "//trim(seedname)//".nnkp", &
                  1)
    END IF
    
    ! Read in the real lattice
    CALL scan_to_begin(iun_nnkp, 'real_lattice', lflag)
    IF (.NOT. lflag) THEN
      CALL errore('read_nnkp', 'No real_lattice block in '&
                  &//trim(seedname)//'.nnkp', 1)
    END IF
    DO j = 1, 3
      READ(iun_nnkp,*) (atwan(i, j), i = 1, 3)
    END DO

    ! Read in the reciprocal lattice
    CALL scan_to_begin(iun_nnkp, 'recip_lattice', lflag)
    IF (.NOT. lflag) THEN
      CALL errore('read_nnkp', 'No recip_lattice block in' &
                  &//trim(seedname)//'.nnkp', 1)
    END IF
    DO j = 1, 3
      READ(iun_nnkp, *) (bgwan(i,j), i = 1, 3)
    END DO

    ! Read in the k-points
    CALL scan_to_begin(iun_nnkp, 'kpoints', lflag)
    IF (.NOT. lflag) THEN
      CALL errore('read_nnkp', 'No kpoints block in' &
                  &//trim(seedname)//'.nnkp', 1)
    END IF
    READ(iun_nnkp, *) nkwan

    IF (ALLOCATED(xkwan)) DEALLOCATE(xkwan)
    ALLOCATE(xkwan(3, nkwan))

    DO j = 1, nkwan
      READ(iun_nnkp, *) (xkwan(i, j), i = 1, 3)
    END DO

    ! Read in the excluded bands
    CALL scan_to_begin(iun_nnkp, 'exclude_bands', lflag)
    IF (.NOT. lflag) THEN
      CALL errore('read_nnkp', 'No exclude_bands block in' &
                  &//trim(seedname)//'.nnkp', 1)
    END IF
    READ(iun_nnkp, *) num_exc_bands

    IF (num_exc_bands > 0) THEN
      IF (ALLOCATED(exc_bands)) DEALLOCATE(exc_bands)
      ALLOCATE(exc_bands(num_exc_bands))
      DO i = 1, num_exc_bands
        READ(iun_nnkp, *) exc_bands(i)
      END DO
    END IF

    CLOSE(iun_nnkp)
    RETURN
  END SUBROUTINE read_nnkp


  SUBROUTINE read_u_dis()
    ! Read the Bloch-to-disentangled-Bloch matrices:
    !   u_dis(num_band, num_wann, nkwan)
    ! from seedname_u_dis.mat
    ! Also sets the module variable nbwan, the number of Bloch bands per k-point

    IMPLICIT NONE

    ! Externals
    INTEGER, EXTERNAL               :: find_free_unit

    ! Locals
    INTEGER                         :: iun_u_dis, ios
    CHARACTER(LEN=512)              :: charbuff
    INTEGER                         :: num_kpts, num_wann, num_band
    REAL(dp)                        :: k_coord(3)
    INTEGER                         :: ik, i, j

    iun_u_dis = find_free_unit()

    OPEN(UNIT = iun_u_dis, FILE = trim(seedname)//"_u_dis.mat", &
         FORM = "formatted", STATUS = "old", IOSTAT = ios)

    IF (ios /= 0) RETURN                    ! No disentanglement? Do nothing

    ldis = .TRUE.                           ! Disentanglement flag

    READ(iun_u_dis, '(a)') charbuff         ! first line: date
    READ(iun_u_dis, '(3i)') num_kpts, num_wann, num_band

    nbwan = num_band                        ! module var: number of Bloch bands

    IF (ALLOCATED(u_dis)) DEALLOCATE(u_dis)
    ALLOCATE(u_dis(num_band, num_wann, num_kpts))

    DO ik = 1, num_kpts
      READ(iun_u_dis, '(a)') charbuff       ! empty line for each k-point
      READ(iun_u_dis, '(f15.10,sp,f15.10,sp,f15.10)') k_coord(3)
      READ(iun_u_dis, '(f15.10,sp,f15.10)') &
          ((u_dis(i, j, ik), i = 1, num_band), j = 1, num_wann) 
    END DO

    CLOSE(iun_u_dis)
    RETURN
  END SUBROUTINE read_u_dis


  SUBROUTINE read_u_wan()
    ! Read the (disentangled) Bloch-to-Wannier transformation matrices:
    !   u_wan(num_wann, num_wann, num_kpts)
    ! from seedname_u.mat
    ! Also sets the module variable nwan, the number of Wannier functions per
    !   unit cell.
    IMPLICIT NONE

    ! Externals
    INTEGER, EXTERNAL               :: find_free_unit

    ! Locals
    INTEGER                         :: iun_u_wan, ios
    CHARACTER(LEN=512)              :: charbuff
    INTEGER                         :: num_kpts, num_dbnd, num_wann
    REAL(dp)                        :: k_coord(3)
    INTEGER                         :: ik, i, j

    iun_u_wan = find_free_unit()

    OPEN(UNIT = iun_u_wan, FILE = trim(seedname)//"_u.mat", &
         FORM = "formatted", STATUS="old", IOSTAT = ios)
    IF (ios /= 0) THEN
      CALL errore("read_u_wan", "File "//trim(seedname)//"_u.mat not found", 1)
    END IF

    READ(iun_u_wan, '(a)') charbuff                      ! first line is date
    READ(iun_u_wan, '(3i)') num_kpts, num_dbnd, num_wann ! num_dbnd == num_wann

    ! Set number of Wannier functions (and Bloch bands if no disentanglement)
    IF (.NOT. ldis) nbwan = num_dbnd
    nwan = num_wann

    IF(ALLOCATED(u_wan)) DEALLOCATE(u_wan)
    ALLOCATE(u_wan(num_wann, num_wann, num_kpts))

    DO ik = 1, num_kpts
      READ(iun_u_wan, '(a)') charbuff                 ! Empty line at each k
      READ(iun_u_wan, '(f15.10,sp,f15.10,sp,f15.10)') k_coord 
      READ(iun_u_wan, '(f15.10,sp,f15.10)') &
          ((u_wan(i, j, ik), i = 1, num_wann), j = 1, num_wann)
    END DO

    CLOSE(iun_u_wan)
    RETURN
  END SUBROUTINE read_u_wan


  SUBROUTINE calc_u_loc()
    ! Calculate the overall Wannierization matrix u_loc = u_dis * u_wan,
    !   which maps Bloch -> Wannier orbitals

    IMPLICIT NONE

    ! Locals
    INTEGER                     :: ik

    IF(ALLOCATED(u_loc)) DEALLOCATE(u_loc)
    ALLOCATE(u_loc(nbwan, nwan, nkwan))

    IF (ldis) THEN      ! disentanglement
      DO ik = 1, nkwan
        CALL zgemm('N', 'N', nbwan, nwan, nwan, (1.d0, 0.d0), u_dis(:, :, ik), &
                  nbwan, u_wan(:, :, ik), nwan, (0.d0, 0.d0), u_loc(:, :, ik), &
                  nbwan)
      END DO
    ELSE                ! no disentanglement
      u_loc(:, :, :) = u_wan(:, :, :)
    END IF
  END SUBROUTINE calc_u_loc


  SUBROUTINE save_u_loc(seed)

    IMPLICIT NONE

    CHARACTER(len=80), INTENT(IN)       :: seed
    INTEGER, EXTERNAL                   :: find_free_unit
    INTEGER                             :: iunit, ik, i, j

    iunit = find_free_unit()
    OPEN(unit=iunit,file=trim(seed)//'_u_loc.mat',form='formatted')
    DO ik = 1, nkwan
        WRITE(iunit,*) ! newline
        WRITE(iunit,'(f15.10,sp,f15.10,sp,f15.10)') xkwan(:,ik)
        WRITE(iunit,'(f15.10,sp,f15.10)') ((u_loc(i,j,ik), i=1,nbwan), j=1,nwan)
    END DO
    CLOSE(iunit)
  END SUBROUTINE save_u_loc


  SUBROUTINE read_wannier_centres()
    ! Read in the Wannier centres lwfcen(1:3,nwan) from seedname_centres.xyz
    ! Requires that nwan be set previously

    IMPLICIT NONE

    ! Externals
    INTEGER, EXTERNAL               :: find_free_unit

    ! Locals
    CHARACTER(LEN=512)              :: charbuff
    INTEGER                         :: iun_lwfcen, ios
    INTEGER                         :: ncens              ! Number of centers
    INTEGER                         :: iwan
    REAL(dp)                        :: rsc1(3), xkw(3)

    IF(ALLOCATED(lwfcen)) DEALLOCATE(lwfcen)
    ALLOCATE(lwfcen(3, nwan))

    iun_lwfcen = find_free_unit()
    OPEN(UNIT = iun_lwfcen, FILE = trim(seedname)//"_centres.xyz", &
         FORM = "formatted", STATUS = "old", IOSTAT = ios)
    IF (ios /= 0) THEN
      CALL errore("read_wannier_centres", "Error opening "//trim(seedname)// & 
                  "_centres.xyz!", 1)
    END IF

    READ(iun_lwfcen, '(i)', ERR = 701, IOSTAT = ios) ncens  ! num_wann + n_atoms
    READ(iun_lwfcen, '(a)', ERR = 701, IOSTAT = ios) charbuff ! date

    DO iwan = 1, nwan
      READ(iun_lwfcen, '(a4,3(1x,f16.8))', ERR = 701, IOSTAT = ios) &
           charbuff, lwfcen(1:3, iwan)
      ! Some checking here
      IF (charbuff /= 'X') CALL errore("read_wannier_centres", &
                                       "Wannier centre missing - wrong nat?", 1)
    END DO

    ! There should be atoms at the end
    READ(iun_lwfcen, '(a4,3(1x,f16.8))', END = 702, ERR = 702, IOSTAT = ios) &
         charbuff, xkw(1:3)
    IF (charbuff == 'X') CALL errore("read_wannier_centres", &
                                     "Extra Wannier centre - wrong nat?", 1)

    RETURN

    701 CALL errore("read_wannier_centres", "Error reading "//trim(seedname)// &
                    "_centres.xyz", ABS(ios)) 
    702 CLOSE(iun_lwfcen)
  END SUBROUTINE read_wannier_centres


  SUBROUTINE wan2losc_print(iprint)
    ! Print the information read from wannier90

    IMPLICIT NONE

    ! Parameters
    INTEGER, INTENT(IN)             :: iprint       ! verbosity

    ! Locals
    INTEGER                         :: ib1

    IF (iprint > 0) THEN
      WRITE(6,*) "disentanglement = ", ldis
      WRITE(6,*) "nwan = ", nwan, "nbwan = ", nbwan
    END IF

    IF (iprint > 9) THEN
      ! Print Wannier centers
      WRITE(6,*) "LWF centers:"
      DO ib1 = 1, nwan
          WRITE(6,'(7x,i4," cart: <",2(f12.5,1x),f12.5,">")'), &
                ib1, lwfcen(1:3,ib1)
      END DO

    END IF
  END SUBROUTINE wan2losc_print 

  SUBROUTINE dealloc_wannier_unitaries()
    IMPLICIT NONE

    IF (ALLOCATED(u_dis)) DEALLOCATE(u_dis)
    IF (ALLOCATED(u_wan)) DEALLOCATE(u_wan)
    IF (ALLOCATED(u_loc)) DEALLOCATE(u_loc)

    RETURN
  END SUBROUTINE dealloc_wannier_unitaries

END MODULE wan2losc
!  SUBROUTINE read_nnkp_old(pwcheck) ! RESULT(excbands)
!    !-----------------------------------------------------------------------
!    !
!  
!    USE io_global, ONLY: stdout, ionode, ionode_id
!    USE kinds,     ONLY: DP
!    USE constants, ONLY: eps6, tpi, bohr => BOHR_RADIUS_ANGS
!    use cell_base, only: get_volume
!    use lsda_mod,  only: nspin
!    USE mp,        ONLY : mp_bcast, mp_sum
!    USE mp_global, ONLY : intra_pool_comm
!    USE mp_world,  ONLY : world_comm
!    !! wannier module is where seedname originates
!    USE sys, only: prlvl, at, bg, alat, nkstot, nks, xkg, xk
!    USE sys, only: nbnd, nexbnd, dbnd, dis_bndi, exc_band, &
!                        lwfexc, losc_set_cell_base
!    USE utils, only: scan_to_begin
!
!    IMPLICIT NONE
!    ! external
!    INTEGER, EXTERNAL :: find_free_unit
!    ! Params
!    LOGICAL, INTENT(IN), OPTIONAL :: pwcheck ! if .true. check system settings match sys
!    ! Locals
!    !LOGICAL :: excbands
!    LOGICAL :: lpwcheck, lsetk, lsetat
!    !  real(DP) :: g_(3), gg_
!    INTEGER :: ik, ib, ig, ipol, iw, idum, indexb
!    INTEGER numk, i, j, iun_nnkp, itmp1, itmp2, itmp3
!    INTEGER, ALLOCATABLE :: ig_check(:,:)
!    real(DP) :: xx(3), xnorm, znorm, coseno
!    LOGICAL :: have_nnkp,found
!    ! vv: tmp integer for reading in SCDM info 
!    INTEGER :: scdm_proj_tmp 
!    ! stand in for wannier module vars:
!    real(DP)              :: rlatt(3,3),glatt(3,3), r1, r2
!    integer :: nexband
!    ! debug
!    character(len=256) :: charbuff, cbuff2
!
!    !seedname  = 'wan
!
!    lpwcheck = .false.
!    IF( PRESENT(pwcheck) ) THEN
!      lpwcheck = pwcheck
!    END IF
!
!    IF (ionode) THEN  ! Read nnkp file on ionode only
!
!        INQUIRE(file=trim(seedname)//".nnkp",exist=have_nnkp)
!        IF(.not. have_nnkp) THEN
!          CALL errore( 'read_nnkp', 'Could not find the file '&
!              &//trim(seedname)//'.nnkp', 1 )
!        ENDIF
!
!        iun_nnkp = find_free_unit()
!        OPEN (unit=iun_nnkp, file=trim(seedname)//".nnkp",form='formatted', status="old")
!
!        !   check the information from *.nnkp with the nscf_save data
!        if(lpwcheck) WRITE(stdout,*) ' Checking info from wannier.nnkp file'
!
!        lsetat = sum(abs(at)) .eq. 0.0_dp
!        CALL scan_to_begin(iun_nnkp,'real_lattice',found)
!        if(.not.found) then
!          CALL errore( 'read_nnkp', 'Could not find real_lattice block in '&
!              &//trim(seedname)//'.nnkp', 1 )
!        endif
!        DO j=1,3
!        READ(iun_nnkp,*) (rlatt(i,j),i=1,3)
!        !read(iun_nnkp,*) charbuff ! debug line
!        ENDDO
!        if( lsetat )then
!          alat = sum(abs(rlatt(:,1)))/bohr ! hack, only works for ibrav={1,2,3,4,6,8}
!          ! test if the hack worked without checking ibrav (it may not be set)
!          r1 = maxval(abs(rlatt(:,1)))
!          do i=1, 3 ! hack works if all elments of at(:,1) are the same or zero
!            if( abs(r1-abs(rlatt(i,1))).gt.1.0e-08_dp .and. &
!                abs(rlatt(i,1)).gt.1.0e-08_dp )  &
!              call errore('read_nnkp','unsupported ibrav detected',1)
!          end do
!          rlatt(:,:) = rlatt(:,:) / (alat*bohr)
!          if(prlvl.gt.1)then
!            r1 = get_volume( rlatt ) * alat**3
!            write(stdout,'(3x,a,f10.4,a,f10.4)') "- cell info set in read_nnkp, alat: ", &
!                  alat, ", omega: ", r1
!          end if
!        end if
!
!        IF( lpwcheck )THEN
!          rlatt(:,:) = rlatt(:,:)/(alat*bohr)
!          DO j=1,3
!            DO i=1,3
!              IF(abs(rlatt(i,j)-at(i,j))>eps6) THEN
!                  WRITE(stdout,*)  ' Something wrong! '
!                  WRITE(stdout,*)  ' rlatt(i,j) =',rlatt(i,j),  ' at(i,j)=',at(i,j)
!                  write(6,*) alat, bohr
!                  write(6,*) rlatt(:,1); write(6,*) rlatt(:,2); write(6,*) rlatt(:,3)
!                  write(6,*) at(:,1); write(6,*) at(:,2); write(6,*) at(:,3)
!                  CALL errore( 'wan2losc', 'Direct lattice mismatch', 3*j+i )
!              ENDIF
!            ENDDO
!          ENDDO
!          WRITE(stdout,*) ' - Real lattice is ok'
!        END IF
!
!        CALL scan_to_begin(iun_nnkp,'recip_lattice',found)
!        if(.not.found) then
!            CALL errore( 'wan2losc', 'Could not find recip_lattice block in '&
!                &//trim(seedname)//'.nnkp', 1 )
!        endif
!        DO j=1,3
!            READ(iun_nnkp,*) (glatt(i,j),i=1,3)
!        ENDDO
!        if( lsetat )then
!          glatt(:,:) = (alat*bohr/tpi)*glatt(:,:)
!        end if
!        IF( lpwcheck )THEN
!          glatt(:,:) = (alat*bohr)*glatt(:,:)/tpi
!          DO j=1,3
!              DO i=1,3
!                IF(abs(glatt(i,j)-bg(i,j))>eps6) THEN
!                    WRITE(stdout,*)  ' Something wrong! '
!                    WRITE(stdout,*)  ' glatt(i,j)=',glatt(i,j), ' bg(i,j)=',bg(i,j)
!                    CALL errore( 'wan2losc', 'Reciprocal lattice mismatch', 3*j+i )
!                ENDIF
!              ENDDO
!          ENDDO
!          WRITE(stdout,*) ' - Reciprocal lattice is ok'
!        END IF
!
!        if( lsetat .and. prlvl.gt.1 )then
!          !write(6,'(4x,"Unit cell info:")')
!          write(6,'(3(5x," at(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i, rlatt(1:3,i), i=1,3)
!          !write(6,'(3(5x," bg(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i, glatt(1:3,i), i=1,3)
!          !write(6,'(/,4x,"Unit cell info:",/)')
!          !write(6, '(3(5x," at(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i, rlatt(1:3,i), i=1,3)
!          !write(6, '(3(5x," bg(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i, glatt(1:3,i), i=1,3)
!        endif
!
!        CALL scan_to_begin(iun_nnkp,'kpoints',found)
!        if(.not.found) then
!          CALL errore( 'wan2losc', 'Could not find kpoints block in '&
!              &//trim(seedname)//'.nnkp', 1 )
!        endif
!        READ(iun_nnkp,*) numk
!        IF(numk * nspin /= nkstot .and. lpwcheck ) THEN
!          WRITE(stdout,*)  ' Something wrong! '
!          WRITE(stdout,*)  ' numk=',numk, ' nkstot=',nkstot
!          CALL errore( 'read_nnkp', 'Wrong number of k-points', numk)
!        ENDIF
!
!        ! 2022-03-30: Set for removal. nks, xkg should be set from pwscf
!        !   currently I think only xk has that happen
!        ! Set nks and xkg if unset
!        lsetk = .false.
!        !IF( nkstot.lt.1 )THEN
!        IF (.NOT. ALLOCATED(xkg)) THEN
!          !nkstot = numk
!          !nks = numk
!          lsetk = .true.
!          IF( allocated(xkg) ) deallocate(xkg)
!          allocate( xkg(3,numk) )
!        END IF
!
!        DO i=1,numk
!            READ(iun_nnkp,*) xx(1), xx(2), xx(3)
!            if( lsetk ) xkg(1:3,i) = xx(1:3)
!          IF( lpwcheck )THEN
!            CALL cryst_to_cart( 1, xx, bg, 1 )
!            IF(abs(xx(1)-xk(1,i))>eps6.or. &
!               abs(xx(2)-xk(2,i))>eps6.or. &
!               abs(xx(3)-xk(3,i))>eps6) THEN
!                WRITE(stdout,*)  ' Something wrong! '
!                WRITE(stdout,*) ' k-point ',i,' is wrong'
!                WRITE(stdout,*) xx(1), xx(2), xx(3)
!                WRITE(stdout,*) xk(1,i), xk(2,i), xk(3,i)
!                CALL errore( 'wan2losc', 'problems with k-points', i )
!          ENDIF; ENDIF
!      ENDDO
!      if(lpwcheck) WRITE(stdout,*) ' - K-points are ok'
!      if( lsetk .and. prlvl.gt.1 ) write(stdout,*) '  - k-points set in read_nnkp, nkstot: ',nkstot
!
!      CALL scan_to_begin(iun_nnkp,'exclude_bands',found)
!      if(.not.found) then
!        CALL errore( 'wan2losc', 'Could not find exclude_bands block in '&
!            &//trim(seedname)//'.nnkp', 1 )
!      endif
!      READ (iun_nnkp,*) nexband
!      nexbnd = nexband
!
!      IF( nbnd.lt.1 )THEN
!        itmp3 = 0
!        DO i=1, nexband
!            read(iun_nnkp,*,end=210) itmp1
!            IF( itmp1.gt.itmp3 ) itmp3 = itmp1
!        END DO
!        210 CONTINUE
!      ELSE
!        itmp3 = nbnd
!      END IF
!
!      IF( nexband.gt.0 )THEN
!        if( allocated(exc_band) ) deallocate(exc_band)
!        ALLOCATE( exc_band(itmp3) )
!        exc_band(:)=.false.
!        CALL scan_to_begin(iun_nnkp,'exclude_bands',found)
!        READ (iun_nnkp,*) nexband
!        DO i=1,nexband
!            READ(iun_nnkp,*) indexb
!            !READ(iun_nnkp,*) charbuff ! debug checking line
!            IF( (lpwcheck.and.indexb>nbnd) .or. indexb<1 ) &
!                CALL errore('read_nnkp',' wrong excluded band index ', 1)
!            exc_band(indexb)=.true.
!        ENDDO
!      END IF
!    END IF
!    
!    IF (ionode) CLOSE (iun_nnkp)   ! ionode only
!
!  #if defined(__MPI)
!      CALL mp_bcast( nexbnd, ionode_id, world_comm )
!      lwfexc = nexbnd.gt.0
!      if( nexbnd.gt.0 )then
!        CALL mp_bcast( itmp3, ionode_id, world_comm )
!        if( .not.allocated(exc_band) ) allocate( exc_band(itmp3) )
!        CALL mp_bcast( exc_band, ionode_id, world_comm )
!      end if
!
!      CALL mp_bcast( lsetk, ionode_id, world_comm )
!      if( lsetk )then
!        CALL mp_bcast( nks, ionode_id, world_comm )
!        if( .not.allocated(xkg) ) allocate(xkg(3,nks) )
!        CALL mp_bcast( xkg, ionode_id, world_comm )
!      end if
!
!      CALL mp_bcast( lsetat, ionode_id, world_comm )
!      if( lsetk )then
!        CALL mp_bcast( alat,  ionode_id, world_comm )
!        CALL mp_bcast( rlatt, ionode_id, world_comm )
!        CALL mp_bcast( glatt, ionode_id, world_comm )
!        r1 = get_volume( rlatt ) * alat**3
!        CALL losc_set_cell_base( r1, alat, rlatt, glatt, 0 )
!      end if
!  #endif
!    
!    RETURN
!  END SUBROUTINE read_nnkp_old