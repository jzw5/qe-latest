MODULE losc_plot

  ! 2022-04-01: Reference all the subroutines, mostly from wannier_density
  ! 2022-04-29: Currently plotting routines/vars are just dumped here;
  !             not expected to work

    USE kinds, ONLY : dp

  IMPLICIT NONE

  ! plotting vars
  real(dp)           :: x0(3)      ! plot origin
  real(dp)           :: e1(3)      ! plot direction
  integer            :: nx         ! number of plot points
  integer            :: kband(2)   ! LBO bands to plot
  integer            :: kpoint(2)  ! LBO k-points to plot
  integer            :: lband(2)   ! LWF bands to plot
  integer            :: lcell(2)   ! LWF cell to plot
  logical            :: lplot_lwf ! LWF plot bool
  logical            :: lplot_lbo ! LBO plot bool

  CONTAINS

  subroutine losc_set_plotvars(x0i, e1i, nxi, kbndi, kpnti, lbndi, lclli, lpwfi, lpboi)
    real(dp), intent(in):: x0i(3)      ! plot origin
    real(dp), intent(in):: e1i(3)      ! plot direction
    integer,  intent(in):: nxi         ! number of plot points
    integer,  intent(in):: kbndi(2)   ! LBO bands to plot
    integer,  intent(in):: kpnti(2)  ! LBO k-points to plot
    integer,  intent(in):: lbndi(2)   ! LWF bands to plot
    integer,  intent(in):: lclli(2)   ! LWF cell to plot
    logical,  intent(in) :: lpwfi ! LWF plot bool
    logical,  intent(in) :: lpboi ! LBO plot bool

    x0 = x0i
    e1 = e1i
    nx = nxi
    kband = kbndi
    kpoint = kpnti
    lband = lbndi
    lcell = lclli
    lplot_lwf = lpwfi
    lplot_lbo = lpboi

  end subroutine losc_set_plotvars

  SUBROUTINE supercell_fft_plot()
    ! Plot the dwf using the supercell FFT PW expansion
    USE sys, ONLY: lplot_lwf, lband
    USE sys, ONLY: dwfft, afft
    USE supercell_fft, ONLY: supercell_fft_rho_r2g

    INTEGER :: ip, psz  ! plotting index
    INTEGER :: ib1, id1 ! band index, dwfn0 index(==1)
    INTEGER :: ig

    psz = lband(2) - lband(1) + 1
    ip = 1

    ifPlot: IF( lplot_lwf ) THEN

        CALL losc_alloc_dwf(1)

        DO ip = 1, psz
            ib1 = lband(1) + (ip-1)

            id1 = losc_get_dwfn0(ib1)          ! Load
            CALL dwf_uc2sc( afft, id1 )        ! convert uc2sc storage
            CALL supercell_fft_rho_r2g(afft, dwfft(:,id1))

        !CALL fwfft('Rho', afft, dfftlosc ) ! FFT
        ! explicitly do fftx_threed2oned for no spin, no norm
        !do ig = 1, dfftlosc%ngm 
        !    dwfft(ig,id1) = afft(dfftlosc%nl(ig))
        !end do

            CALL supercell_fft_plot_wan(dwfft(:,id1),ib1)
        END DO
      
        CALL losc_dealloc_dwf()
    END IF ifPlot

  END SUBROUTINE supercell_fft_plot


  SUBROUTINE supercell_fft_plot_wan(dwfg, ibin)
    ! plot LWF density along a 1D line
    USE constants, ONLY : tpi
    USE sys,  ONLY: x0, e1, nx, lcell, at, nkdm, omegalosc
    USE supercell_fft,  ONLY: ngmlosc_g, glosc
    ! Debug norm checking stuff
    USE sys,  ONLY: dwfn0, omega, nksz, nrsz, rsgr, mcvec1d
    IMPLICIT NONE
    INTEGER,     EXTERNAL :: find_free_unit
    ! Params
    !INTEGER,     intent(in) :: ounit ! output unit
    COMPLEX(dp), INTENT(IN) :: dwfg(:) ! dim(ngm)
    integer,     INTENT(IN) :: ibin
    ! Locals
    CHARACTER(LEN=256) :: filename   ! for opening files
    INTEGER     :: ounit, ios
    INTEGER     :: i1, igv
    REAL(dp)    :: m1, mbnx
    REAL(dp)    :: e2(3) ! e1 divided
    REAL(dp)    :: xp(3) ! x in primitive vectors
    REAL(dp)    :: r1, ri1, xfac
    REAL(dp)    :: gnorm
    COMPLEX(dp) :: c1, c2, c3

    INTEGER :: iuc, ir

    CALL start_clock( 'plot_wan' )

    ounit = find_free_unit()
    WRITE(filename, '(1(A,I0.3),A)') 'plotDWF_B', ibin, '.dat' 
    OPEN(unit=ounit, file=trim(filename), form='formatted', status='unknown', iostat=ios)
    IF( ios.ne. 0) CALL errore('SUPERCELL_FFT_PLOT_WAN', 'ERR opening '//trim(filename), 2)

    m1 =  sqrt( e1(1)**2 + e1(2)**2 + e1(3)**2 ) !/ real(nkdm(1),dp)
    IF (abs(m1) < 1.0e-6_dp ) THEN
        e1(:) = at(:,1)
        m1 =  sqrt( e1(1)**2 + e1(2)**2 + e1(3)**2 )
    END IF
    mbnx = m1 / real(nx, dp)
    e2(:) = e1(:) / real(nx-1, dp) ! divide into deltax elements

    xfac = tpi / real(nkdm(1),dp)
    gnorm = 0.0_dp
    DO igv = 1, ngmlosc_g
        gnorm = gnorm + abs(dwfg(igv))**2
    END DO
    gnorm = 1.0_dp !/ sqrt( omegalosc )!* gnorm )

    
    WRITE(ounit,'(7(a20,1x))') 'total dist.', 'x-position', 'y-position', &
                            'z-posiiton', 'Re{DWF}', 'Im{DWF}', '|DWF|'

    plotdo2: DO i1 = 1, nx
    
        ri1 = real(i1-1, dp)
        xp(1:3) = xfac * ( x0(1:3) + ri1*e2(1:3) )
    
        c1 = CMPLX(0.0_dp)
        DO igv = 1, ngmlosc_g
            r1 = SUM( xp(1:3) * glosc(1:3,igv) )
            c1 = c1 + ( dwfg(igv) * CMPLX(COS(r1),SIN(r1)) )
        END DO
        c1 = c1 * gnorm

        ! write to out
        WRITE(ounit,'(7(f20.10,x))') ri1 * mbnx, &
                    xp(1:3)/xfac, c1, ABS(c1)

    END DO plotdo2

    CLOSE(ounit)

    WRITE(6,'(1x,"gnorm:   ",g20.10)') gnorm
    WRITE(6,'(1x,"rhog(1): ",2(g20.10,sp),"i")') dwfg(1)

    ! sum over the SC grid to see if we're normalized
    WRITE(6,'(1x,"rnorm: ",g20.10)') omega / REAL(nrsz,dp)
    c3 = 0.0_dp
    DO iuc = 1, nksz
        r1 = 0.0_dp
        DO ir = 1, nrsz
            r1 = r1 + dwfn0(ir,iuc,1)
        END DO
        WRITE(6,'(1x,g10.3)',advance='no') r1; CALL flush(6)
        c3 = c3 + r1
    END DO
    WRITE(6,*)
    WRITE(6,'(2x,i3,2(f20.10,1x),g20.10,sp,g20.10,"i")') ibin, REAL(c3)

    CALL stop_clock( 'plot_wan' )
  END SUBROUTINE supercell_fft_plot_wan


  SUBROUTINE findset_plotvars(lplot)
    ! find plotting variables from plot namelist vars from stdin
    ! set corresponding sys variables
    USE sys, ONLY: losc_set_plotvars, at, dbnd, nksz, spin_offset
    ! Params
    LOGICAL, INTENT(in) :: lplot     ! wheter to plot at all
    ! Locals
    ! &plot namelist vars
    REAL(dp)           :: x0(3)      ! plot origin
    REAL(dp)           :: e1(3)      ! plot direction
    INTEGER            :: nx         ! number of plot points
    INTEGER            :: kband(2)   ! LBO bands to plot
    INTEGER            :: kpoint(2)  ! LBO k-points to plot
    INTEGER            :: lband(2)   ! LWF bands to plot
    INTEGER            :: lcell(2)   ! LWF cell to plot
    ! other plotting vars
    REAL(dp)           :: m1
    LOGICAL            :: lplot_lwf ! LWF plot bool
    LOGICAL            :: lplot_lbo ! LBO plot bool

    ! I/O variables
    INTEGER            :: ios

    NAMELIST / plot / kband, kpoint, lband, lcell, e1, x0, nx

    ! default plotting vars
    lplot_lbo   = .false.
    lplot_lwf   = .false.
    kband(1:2)  = 0
    kpoint(1:2) = 0
    lband(1:2)  = 0
    lcell(1:2)  = 0
    nx = 500
    e1(1:3) = 0.0_dp
    x0(1:3) = 0.0_dp

    IF( .not.lplot )THEN ! set default vars
        CALL losc_set_plotvars(x0, e1, nx, kband, kpoint, lband, lcell, &
                               lplot_lwf, lplot_lbo)
        RETURN
    END IF
        
    READ (5, plot, err = 602, iostat = ios)

    m1 =  sqrt( e1(1)**2 + e1(2)**2 + e1(3)**2 )
    IF (abs(m1) < 1.0e-6_dp ) THEN
        e1(:) = at(:,1)
        m1 =  sqrt( e1(1)**2 + e1(2)**2 + e1(3)**2 )
    END IF

    ! LWF plot setup & error checking
    IF ( lband(2).lt.lband(1) ) lband(2) = lband(1)
    IF ( lcell(2).lt.lcell(1) ) lcell(2) = lcell(1)
    IF ( (lband(1).eq.0) .and. (lcell(1).ne.0) ) THEN
    ! if LWF cell is specified but band is not, plot all bands
        lband(1) = 1
        lband(2) = nwan
    END IF
    IF ( (lcell(1).eq.0) .and. (lband(1).ne.0) ) THEN
    ! if LWF band is specified but cell is not, plot home cell
        lcell(1) = 1
        lcell(2) = 1
    END IF
    IF ( lband(1) .ge. 1) THEN
        lplot_lwf = .true.
        IF ( (lband(2).gt.nwan) .or. (lband(1).lt.1) ) THEN
            WRITE(6,'("ERR: invalid LWF band(s) to plot: ",2(i2,x))') &
                    lband(1:2)
            CALL errore('LOSC', 'ERR: invalid LWF bands(s) to plot',1)
        END IF
        IF ( (lcell(2).gt.nksz) .or. (lcell(1).lt.1) ) THEN
            WRITE(6,'("ERR: invalid LWF cell(s) to plot: ",2(i2,x))') &
                    lcell(1:2)
            CALL errore('LOSC', 'ERR: invalid LWF cell(s) to plot',1)
        END IF
    END IF

    ! LBO plot setup & error checking
    IF ( kband(2) .lt. kband(1) ) kband(2) = kband(1)
    IF ( kpoint(2) .lt. kpoint(1) ) kpoint(2) = kpoint(1)
    IF ( (kpoint(1).eq.0) .and. (kband(1).ne.0) ) THEN
    ! if kband specified but kpoint is not, plot all kpoints
        kpoint(1) = 1 + spin_offset
        kpoint(2) = nksz + spin_offset
    END IF
    IF ( (kband(1).eq.0) .and. (kpoint(1).ne.0) ) THEN
    ! if kpoint specified but kband is not, plot all bands
        kband(1) = 1
        kband(2) = dbnd
    END IF
    IF ( kband(1) .ge. 1) THEN
        lplot_lbo = .TRUE.
        IF ( (kpoint(2).gt.nksz) .or. (kpoint(1).lt.1) ) THEN
            WRITE(6,'("ERR: invalid kpoint to plot")')
            STOP
        END IF
        IF ( (kband(2).gt.dbnd) .or. (kband(1).lt.1) ) THEN
            WRITE(6,'("ERR: invalid kband to plot")')
            STOP
        END IF
    END IF

    IF ( iprint .gt. 0 ) THEN
        IF ( lplot_lwf ) THEN
            WRITE(6,'(5x,"LWF bands to plot: ",2(i3,x))') lband(1:2)
            WRITE(6,'(5x,"      \=> & cells: ",2(i3,x))') lcell(1:2)
        END IF
        IF ( lplot_lbo ) THEN
            WRITE(6,'(5x,"LBO bands to plot: ",2(i3,x))') kband(1:2)
            WRITE(6,'(5x,"   \=> & k-points: ",2(i3,x))') kpoint(1:2) 
        END IF
        IF ( lplot_lwf .or. lplot_lbo ) THEN
            WRITE(6,'(5x,"Plot origin: <",2(f8.2,x),f8.2,">")') x0
            WRITE(6,'(5x,"Plot vector: <",2(f8.2,x),f8.2,">")') e1
            WRITE(6,'(5x,"Plot length:  ",f20.10)') m1
        END IF
    END IF

    CALL losc_set_plotvars(x0, e1, nx, kband, kpoint, lband, lcell, lplot_lwf, lplot_lbo)

    RETURN
    602  CALL errore( 'LOSC', 'ERR reading plot namelist', ABS (ios) )
  END SUBROUTINE findset_plotvars

END MODULE losc_plot