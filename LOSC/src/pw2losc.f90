! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE pw2losc
! Read pw.x output into LOSC variables
  USE kinds,           ONLY : dp

  IMPLICIT NONE

  ! From pwcom:klist
  INTEGER                   :: nks, nkstot          ! (local, global) # k-points
  REAL(dp)                  :: nelec, nelup, neldw  ! total, up, down electrons
  INTEGER, ALLOCATABLE      :: ngk(:)               ! plane waves @ k-point 
  INTEGER, ALLOCATABLE      :: igk_k(:, :)          ! G index at given (k + G)
  REAL(dp), ALLOCATABLE     :: xk(:, :), wk(:)      ! k-point coords, weights
  LOGICAL                   :: two_fermi_energies   ! TRUE if spin-polarized

  ! From pwcom::lsda_mod
  INTEGER                   :: nspin = 1            ! number of spin components
  INTEGER, ALLOCATABLE      :: isk(:)               ! 1: k-point is up, 2: dw

  ! From pwcom::wvfct
  INTEGER                   :: nbnd                 ! number of electron bands
  INTEGER                   :: npwx                 ! maximum plane waves/k
  REAL(dp), ALLOCATABLE     :: et(:, :)             ! Kohn-Sham eigenvalues
  REAL(dp), ALLOCATABLE     :: wg(:, :)             ! (k-point, band) weights

  CONTAINS

  SUBROUTINE read_pwscf(iprint)
    ! Read in variables from pwcom
    USE parameters,     ONLY : npk

    USE io_files,       ONLY : prefix

    USE klist,          ONLY : nks_in => nks, nkstot_in => nkstot, &
                               nelec_in => nelec, &
                               nelup_in => nelup, neldw_in => neldw, &
                               xk_in => xk, wk_in => wk, ngk_in => ngk, &
                               igk_k_in => igk_k, &
                               twofermi_in => two_fermi_energies

    USE lsda_mod,       ONLY : nspin_in => nspin, isk_in => isk

    USE wvfct,          ONLY : nbnd_in => nbnd, npwx_in => npwx, &
                               et_in => et, wg_in => wg

    IMPLICIT NONE

    INTEGER, INTENT(IN)     :: iprint

    CALL start_clock("read_pwscf")

    ! Open pwscf files
    CALL read_file
    CALL openfil_losc()

    npwx = npwx_in
    two_fermi_energies = twofermi_in
    nks = nks_in
    nkstot = nkstot_in
    nelec = nelec_in
    nelup = nelup_in
    neldw = neldw_in

    IF (ALLOCATED(xk)) DEALLOCATE(xk)
    ALLOCATE(xk(3, nkstot))
    xk(:, 1:nkstot) = xk_in(:, 1:nkstot)

    IF (ALLOCATED(wk)) DEALLOCATE(wk)
    ALLOCATE(wk(nkstot))
    wk(1:nkstot) = wk_in(1:nkstot)

    IF (ALLOCATED(ngk)) DEALLOCATE(ngk)
    ALLOCATE(ngk(nkstot))
    ngk(:) = ngk_in(:)

    IF (ALLOCATED(igk_k)) DEALLOCATE(igk_k)
    ALLOCATE(igk_k(npwx, nkstot))
    igk_k(:, :) = igk_k_in(:, :)

    nspin = nspin_in

    ALLOCATE(isk(nkstot))
    isk(1:nkstot) = isk_in(1:nkstot)

    nbnd = nbnd_in
    npwx = npwx_in

    IF (ALLOCATED(et)) DEALLOCATE(et)
    ALLOCATE(et(nbnd, nkstot))
    et(:, :) = et_in(:, :)

    IF (ALLOCATED(wg)) DEALLOCATE(wg)
    ALLOCATE(wg(nbnd, nkstot))
    wg(:, :) = wg_in(:, :)

    CALL stop_clock("read_pwscf")
    CALL pw2losc_print(iprint)
    RETURN
  END SUBROUTINE read_pwscf


  SUBROUTINE dealloc_pw2losc()
    ! Deallocate all pw2losc memory 

    IMPLICIT NONE

    IF (ALLOCATED(ngk)) DEALLOCATE(ngk)
    IF (ALLOCATED(igk_k)) DEALLOCATE(igk_k)
    IF (ALLOCATED(xk)) DEALLOCATE (xk)
    IF (ALLOCATED(wk)) DEALLOCATE (wk)
    IF (ALLOCATED(et)) DEALLOCATE (et)
    IF (ALLOCATED(wg)) DEALLOCATE (wg)

    RETURN
  END SUBROUTINE dealloc_pw2losc 


  SUBROUTINE pw2losc_print(iprint)
    USE fft_base,           ONLY : dffts, dfftp

    IMPLICIT NONE
    
    INTEGER, INTENT(IN)         :: iprint         ! Print verbosity

    INTEGER                     :: ik

    IF (iprint > 0) THEN
      ! Basic output
      WRITE(6,1310) nbnd, nelec, nelup, neldw, nspin 
      WRITE(6,'(5x,"nkstot: ",i3,", nks: ",i3)') nkstot, nks
      !WRITE(6,'(5x,"nkstot: ",i3,", nks: ",i3,", nk(x,y,z): ",3(i2,1x))') &
      !      nkstot, nks, nk1, nk2, nk3
 
      WRITE(6,*) "two_fermi_energies = ", two_fermi_energies

    END IF

    IF (iprint > 2) THEN
      ! Number of plane-waves per k-point
      WRITE(6,'(5x,"ngk(1:nks)")')
      WRITE(6,'(999(5x,8(i8,2x),/))') (ngk(ik), ik=1,nkstot) ! 8 per line
      
      ! FFT comparison
      WRITE(6,*) "FFT comparison: s/p"
      WRITE(6,1311) "s/p", "ngm", "nnr", &
                  "nr1", "nr2", "nr3", "nr1x", "nr2x", "nr3x"
      WRITE(6,1312) 's', dffts%ngm, dffts%nnr, dffts%nr1, dffts%nr2, &
                      dffts%nr3, dffts%nr1x, dffts%nr2x, dffts%nr3x
      WRITE(6,1312) 'p', dfftp%ngm, dfftp%nnr, dfftp%nr1, dfftp%nr2, &
                      dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%nr3x
    END IF

    IF (iprint >= 10) THEN
      ! The coordinates of the k-points
      WRITE(6,*) "xk(1:nkstot) = "
      DO ik = 1, nkstot
        WRITE(6,*) xk(:, ik)
      END DO
    END IF

    RETURN

    1310 FORMAT(5x,'nbnd: ',i4,', nelec:',f6.1,', nup: ',f5.1,', ndw: ',& 
                f5.1,', nspin: ',i4)
    1311 FORMAT(5x,a3,1x,2(a8,1x),6(a4,1x))
    1312 FORMAT(5x,a3,1x,2(i8,1x),6(i4,1x))
  END SUBROUTINE pw2losc_print

END MODULE pw2losc
  !SUBROUTINE init_pw2losc(rdin)
  !    use sys, only: xk, at, rsgr, nrdm, nkdm, nrsz, nksz, scnrdm 
  !    use sys, only: mcvec, mcvec1d, mmci, mmcsz, mmcvec1d, mmcoff, pscshsz
  !    use utils,    only: pscsh, pscshoff, mpmsz, mci
  !    use utils,    only: losc_vec2uc

  !    implicit none
  !    ! Params
  !    integer, intent(in) :: rdin(3) ! real grid dims
  !    ! Locals
  !    integer  :: i1, i2, i3, ii, jj, kk
  !    integer  :: ix(3)
  !    integer  :: inr1, inr12 ! nr1, nr1*nr2
  !    real(dp) :: obnr(3)
  !    real(dp) :: atbnr(3)

  !    ! MPI message size: now we set to 10 in utils
  !    mpmsz = 10

  !    ! This is moved to set_tolerance() in utils
  !    ! tolerance if half a grid spacing
  !    ! @CUBIC: change at to but 1.0 for fractional
  !    !eqtol(1) = 0.5_dp / real(rdin(1),dp)
  !    !eqtol(2) = 0.5_dp / real(rdin(2),dp)
  !    !eqtol(3) = 0.5_dp / real(rdin(3),dp)

  !    call losc_ksetup(nkdm, xk) ! set the nk dims, needed for center checks

  !    nksz = product(nkdm(1:3))
  !    nrdm(:) = rdin(:)
  !    scnrdm(1:3) = nrdm(1:3) * nkdm(1:3)
  !    nrsz = product(nrdm(1:3))
  !    mmcsz = 27*nksz ! 3**3 = 27 for shell around home multi-cell
  !    allocate( mcvec1d(3, nksz) )
  !    mcvec(1:3,1:nkdm(1),1:nkdm(2),1:nkdm(3)) => mcvec1d
  !    allocate( mci(3, nksz) )

  !    ii = 0
  !    do i3 = 1, nkdm(3)
  !    do i2 = 1, nkdm(2)
  !    do i1 = 1, nkdm(1)

  !        ii = ii + 1
  !        mci(1,ii) = i1
  !        mci(2,ii) = i2
  !        mci(3,ii) = i3

  !        mcvec1d(:,ii) = ( real(i1-1,dp)*at(:,1) ) + &
  !                        ( real(i2-1,dp)*at(:,2) )+ &
  !                        ( real(i3-1,dp)*at(:,3) )
  !    end do
  !    end do
  !    end do

  !    allocate( mmcvec1d(3, mmcsz) )
  !    allocate( mmci(3, mmcsz) )

  !    ! fill in mmcvec
  !    ii = 0
  !    do i3 = -nkdm(3)+1, 2*nkdm(3)
  !    do i2 = -nkdm(2)+1, 2*nkdm(2)
  !    do i1 = -nkdm(1)+1, 2*nkdm(1)
  !        if ( (i1.eq.1).and.(i2.eq.1).and.(i3.eq.1) ) mmcoff = ii

  !        ii = ii + 1

  !        mmci(1,ii) = i1
  !        mmci(2,ii) = i2
  !        mmci(3,ii) = i3

  !        mmcvec1d(:,ii) = ( real(i1-1,dp)*at(:,1) ) + &
  !                        ( real(i2-1,dp)*at(:,2) )+ &
  !                        ( real(i3-1,dp)*at(:,3) )

  !    end do
  !    end do
  !    end do
  !    pscshsz = product( 2*nkdm(1:3) - 1 )
  !    allocate ( pscsh(3, pscshsz) )

  !    ii = 0
  !    do i3 = -nkdm(3)+1, nkdm(3)-1
  !    do i2 = -nkdm(2)+1, nkdm(2)-1
  !    do i1 = -nkdm(1)+1, nkdm(1)-1

  !        ii = ii + 1
  !        if ( (i3.eq.0) .and. (i2.eq.0) .and. (i1.eq.0) ) pscshoff = ii
  !        pscsh(1:3,ii) = ( real(i1,dp)*at(1:3,1) ) + &
  !                        ( real(i2,dp)*at(1:3,2) ) + &
  !                        ( real(i3,dp)*at(1:3,3) )

  !        call losc_vec2uc(pscsh(:,ii), ix)
  !        ix = ix + 0
  !        if ( (ix(1).ne.i1) .and. (ix(2).ne.i2) .and. (ix(3).ne.i3) ) then
  !            write(6,*) 'ERRRR: Noooope'
  !        end if

  !    end do
  !    end do
  !    end do 

  !    allocate ( rsgr(3,nrsz) ) 

  !    inr1 = rdin(1) !dffts%nr1
  !    inr12 = inr1 * rdin(2) !dffts%nr1 * dffts%nr2
  !    obnr(1:3) = 1.0_dp / real(rdin(1:3),dp)
  !    atbnr(1) = obnr(1) * at(1,1)
  !    atbnr(2) = obnr(2) * at(2,2)
  !    atbnr(3) = obnr(3) * at(3,3)

  !    do i1 = 1, nrsz

  !        ix(3) = (i1-1) / inr12 
  !        ix(2) = ( (i1-1) - ix(3)*inr12 ) / inr1
  !        ix(1) = i1 - ix(3)*inr12 - ix(2)*inr1 - 1

  !        rsgr(1:3,i1) = ( obnr(1)*real(ix(1),dp)*at(1:3,1) ) + &
  !                      ( obnr(2)*real(ix(2),dp)*at(1:3,2) ) + &
  !                      ( obnr(3)*real(ix(3),dp)*at(1:3,3) )

  !    end do

  !    return
  !end subroutine init_pw2losc


  !subroutine losc_ksetup(nkdims, kvecs)
  !    ! find kmesh 3d dims and verify it's a valid M-P mesh
  !    ! @CUBIC ERR: xk is in xyz, for ksetup to work it needs to be
  !    ! in frac coords, this is how PW/src/summary.f90 does it:
  !  !      use kinds, only : dp
  !  !      use cell_base, only: at
  !  !      USE klist,   ONLY : xk, nks !, wk, nkstot ! k coords & weights
  !        use start_k,    only : nk1, nk2, nk3 ! k-mesh dims
  !        use sys,   only : at, xk, nks
  !        use utils,      only : losc_cart2frac_rec 
  !        implicit none
  !        ! Params
  !        integer,  intent(out)   :: nkdims(3)
  !        real(dp), intent(inout) :: kvecs(:,:)
  !        ! Locals
  !        integer     :: i1, i2, i3, ii
  !        real(dp), allocatable :: xk1(:)
  !        real(dp) :: xkf(3), xk2, xk3
  !  
  !        allocate ( xk1(nks) )
  !        xk1(:) = huge(1.0_dp) ! unlikely number
  !  
  !        if ( (nk1.ne.0) .and. (nk2.ne.0) .and. (nk3.ne.0) ) then
  !            ! can probably assume correct
  !            nkdims(1) = nk1
  !            nkdims(2) = nk2
  !            nkdims(3) = nk3
  !        else
  !            ! need to find myself, count unique elements in each xk dim
  !            do i1 = 1, 3
  !  
  !                xkf = losc_cart2frac_rec(xk(:,1))
  !                nkdims(i1) = 1
  !                xk1(1) = xkf(1)
  !                doi2: do i2 = 2, nks
  !                    xkf = losc_cart2frac_rec(xk(:,i2))
  !                    xk2 = xkf(i1)
  !                    !if ( xk(i1,i2) .ne. xk1(1) ) then
  !                    doi3: do i3 = 1, nkdims(i1)
  !                        !if ( xk(i1,i2) .eq. xk1(i3) ) cycle doi2
  !                        if( abs(xk2-xk1(i3)) .lt. 1.0e-10_dp )then
  !                            cycle doi2
  !                        end if
  !                    end do doi3
  !                    nkdims(i1) = nkdims(i1) + 1
  !                    xk1(nkdims(i1)) = xk2
  !                end do doi2
  !  
  !            end do
  !  
  !        end if
  !  
  !        i1 = nkdims(1)*nkdims(2)*nkdims(3)
  !        if ( i1 .ne. nks ) then
  !            write(6,*) 'fuuuu2'
  !            write(6,'(5x,"The first ",i0," k-points: ")') min(nks,20)
  !            do i2 = 1, min(nks,20)
  !                write(6,'(5x,"k(",i3,") = <"2(g20.10,x),g20.10,">")') i2, xk(:,i2)
  !            end do
  !            ! @CUBIC
  !            !@ERR! the xk are in xyz but units of alat, need to 
  !            ! be in fractional coords for this to work
  !            write(6,'("ERR, mismatch in losc_ksetup")')
  !            write(6,'("nkdims :",3(i4,x),i0)') nkdims(1:3), i1
  !        end if
  !  
  !        !if ( allocated(sccs3) ) deallocate( sccs3 )
  !  !      allocate( sccs3(3, nkdims(1), nkdims(2), nkdims(3)) )
  !  
  !        deallocate ( xk1 )
  !end subroutine losc_ksetup


  !SUBROUTINE read_pwscf(odir)
  !  ! Read in pwscf variables, apart from wavefunctions
  !  ! 2022-04-05: I don't think ions_base vars are needed in sLOSC at all
  !  USE wvfct,              ONLY : nbnd_in => nbnd, npwx_in => npwx, &
  !                                 et_in => et, wg_in => wg
  !  USE cell_base,          ONLY : omegain => omega, ibravin => ibrav, &
  !                                 alatin => alat, bgin => bg, &
  !                                 tpibain => tpiba, tpiba2in => tpiba2
  !  USE klist,              ONLY : ngk_in => ngk, xk_in => xk, wk_in => wk, &
  !                                 nks_in => nks, nkstot_in => nkstot, &
  !                                 nelec_in => nelec, nelup_in => nelup, &
  !                                 neldw_in => neldw, &
  !                                 lgauss_in => lgauss, ltetra_in => ltetra
  !  USE lsda_mod,           ONLY : nspin_in => nspin, isk_in => isk
  !  USE fft_base,           ONLY : dffts
  !
  !  IMPLICIT NONE
  !
  !  CHARACTER(LEN=256), INTENT(IN)  :: odir     ! pwscf output directory
  !
  !  CALL start_clock("read_pwscf")
  !
  !  ! Open pwscf files
  !  CALL read_file
  !  CALL openfil_losc
  !
  !  ! Set integer values
  !      ! wvfct
  !  nbnd = nbnd_in
  !  npwx = npwx_in
  !      ! klist
  !  nks = nks_in
  !  nkstot = nkstot_in
  !  nelec = nelec_in
  !  nelup = nelup_in
  !  neldw = neldw_in
  !      ! lsda_mod
  !  nspin = nspin_in
  !
  !  ! Set real values
  !      ! ener? (for: Fermi energy)
  !
  !
  !  ! Allocate allocatables
  !      ! wvfct
  !  ALLOCATE (et(nbnd, nkstot))
  !  ALLOCATE(wg(nbnd, nkstot))
  !      ! klist
  !  ALLOCATE(xk(3, nkstot))
  !  ALLOCATE(wg(nkstot))
  !      ! lsda_mod
  !
  !  ! Set allocatables
  !  et = et_in
  !  wg = wg_in
  !
  !
  !  CALL stop_clock("read_pwscf")
  !  
  !END SUBROUTINE read_pwscf


  !SUBROUTINE read_from_pwscf_old(odir, iprint)
  !    ! Read in important variables from pwscf (not including wavefunctions)
  !    ! 2022-03-03: "First half" of pw2losc_read(), meant to happen before
  !    !   spin-polarization loop (?) and wannier90 readin
  !    USE cell_base,   ONLY : omega, at, alat, bg, ibrav !, tpiba2
  !    USE ions_base,   ONLY : nat, nsp, ityp, tau, atm
  !    USE fft_base,    ONLY : dffts
  !    USE wvfct,       ONLY : nbnd, npwx, et, wg
  !    USE klist,       ONLY : xk, wk, nks, nkstot, igk_k ! k coords & weights
  !    USE klist,       ONLY : ngk, lgauss, ltetra  ! PWs per ik, metal flags
  !    USE klist,       ONLY : nelec, nelup, neldw ! elec num stats

  !    ! Set LOSC variables
  !    USE sys,         ONLY : nkdm
  !    USE sys,         ONLY : losc_set_ints, losc_set_cell_base, &
  !                            losc_set_ions_base, losc_set_wvfct, &
  !                            losc_set_klist, set_supercell_base
  !    USE utils,       ONLY : openfil_losc

  !    IMPLICIT NONE

  !    CHARACTER(LEN=256), INTENT(IN)  :: odir     ! pwscf output directory
  !    INTEGER, INTENT(IN)             :: iprint   ! LOSC print verbosity
  !    
  !    INTEGER                         :: nr3(3)

  !    CALL start_clock("read_from_pwscf")

  !    CALL read_file ! this should call read_xml_file
  !    CALL openfil_losc
  !  
  !    ! None of these are actually stored in LOSC vars anymore
  !    !CALL losc_set_ints(nbnd, npwx, nks, nkstot)
  !    !CALL losc_set_cell_base( omega, alat, at, bg, ibrav )
  !    !CALL losc_set_ions_base( nat, nsp, ityp, tau, atm )
  !    !CALL losc_set_wvfct( ngk, et, wg )
  !    !CALL losc_set_klist( nelec, nelup, neldw, xk, wk )
  !  
  !    nr3(1) = dffts%nr1
  !    nr3(2) = dffts%nr2
  !    nr3(3) = dffts%nr3
  !      ! Equivalent functionality is in sys::set_supercell_{base,grid}
  !    !CALL init_pw2losc(nr3)
  !    !CALL set_supercell_base( omega, alat, at, bg, nkdm )

  !    CALL stop_clock("read_from_pwscf")
  !END SUBROUTINE read_from_pwscf_old


  !subroutine pw2losc_read(wffil, odir, savin, iprint)

  !  use constants,  only : tpi, pi

  !  use cell_base,   ONLY : omega, at, alat, bg, ibrav !, tpiba2
  !  use ions_base,   only : nat, nsp, ityp, tau, atm
  !  USE fft_base,    only : dffts, dfftp ! @pw2losc

  !  ! wave func modules
  !  USE wavefunctions, ONLY : evc, deallocate_wavefunctions
  !  USE wvfct,       ONLY : nbnd, npwx, et, wg
  !  use ener,        only : ef ! e_fermi
  !  USE gvect,       ONLY : ngm, g 
  !  USE uspp,        only : okvan, vkb, nkb
  !  use becmod,      only : calbec
  !  use paw_variables, only : okpaw

  !  USE klist,       ONLY : xk, wk, nks, nkstot, igk_k ! k coords & weights
  !  use klist,       only : ngk, lgauss, ltetra  ! PWs per ik, metal flags
  !  USE klist,       ONLY : nelec, nelup, neldw ! elec num stats
  !  use start_k,     only : nk1, nk2, nk3 ! k-mesh dims

  !  USE noncollin_module, ONLY : npol, noncolin
  !  USE lsda_mod,    ONLY : nspin
  !  ! io modules
  !  USE io_files,    ONLY : prefix, tmp_dir, diropn, nwordwfc, iunwfc
  !  USE io_global,   ONLY : ionode, ionode_id, stdout
  !  use sys,    only : losc_set_ints, losc_set_wvfct, losc_set_klist, &
  !                          losc_set_cell_base, set_supercell_base, & 
  !                          losc_set_ions_base, losc_save_sys
  !  use sys,    only : u_loc, ikjtyp, lwfcen, nkdm, nrsz, rsgr
  !  use sys,    only: lplot_lwf, lband, mcvec1d, seedname
  !  use sys,    only: nwan, dbnd, dis_bndi, set_excluded_bands
  !  use utils,  only: openfil_losc
  !  !use wan2losc,    only: read_nnkp, set_excluded_bands

  !  integer,            external :: find_free_unit

  !  ! Params
  !  !integer, intent(in) :: nwan ! num wann read from wan_u_dis.mat
  !  !integer, intent(in) :: nbin ! num bands read from wan_u_dis.mat
  !  !integer, intent(in) :: nkin ! num of k points read from wan_u.mat
  !  integer, intent(in) :: iprint ! print verbosity
  !  logical, intent(in) :: savin ! whether to save to file
  !  CHARACTER(len=256), intent(in) :: odir     ! QE ouput dir
  !  CHARACTER(len=64),  intent(in) :: wffil    ! scr file for wave func seedname
  !  ! Locals
  !  character(len=8)   :: date
  !  integer            :: values(8)
  !  character(len=512) :: charbuff    ! char buffer
  !  character(len=256) :: filename    ! for opening files
  !  integer            :: lrwfcr      ! length of wfcr
  !  integer            :: oun, iuwfcr, ios ! input unit: wfcr
  !!  integer            :: iun ! input unit
  !  LOGICAL            :: exst        ! exist

  !  integer   :: is, isoff
  !  integer   :: ir1, ik, ig, iuc
  !  integer   :: ib1, ib2, ii1, ii2
  !  integer   :: npw
  !  integer   :: inr1, inr12
  !  integer   :: nr3(3)

  !  complex(dp) :: eikr
  !  real(dp)    :: r1, rwf, iwf, rnorm
  !  real(dp)    :: xp(3)

  !  integer  :: ik1, ik2
  !  real(dp) :: np1, np2
  !  real(dp) :: ecos(3), esin(3) ! expecation of periodic position op

  !  integer :: ip, psz ! plot iter, plot size (how many WFs)
  !  complex(dp), allocatable :: evc_plt(:,:,:)

  !  integer                  :: inz, unz   ! num of non-zero u_loc element for a given wann
  !  integer,     allocatable :: ucol(:)    ! indices of non-zero u_loc elements
  !  complex(dp), allocatable :: utmp(:)    ! non-zero elements of u_loc
  !  complex(dp), allocatable :: evc3(:,:,:)  ! evc debug,  HUUUUGE data struct
  !  complex(dp), allocatable :: evc_lok(:) ! loc. u_nk in G-space
  !  complex(dp), allocatable :: unkgr(:,:) ! unk on grid with k fixed
  !  complex(dp), allocatable :: clwf(:)    ! LWF for 1 unit cell (needed for fast calc)
  !  real(dp),    allocatable :: dwfn(:,:)  ! density of LWF_n
  !  !complex(dp), allocatable :: becp(:,:)  ! USPP 
  !  !real(dp),    allocatable :: ursgr(:,:) ! unit cell offset rsgr

  !  call start_clock('pw2losc_read')

  !  !call read_file ! this should call read_xml_file
  !  !call openfil_losc

  !  !call losc_set_ints(nbnd, npwx, nks, nkstot)
  !  !call losc_set_cell_base( omega, alat, at, bg, ibrav )
  !  !call losc_set_ions_base( nat, nsp, ityp, tau, atm )
  !  !call losc_set_wvfct( ngk, et, wg )
  !  !call losc_set_klist( nelec, nelup, neldw, xk, wk )

  !  nr3(1) = dffts%nr1
  !  nr3(2) = dffts%nr2
  !  nr3(3) = dffts%nr3
  !  !call init_pw2losc(nr3)
  !  !call set_supercell_base( omega, alat, at, bg, nkdm )

  !  ! Start here? later, after wannier90 inputs are read
  !  !call read_nnkp(seedname,pwcheck=.true.)
  !  !call set_excluded_bands(nbnd)

  !  allocate( ucol(nbnd+1) )
  !  allocate( utmp(nbnd+1) )
  !  allocate( unkgr(nrsz,nwan) )
  !  allocate( evc_lok(npwx) )         ! local orbitals
  !  !allocate( becp(nkb, nbnd) )

  !  filename = 'wfc_r'
  !  iuwfcr = find_free_unit() !877
  !  lrwfcr = 2 * dffts%nr1x*dffts%nr2x*dffts%nr3x * npol
  !  !lrwfc = 2 * nbnd * npwx * npol
  !  exst = .false.

  !  inr1 = dffts%nr1
  !  inr12 = inr1 * dffts%nr2

  !  ! 2022-04-20: Printing needs to go to a different method,
  !  !   i.e. not wannier_density::construct_wannier_density()
  !  !   (which most of pw2losc_read() are going to)
  !  if ( ionode .and. (iprint.gt.0) ) then
  !  ! print some output for debugging stuff
  !  ! 2022-04-27: Some of this is printed in pw2losc, some in wannier_density,
  !  !   some in in wan2losc
  !      write(6,'(5x,"filename = ",a80)') filename
  !      write(6,'(5x,"iunwfc: ",i3,", okvan: ",l,", okpaw: ",l)') &
  !              iunwfc, okvan, okpaw
  !      write(6,'(5x,"nwordwfc                            ",i12)') nwordwfc
  !      write(6,'(5x,"length of wfc in real space/per band",i12)') nks*lrwfcr*8
  !      write(6,'(5x,"length of 1 wfc on real space grid  ",i12)') lrwfcr
  !      write(6,'(5x,"length of wfcs in G space           ",i12)') &
  !              2*nbnd*npwx*nks*8
  !      write(6,1310) nbnd, nelec, nelup, neldw, npol
  !      write(6,'(5x,a,i4)') 'nwan: ', nwan
  !      write(6,1315) nspin, ef, alat, omega
  !      write(6,'(5x,2(A,1x))') 'prefix: ', trim(prefix)
  !      write(6,'(5x,"nkstot: ",i3,", nks: ",i3,", nk(x,y,z): ",3(i2,1x))') &
  !              nkstot, nks, nk1, nk2, nk3
  !      write(6,'(5x,"nkdm(1:3): ",3(i2,1x))') nkdm(1:3)

  !      if( iprint.gt.2 )then
  !        write(6,'(5x,"ngk(1:nks)")')
  !        write(6,'(999(5x,8(i8,2x),/))') (ngk(ik), ik=1,nks) ! 8 per line
  !      end if
  !      ! FFT(s/p) comparison
  !      write(6,1311) "s/p", "ngm", "nnr", &
  !                  "nr1", "nr2", "nr3", "nr1x", "nr2x", "nr3x"
  !      write(6,1312) 's', dffts%ngm, dffts%nnr, dffts%nr1, dffts%nr2, &
  !                    dffts%nr3, dffts%nr1x, dffts%nr2x, dffts%nr3x
  !      write(6,1312) 'p', dfftp%ngm, dfftp%nnr, dfftp%nr1, dfftp%nr2, &
  !                    dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%nr3x
  !  end if

  !  if (nelup.ne.neldw) then
  !      write(6,'(2x,"ERR(losc): odd num of elecs unsupported")')
  !      call errore('LOSC', 'odd num of elecs unsupported', 1)
  !  end if

  !  CALL diropn (iuwfcr, filename, lrwfcr, exst)
  !  oun = find_free_unit()
  !  call date_and_time(DATE=date, VALUES=values)

  !  if( lplot_lwf )then  ! plotting data
  !    psz = lband(2) - lband(1) + 1
  !    ip = 1
  !    allocate(evc_plt(npwx,nks,psz))
  !  end if

  !  is = 1
  !  isoff = (is - 1) * npwx  ! evc index 1 spin offset
  !  !if( iprint.gt.2 ) write(*,*) '  --( BO phase stats )--'
  !!
  !  call start_clock('pw2losc:LBO')
  !  doEvcReadIk: do ik = 1, nks
  !      !if( iprint.gt.2 ) write(*,*) '  -( k = ', ik,' : ', xk(1:3,ik),' )-'
  !      npw = ngk(ik)
  !      CALL davcio (evc, 2*nwordwfc, iunwfc, ik, -1) ! read wfc
  !      !call init_us_2(npw, igk_k(1,ik), xk(:,ik), vkb)
  !      !call calbec (npw, vkb, evc, becp)
  !      ! calbec is not the difference, checked against losc_ref

  !      doEvcReadIb: do ib1 = 1, nwan ! ib1 slowest index and not outermost, but it will
  !                      ! be outermost later when evaluated the most
  !          evc_lok(:) = cmplx(0.0_dp)
  !          do ib2 = 1, dbnd
  !            ii2 = dis_bndi(ib2)
  !            do ig = 1, npw !x
  !                evc_lok(ig) = evc_lok(ig) + u_loc(ib2,ib1,ik)*evc(ig+isoff,ii2)
  !                                    !u_loc(ib2,ib1,ik)*evc(ig+isoff,ib2)
  !            end do
  !          end do

  !          call losc_init_lbo(evc_lok(:), unkgr(:,ib1), ik, ib1)
  !          if( lplot_lwf .and. (ib1.ge.lband(1) .and. ib1.le.lband(2)) ) then
  !            evc_plt(:,ik,ib1-lband(1)+1) = evc_lok(:)
  !          end if

  !          if( savin )then !write(oun) unkgr(:,:)

  !              write(filename,'(4(a),i0.4,a)') trim(odir), "/", trim(prefix), &
  !                                              "_losc_lun", ib1, ".bin"
  !              if( ik.eq.1 ) then
  !                  open(oun, file=trim(filename), action='write', & 
  !                      form='unformatted', status='unknown')
  !              else
  !                  open(oun, file=trim(filename), access='append', &
  !                      form='unformatted', status='old')
  !              end if
  !              write(oun) unkgr(:,ib1)
  !              close(oun)
  !          else

  !              write(filename,'(2(a),i0.4,a)') trim(wffil), "_losc_lun", ib1, ".bin"
  !              if( ik.eq.1 ) then
  !                  open(oun, file=trim(filename), action='write', & 
  !                      form='unformatted', status='unknown', iostat=ios)
  !                if( ios.ne. 0)then
  !                    call errore('LOSC','error opening file '//trim(filename),3)
  !                end if
  !              else
  !                  open(oun, file=trim(filename), access='append', &
  !                      form='unformatted', status='old')
  !              end if
  !              write(oun) unkgr(:,ib1)
  !              close(oun)
  !          end if

  !      end do doEvcReadIb

  !  end do doEvcReadIk
  !  call stop_clock('pw2losc:LBO')
  !  
  !  ifPlot: if( lplot_lwf )then
  !      do ip = 1, psz
  !        ib1 = lband(1) + (ip-1)
  !        !call losc_plot_wan(evc_plt(:,:,ip),ib1)
  !      end do
  !      deallocate( evc_plt )
  !  end if ifPlot

  !  call deallocate_wavefunctions()
  !  deallocate( ucol )
  !  deallocate( utmp )
  !  deallocate( evc_lok )
  !  if( allocated(evc3) ) deallocate( evc3 )
  !  deallocate( unkgr )
  !  close(iuwfcr)

  !  ifSaveSys: if ( ionode .and. savin ) then

  !      write(filename,'(4(a))') trim(odir), "/", trim(prefix), "_utils.dat"
  !      open(oun, file=trim(filename), form='formatted', status='unknown',iostat=ios)
  !      if(ios.ne.0) call errore('PW2LOSC','error opening '//trim(filename),1)
  !      call losc_save_sys(oun)
  !      close(oun)

  !  end if ifSaveSys

  !  call start_clock('pw2losc:SavD')
  !  ifSavinDens: if( .not.savin ) then
  !  ! Save LWF density to disk if requested, and only if not doing a DWF
  !  ! overwrite such as devel_flag='sgto'
  !      allocate( dwfn(nrsz, nks) )
  !      allocate( clwf(nrsz) )
  !      allocate( unkgr(nrsz,nks) )

  !      doCalcLwfDens: do ib1 = 1, nwan

  !          ! Get LWF ib1's unk on the grid
  !          !if( savin ) then
  !          !    write(filename,'(4(a),i0.4,a)') trim(odir), "/", trim(prefix), &
  !          !                                    "_losc_wfcr", ib1, ".bin"
  !        call start_clock('read_unkgr')
  !          write(filename,'(2(a),i0.4,a)') trim(wffil), "_losc_lun", ib1, ".bin"
  !          open(oun, file=trim(filename), action='read', & 
  !              form='unformatted', status='unknown')
  !          do ik = 1, nks
  !              read(oun) unkgr(:,ik)
  !          end do
  !          close(oun)
  !        call stop_clock('read_unkgr')

  !        call start_clock('make_DWF')
  !        rnorm = 0.0_dp
  !        doDwfUc: do iuc = 1, nks

  !            call start_clock('make_DWF1')
  !            ! use unkgr to fill in clwf
  !            clwf = cmplx(0.0_dp,kind=dp) 
  !            do ik = 1, nks
  !                do ir1 = 1, nrsz
  !                    !r1 = tpi * sum( xk(1:3,ik) * (rsgr(1:3,ir1)+mcvec1d(1:3,iuc)) )
  !                    r1 = xk(1,ik) * (rsgr(1,ir1)+mcvec1d(1,iuc)) + &
  !                        xk(2,ik) * (rsgr(2,ir1)+mcvec1d(2,iuc)) + &
  !                        xk(3,ik) * (rsgr(3,ir1)+mcvec1d(3,iuc))
  !                    r1 = r1 * tpi
  !                    !eikr = cmplx( cos(r1), sin(r1) )
  !                    eikr = exp( cmplx(0.0_dp,r1,kind=dp) )
  !                    clwf(ir1) = clwf(ir1) + eikr * unkgr(ir1,ik)
  !                end do
  !            end do
  !            call stop_clock('make_DWF1')

  !            call start_clock('make_DWF2')
  !            ! use clwf to fill in dwfn
  !            r1 = 1.0_dp / real(nks,dp)
  !            do ir1 = 1, nrsz
  !                rwf = real(clwf(ir1)) * r1
  !                iwf = aimag(clwf(ir1)) * r1
  !                rwf = rwf * rwf
  !                iwf = iwf * iwf
  !                rwf = rwf + iwf
  !                rnorm = rnorm + rwf
  !                dwfn(ir1,iuc) = rwf ! + iwf
  !            end do
  !            call stop_clock('make_DWF2')

  !        end do doDwfUc

  !        ! re-norm dwfn to correct bad norm before, also corrects floating point errors
  !        rnorm = real(nrsz,dp) / ( rnorm * omega )
  !        write(6,'(1x,"prenorm(",i0,")=",g20.12)') ib1, omega / (rnorm*real(nrsz,dp))
  !        dwfn(:,:) = dwfn(:,:) * rnorm
  !        call stop_clock('make_DWF')


  !        call start_clock('save_DWF')
  !        ! write LWF ib1's density on the grid
  !        write(filename,'(2(a),i0.4,a)') trim(wffil), "_wannier_density", ib1, ".bin"
  !        open(oun, file=trim(filename), action='write', & 
  !            form='unformatted', status='unknown')
  !        do iuc = 1, nks
  !            write(oun) dwfn(:,iuc)
  !        end do
  !        close(oun)
  !        call stop_clock('save_DWF')

  !      end do doCalcLwfDens

  !      deallocate( unkgr )
  !      deallocate( clwf )
  !      deallocate( dwfn )

  !  end if ifSavinDens
  !  call stop_clock('pw2losc:SavD')


  !1310 format(5x,'nbnd: ',i4,', nelec:',f6.1,', nup: ',f5.1,', ndw: ',f5.1,', npol: ',i4)
  !1311 format(5x,a3,1x,2(a8,1x),6(a4,1x))
  !1312 format(5x,a3,1x,2(i8,1x),6(i4,1x))
  !1315 format(5x,'nspin:',i4,', e_f: ',f7.3, ', alat: ',f6.2,', omega: ',f9.3)

  !  call stop_clock('pw2losc_read')
  !  return
  !end subroutine pw2losc_read


  !subroutine pw2losc_open(nbin, nkin, umio, unkio, odir)
  !subroutine pw2losc_open(nbin, nkin, umio, wffil, odir)
  !    ! instead of having the same code twice for the formatting of read/write,
  !    ! make a wrapper func for write/read, then wrap the losc_read/open in a function
  !    ! that passes whether to do reading/writing, only need the formatting in one place
  !    ! iowrap(io,ioun,fmt,ordered args) -> how to do this for args outside the parentheses...
  !
  !  use constants,  only : tpi
  !  use kinds,       only: dp
  !  use parameters,  only: ntypx
  !
  !  USE noncollin_module, ONLY : npol, noncolin
  !  use ener,        only : ef ! e_fermi
  !  USE lsda_mod,    ONLY : nspin
  !  use start_k,     only : nk1, nk2, nk3 ! k-mesh dims
  !
  !  USE io_files,    ONLY : prefix!, tmp_dir, diropn, nwordwfc, iunwfc
  !  USE io_global,   ONLY : ionode, ionode_id, stdout
  !  USE mp_world,    ONLY : world_comm
  !  USE mp,          ONLY : mp_bcast, mp_barrier
  !  use sys,    only : nwan, nkdm, nrsz, rsgr
  !  use sys,    only : losc_set_ints, losc_set_wvfct, losc_set_klist, &
  !                          losc_set_cell_base, set_supercell_base, & 
  !                          losc_set_ions_base, ionodescr , mcvec1d
  !
  !  implicit none
  !  integer, external   :: find_free_unit
  !  ! Params
  !  integer, intent(in) :: nbin ! num bands read from wan_u.mat
  !  integer, intent(in) :: nkin ! num of k points read from wan_u.mat
  !  complex(dp), intent(in) :: umio(:,:,:)  ! u_wan
  !!  complex(dp), allocatable, intent(inout) :: unkio(:,:,:) ! unkgr
  !  CHARACTER(len=64),  intent(in) :: wffil    ! scr file for wave func seedname
  !  CHARACTER(len=256), intent(in) :: odir     ! QE ouput dir
  !  ! Locals
  !  character(len=512) :: charbuff   ! char buffer
  !  character(len=256) :: filename   ! for opening files
  !  integer            :: lrwfcr     ! length of wfcr
  !  integer            :: iun, oun   ! input, output unit
  !  LOGICAL            :: exst       ! exist
  !
  !  integer  :: ib1, ik, is
  !  integer  :: nkstot, nbnd, npwx
  !  integer  :: nat, nsp, nelec, nelup, neldw
  !  integer  :: nr3(3)
  !  integer  :: ibrav
  !  real(dp) :: alat, omega
  !  real(dp) :: at(3,3), bg(3,3)
  !  real(dp), allocatable :: tau(:,:)  ! dim(3,nat)
  !  integer,  allocatable :: ityp(:)   ! dim(nat)
  !  character(len=3) :: atm(ntypx)     ! dim(3,nsp)
  !  real(dp), allocatable :: et(:,:)   ! dim(nbnd,nks)
  !  real(dp), allocatable :: wg(:,:)   ! dim(nbnd,nks)
  !  integer,  allocatable :: ngk(:)    ! dim(nks)
  !  real(dp), allocatable :: xk(:,:)   ! dim(3,nks)
  !  real(dp), allocatable :: wk(:)     ! dim(nks)
  !
  !  integer     :: iuc, ir1
  !  real(dp)    :: r1, rwf, iwf
  !  real(dp)    :: obnks2          ! 1 / nks^2
  !  complex(dp) :: clwf, eikr
  !  complex(dp), allocatable :: unkgr(:,:) ! unk on grid with n fixed
  !  complex(dp), allocatable :: cwfn(:)    ! LWF_n on the grid for 1 unit cell
  !  real(dp),    allocatable :: dwfn(:,:)  ! density of LWF_n
  !  integer :: wsz, wrank, ierr
  !
  !  call start_clock('pw2losc_open')
  !
  !  if ( ionode ) then
  !
  !      write(filename,'(4(a))') trim(odir), "/", trim(prefix), "_utils.dat"
  !      inquire(file=trim(filename), exist=exst)
  !      if ( .not.exst ) then
  !          write(6,'("ERR, *_utils.dat not found")')
  !          call errore('PW2LOSC_OPEN',trim(filename)//' not found',2)
  !      end if
  !      
  !      iun = find_free_unit()
  !      open(iun, file=trim(filename), form='formatted', status='old')
  !
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(7(1x,i6))') nbnd, nkstot, nelec, nelup, neldw, nat, nsp
  !      read(iun,'(a)') charbuff
  !      read(iun,'(4(1x,i12))') npwx, nr3(1:3)
  !
  !  end if
  !
  !#if defined(__MPI)
  !  ! need mp_bcast for integer sizes
  !  CALL mp_bcast( nat,    ionode_id, world_comm )
  !  CALL mp_bcast( nsp,    ionode_id, world_comm )
  !  CALL mp_bcast( nbnd,   ionode_id, world_comm )
  !  CALL mp_bcast( nkstot, ionode_id, world_comm )
  !  CALL mp_bcast( nelec,  ionode_id, world_comm )
  !  CALL mp_bcast( nelup,  ionode_id, world_comm )
  !  CALL mp_bcast( neldw,  ionode_id, world_comm )
  !  CALL mp_bcast( npwx,   ionode_id, world_comm )
  !  CALL mp_bcast( nr3,    ionode_id, world_comm )
  !#endif
  !
  !  allocate( tau(3, nat) )
  !  allocate( ityp(nat) )
  !  allocate( et(nbnd, nkstot) )
  !  allocate( wg(nbnd, nkstot) )
  !  allocate( ngk(nkstot) )
  !  allocate( xk(3,nkstot) )
  !  allocate( wk(nkstot) )
  !
  !  if ( ionode ) then
  !
  !      ! Unit cell info
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(1x,i5,2(1x,g22.14))') ibrav, alat, omega
  !
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      do is = 1, 3
  !          read(iun,'(3(1x,g22.14))') at(1:3,is)
  !      end do
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      do is = 1, 3
  !          read(iun,'(3(1x,g22.14))') bg(1:3,is)
  !      end do
  !
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      do is = 1, nat
  !          read(iun,'(3(1x,g22.14))') tau(1:3,is)
  !      end do
  !
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(8(1x,i3))') ityp(1:nat)

  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      do is = 1, nsp
  !          read(iun,'(" ",a)') atm(is)
  !      end do

  !      ! k-point info
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(8(1x,i12))') (ngk(ik), ik=1,nkstot)

  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(3(1x,f20.10))') (xk(1:3,ik), ik=1,nkstot)
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      read(iun,'(8(1x,g22.14))') (wk(ik), ik=1,nkstot)

  !      ! Band structure 
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      do ik = 1, nkstot
  !          read(iun,'(a)') charbuff
  !          read(iun,'(8(1x,g22.14))') (et(ib1,ik), ib1=1,nbnd)
  !      end do
  !      read(iun,'(a)') charbuff
  !      read(iun,'(a)') charbuff
  !      do ik = 1, nkstot
  !          read(iun,'(a)') charbuff
  !          read(iun,'(8(1x,g22.14))') (wg(ib1,ik), ib1=1,nbnd)
  !      end do

  !  end if

  !#if defined(__MPI)
  !  ! need mp_bcast for integer sizes
  !  CALL mp_bcast( omega, ionode_id, world_comm )
  !  CALL mp_bcast( alat,  ionode_id, world_comm )
  !  CALL mp_bcast( at,    ionode_id, world_comm )
  !  CALL mp_bcast( bg,    ionode_id, world_comm )
  !  CALL mp_bcast( ityp,  ionode_id, world_comm )
  !  CALL mp_bcast( tau,   ionode_id, world_comm )
  !  CALL mp_bcast( atm,   ionode_id, world_comm )
  !  CALL mp_bcast( ngk,   ionode_id, world_comm )
  !  CALL mp_bcast( et,    ionode_id, world_comm )
  !  CALL mp_bcast( wg,    ionode_id, world_comm )
  !  CALL mp_bcast( xk,    ionode_id, world_comm )
  !  CALL mp_bcast( wk,    ionode_id, world_comm )
  !#endif

  !  call losc_set_ints(nbnd, npwx, nkstot, nkstot)
  !  call losc_set_cell_base( omega, alat, at, bg, ibrav )
  !  call losc_set_ions_base( nat, nsp, ityp, tau, atm )
  !  call losc_set_wvfct( ngk, et, wg )
  !  call losc_set_klist( real(nelec,dp), real(nelup,dp), real(neldw,dp), &
  !                      xk, wk )

  !  call init_pw2losc(nr3)
  !  call set_supercell_base( omega, alat, at, bg, nkdm )

  !  allocate( dwfn(nrsz, nkstot) )
  !  if( ionode ) allocate( cwfn(nrsz) )
  !  if( ionode ) allocate( unkgr(nrsz,nkstot) )

  !  ! each process creates a file in scr with their rank in the file name
  !  ! then do a barrier for syncing. next each process does an inquiry
  !  ! for all filenames of that template from 0..wsz. Then it will know all
  !  ! process ranks on it's node. The process with lowest rank on that node
  !  ! will then write the dwfn
  !  call mpi_comm_rank(WORLD_COMM, wrank, ierr)
  !  call mpi_comm_size(WORLD_COMM, wsz, ierr)
  !  oun = find_free_unit()
  !  write(filename,'(2(a),i0.4,a)') trim(wffil), "_mpiexst.", wrank
  !  open(oun, file=trim(filename), action='write', & 
  !      form='formatted', status='unknown')
  !  write(oun,'(i0)') wrank ! write some garbage
  !  call mp_barrier( world_comm )

  !  ionodescr = .true.
  !  do ib1 = 1, wsz
  !      write(filename,'(2(a),i0.4,a)') trim(wffil), "_mpiexst.", ib1
  !      inquire(file=trim(filename), exist=exst)
  !      if( exst )then
  !          if( ib1.lt.wrank ) ionodescr = .false.
  !      end if
  !  end do
  !  close(oun)!, status='delete') ! delete below

  !  obnks2 = 1.0_dp / real(nkstot*nkstot,dp)

  !  doRdCalDens: do ib1 = 1, nwan
  !      ifIo1: if( ionode ) then
  !          write(filename,'(4(a),i0.4,a)') trim(odir), "/", trim(prefix), &
  !                                          "_losc_lun", ib1, ".bin"
  !          open(oun, file=trim(filename), action='read', & 
  !              form='unformatted', status='unknown')
  !          do ik = 1, nkstot
  !              read(oun) unkgr(:,ik)
  !          end do
  !          close(oun)

  !          do iuc = 1, nkstot
  !              ! @TODO @SPEED swap `do ir1` and `do ik` to iter unkgr on fast axis
  !              ! need a new array to accumulate clwf for all those point
  !              ! in 1 unit cell
  !              cwfn(:) = cmplx(0.0_dp)
  !              do ik = 1, nkstot
  !                  do ir1 = 1, nrsz
  !                      r1 = tpi * sum( xk(1:3,ik) * (rsgr(1:3,ir1)+mcvec1d(1:3,iuc)) )
  !                      eikr = cmplx( cos(r1), sin(r1) )
  !                      cwfn(ir1) = cwfn(ir1) + eikr * unkgr(ir1,ik)
  !                  end do
  !              end do
  !              do ir1 = 1, nrsz
  !                  rwf = real(cwfn(ir1))
  !                  iwf = aimag(cwfn(ir1))
  !                  dwfn(ir1,iuc) = ( rwf*rwf + iwf*iwf ) * obnks2
  !              end do
  !          end do
  !      end if ifIo1

  !      call mp_bcast( dwfn, ionode_id, world_comm )

  !      if( ionodescr ) then
  !          ! write LWF ib1's density on the grid
  !          write(filename,'(2(a),i0.4,a)') trim(wffil), "_wannier_density", ib1, ".bin"
  !          open(oun, file=trim(filename), action='write', & 
  !              form='unformatted', status='unknown')
  !          do iuc = 1, nkstot
  !              write(oun) dwfn(:,iuc)
  !          end do
  !          close(oun)
  !      end if

  !  end do doRdCalDens

  !  deallocate( dwfn )
  !  if( ionode ) deallocate( cwfn )
  !  if( ionode ) deallocate( unkgr )

  !  write(filename,'(2(a),i0.4,a)') trim(wffil), "_mpiexst.", wrank
  !  open(oun, file=trim(filename), action='write', & 
  !      form='formatted', status='unknown')
  !  close(oun, status='delete')
  !  !close(oun) ! debug

  !1310 format(5x,'nbnd: ',i4,', nelec:',f6.1,', nup: ',f5.1,', ndw: ',f5.1,&
  !              ', npol: ',i4)
  !1315 format(5x,'nspin:',i4,', e_f: ',f7.3, ', alat: ',f6.2,', omega: ',f9.3)
  !  call mp_barrier( world_comm )
  !  call stop_clock('pw2losc_open')
  !  return
  !end subroutine pw2losc_open


  !subroutine losc_init_lbo(evck, evcr, ik, ib)
  !  ! normalizes evck, FFT to real space & store in evcr
  !  ! optionally checks for phase stats (using directional stats)
  !    use gvect,     only : g
  !    use cell_base, ONLY : omega!, at, alat, bg !, tpiba2
  !    use fft_base,  only: dffts
  !    use klist,     only: ngk!, lgauss, ltetra  ! PWs per ik, metal flags
  !    implicit none
  !    ! Params
  !    complex(dp), intent(inout) :: evck(:) ! evc_lok @ an ik and ib
  !    complex(dp), intent(inout) :: evcr(:) ! evck FFT'd to real-space grid
  !    integer, intent(in) :: ik
  !    integer, optional, intent(in) :: ib
  !    ! Locals
  !    integer :: i1, ig, nrsz
  !    integer :: ibl ! locals of optionals
  !    real(dp) :: gnorm1, rnorm1
  !    real(dp) :: r1, mprev
  !    real(dp) :: rre, rim, rarg, rmag
  !    real(dp) :: rcos, rsin
  !    real(dp) :: amen, avar, astd ! arg stats (phase)
  !    real(dp) :: veden ! volume element density
  !    ! temps for evck norm on grid

  !    CALL start_clock( 'init_lbo' )

  !    nrsz = dffts%nnr
  !    veden = omega / real(nrsz,dp)

  !    ibl = 0
  !    if ( present(ib) ) ibl = ib

  !    r1 = 0.0_dp          ! normalize the LBO
  !    !do ig = 1, ngk(ik)
  !    !    r1 = r1 + abs(evck(ig))**2
  !    !end do
  !    gnorm1 = 1.0_dp !/ sqrt( omega * r1 ) 
  !    !*real(dffts%nnr,dp) ) ! * real(nks, dp) )
  !    !evck(1:ngk(ik)) = evck(1:ngk(ik)) * cmplx(gnorm1)

  !    ! transform on to FFT real grid
  !    call losc_wf_g2r(evck, evcr, ik)

  !    rnorm1 = 0.0_dp
  !    rcos = 0.0_dp
  !    rsin = 0.0_dp

  !    do i1 = 1, nrsz

  !        rnorm1 = rnorm1 + abs(evcr(i1))**2
  !        rmag = abs(evcr(i1))
  !        rre = real(evcr(i1))
  !        rim = aimag(evcr(i1))
  !        rcos = rcos + rre/rmag
  !        rsin = rsin + rim/rmag

  !    end do
  !    rcos = rcos / real(nrsz, dp)
  !    rsin = rsin / real(nrsz, dp)
  !    rmag = sqrt( rcos*rcos + rsin*rsin ) ! average vector length
  !    avar = 1.0_dp - rmag
  !    astd = sqrt( -2.0_dp * log(rmag) )
  !    !amen = acos( rcos / rmag )
  !    amen = atan2( rsin, rcos )

  !    rnorm1 = 1.0_dp / ( sqrt(rnorm1) * veden )
  !    evcr(:) = evcr(:) * rnorm1

  !    CALL stop_clock( 'init_lbo' )
  !end subroutine losc_init_lbo


  !subroutine losc_wf_g2r(psig, psir, ik)
  !    USE klist,          ONLY: ngk, igk_k
  !    USE fft_interfaces, ONLY: invfft
  !    use fft_base,       only: dffts
  !    implicit none
  !    ! Params
  !    integer,     intent(in)    :: ik         ! k-point
  !    complex(dp), intent(in)    :: psig(:) ! wfc in PW basis
  !    complex(DP), intent(inout) :: psir(:) !dffts%nnr) ! 
  !    ! Locals
  !    integer :: ig

  !    CALL start_clock( 'losc_wf_g2r' )

  !    psir = cmplx(0.d0) ! perform the fourier transform
  !    do ig = 1, ngk(ik)
  !      psir(dffts%nl(igk_k(ig,ik))) = psig(ig)
  !    enddo
  !    CALL invfft('Wave', psir(:), dffts)

  !    CALL stop_clock( 'losc_wf_g2r' )
  !end subroutine losc_wf_g2r