! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt.

MODULE utils
  ! General utility functions for the LOSC module.

  USE kinds,              ONLY : dp

  IMPLICIT NONE
  REAL(dp) :: eqtol(3)   ! equal tolerance for real space grid

  ! MPI variables
  INTEGER  :: mpmsz = 10 ! message size granularity
  character(len=64)   :: nodenam
  integer             :: mpioun
  !logical             :: ionodescr  ! the io rank for that node for /scr
    ! 2022-04-20: ionodescr only used in pw2losc_open(); thus DEPRECATED

  CONTAINS

  SUBROUTINE set_io_unit(name, irank, lflag)
    IMPLICIT NONE
    INTEGER, EXTERNAL                   :: find_free_unit
    CHARACTER(len=256), INTENT(INOUT)   :: name 
    INTEGER, INTENT(IN)                 :: irank
    LOGICAL, INTENT(IN)                 :: lflag
    INTEGER                             :: istatus

    IF (lflag) THEN
      mpioun = find_free_unit()
      WRITE(name,'(a,i0,3(a))') 'mpio_rank', irank, '_', trim(nodenam), '.txt'
      OPEN(unit=mpioun, file=trim(name), action='write', form='formatted', &
          status='unknown', iostat=istatus)
    ELSE
      mpioun = -1
    END IF

    RETURN
  END SUBROUTINE set_io_unit 


  SUBROUTINE set_tolerance(igrid)
    ! Set the real-space grid tolerance (for equality testing)

    IMPLICIT NONE

    ! Parameters
    INTEGER, INTENT(IN)             :: igrid(3)   ! real-space grid dimensions

    ! Locals
    REAL(dp)                        :: rgrid(3)   ! igrid cast to real

    rgrid(:) = REAL(igrid(:), KIND=dp)
    eqtol(:) = 0.5_dp / rgrid(:)
    RETURN
  END SUBROUTINE set_tolerance


  function eq_3r(r1, r2) result(res)
  ! equal for 3D coordinates
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3)
      ! Return
      logical              :: res

      if ( ( abs(r1(1)-r2(1)).lt.eqtol(1) ) .and. &
          ( abs(r1(2)-r2(2)).lt.eqtol(2) ) .and. &
          ( abs(r1(3)-r2(3)).lt.eqtol(3) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function eq_3r

  function eq_3rt(r1, r2, rtol) result(res)
  ! equal for 3D coordinates, user defined tolerances
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3), rtol(3)
      ! Return
      logical              :: res

      if ( ( abs(r1(1)-r2(1)).lt.rtol(1) ) .and. &
          ( abs(r1(2)-r2(2)).lt.rtol(2) ) .and. &
          ( abs(r1(3)-r2(3)).lt.rtol(3) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function eq_3rt

  function neq_3r(r1, r2) result(res)
  ! not equal for 3D coordinates
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3)
      ! Return
      logical  :: res

      res = .false.
      if ( abs(r1(1)-r2(1)) .gt. eqtol(1) ) then
          res = .true.
          return
      else if ( abs(r1(2)-r2(2)) .gt. eqtol(2) ) then
          res = .true.
          return
      else if ( abs(r1(3)-r2(3)) .gt. eqtol(3) ) then
          res = .true.
          return
      end if
      return
  end function neq_3r

  function neq_3rt(r1, r2, rtol) result(res)
  ! not equal for 3D coordinates, user defined tolerances
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3), rtol(3)
      ! Return
      logical  :: res

      res = .false.
      if ( abs(r1(1) - r2(1) ) .gt. rtol(1) ) then
          res = .true.
          return
      else if ( abs(r1(2) - r2(2) ) .gt. rtol(2) ) then
          res = .true.
          return
      else if ( abs(r1(3) - r2(3) ) .gt. rtol(3) ) then
          res = .true.
          return
      end if
      return
  end function neq_3rt

  function gt_3r(r1, r2) result(res)
  ! greater than for 3D coords
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3)
      ! Return
      logical              :: res

      if ( ( r1(1) .gt. r2(1) ) .and. &
          ( r1(2) .gt. r2(2) ) .and. &
          ( r1(3) .gt. r2(3) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function gt_3r

  function ge_3r(r1, r2) result(res)
  ! greater than or equal for 3D coords
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3)
      ! Return
      logical              :: res

      if( ( ( r1(1).gt.r2(1) ) .or. ( abs(r1(1)-r2(1)).lt.eqtol(1) ) ) .and. &
          ( ( r1(2).gt.r2(2) ) .or. ( abs(r1(2)-r2(2)).lt.eqtol(2) ) ) .and. &
          ( ( r1(3).gt.r2(3) ) .or. ( abs(r1(3)-r2(3)).lt.eqtol(3) ) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function ge_3r

  function ge_3rt(r1, r2, rtol) result(res)
  ! greater than or equal for 3D coords
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3), rtol(3)
      ! Return
      logical              :: res

      if( ( ( r1(1).gt.r2(1) ) .or. ( abs(r1(1)-r2(1)).lt.rtol(1) ) ) .and. &
          ( ( r1(2).gt.r2(2) ) .or. ( abs(r1(2)-r2(2)).lt.rtol(2) ) ) .and. &
          ( ( r1(3).gt.r2(3) ) .or. ( abs(r1(3)-r2(3)).lt.rtol(3) ) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function ge_3rt

  function lt_3r(r1, r2) result(res)
  ! less than for 3D coords
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3)
      ! Return
      logical              :: res

      if ( ( r1(1) .lt. r2(1) ) .and. &
          ( r1(2) .lt. r2(2) ) .and. &
          ( r1(3) .lt. r2(3) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function lt_3r

  function le_3r(r1, r2) result(res)
  ! less than or equal for 3D coords
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3)
      ! Return
      logical              :: res

      if( ( ( r1(1).lt.r2(1) ) .or. ( abs(r1(1)-r2(1)).lt.eqtol(1) ) ) .and. &
          ( ( r1(2).lt.r2(2) ) .or. ( abs(r1(2)-r2(2)).lt.eqtol(2) ) ) .and. &
          ( ( r1(3).lt.r2(3) ) .or. ( abs(r1(3)-r2(3)).lt.eqtol(3) ) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
      end if
  end function le_3r

  function le_3rt(r1, r2, rtol) result(res)
  ! less than or equal for 3D coords
      implicit none
      ! Params
      real(dp), intent(in) :: r1(3), r2(3), rtol(3)
      ! Return
      logical              :: res

      if( ( ( r1(1).lt.r2(1) ) .or. ( abs(r1(1)-r2(1)).lt.rtol(1) ) ) .and. &
          ( ( r1(2).lt.r2(2) ) .or. ( abs(r1(2)-r2(2)).lt.rtol(2) ) ) .and. &
          ( ( r1(3).lt.r2(3) ) .or. ( abs(r1(3)-r2(3)).lt.rtol(3) ) ) ) then
          res = .true.
          return
      else
          res = .false.
          return
        end if
      end function le_3rt

  FUNCTION losc_frac2cart(xf) RESULT(xc)
      ! fractional coords to cartesian in alat units
      USE cell_base, ONLY: at
      IMPLICIT NONE
      REAL(dp), INTENT(IN) :: xf(3)
      REAL(dp)             :: xc(3)
      ! slice notation on fast index
      xc(1:3) = xf(1)*at(1:3,1) + xf(2)*at(1:3,2) + xf(3)*at(1:3,3)
      RETURN 
  END FUNCTION losc_frac2cart
    
    
  FUNCTION losc_cart2frac(xc) RESULT(xf)
      ! cartesian coords (in alat units) to fractional coords
      USE cell_base, ONLY: bg
      IMPLICIT NONE
      REAL(dp), intent(IN) :: xc(3)
      REAL(dp)             :: xf(3)
    
      xf(1) = xc(1)*bg(1,1) + xc(2)*bg(2,1) + xc(3)*bg(3,1)
      xf(2) = xc(1)*bg(1,2) + xc(2)*bg(2,2) + xc(3)*bg(3,2)
      xf(3) = xc(1)*bg(1,3) + xc(2)*bg(2,3) + xc(3)*bg(3,3)
      RETURN
  END FUNCTION losc_cart2frac


  FUNCTION losc_frac2cart_rec(xf) RESULT(xc)
      ! fractonal coords to cartesian in 2pi/alat units
      ! in reciprocal space (for k-points), weird cuz needs bg^T
      USE cell_base, ONLY: bg
      IMPLICIT NONE 
      REAL(dp), INTENT(IN) :: xf(3)
      REAL(dp)             :: xc(3)

      xc(1) = xf(1)*bg(1,1) + xf(2)*bg(2,1) + xf(3)*bg(3,1)
      xc(2) = xf(1)*bg(1,2) + xf(2)*bg(2,2) + xf(3)*bg(3,2)
      xc(3) = xf(1)*bg(1,3) + xf(2)*bg(2,3) + xf(3)*bg(3,3)
    
      RETURN
  END FUNCTION losc_frac2cart_rec
    
    
  FUNCTION losc_cart2frac_rec(xc) RESULT(xf)
      ! cartesian coords (in alat units) to fractional coords
      ! in reciprocal space (for k-points)
      USE cell_base, ONLY: at
      IMPLICIT NONE
      REAL(dp), INTENT(IN) :: xc(3)
      REAL(dp)             :: xf(3)
    
      xf(1) = at(1,1)*xc(1) + at(2,1)*xc(2) + at(3,1)*xc(3)
      xf(2) = at(1,2)*xc(1) + at(2,2)*xc(2) + at(3,2)*xc(3)
      xf(3) = at(1,3)*xc(1) + at(2,3)*xc(2) + at(3,3)*xc(3)
      
      RETURN 
  END FUNCTION losc_cart2frac_rec


  !-----------------------------------------------------------------------
  SUBROUTINE scan_to_begin (iun, keyword, found)
  !-----------------------------------------------------------------------
  ! scan a file until the line is found: 'begin keyword'
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: iun
  CHARACTER(len=*), intent(in) :: keyword
  logical, intent(out) :: found
  CHARACTER(len=80) :: line1, line2

  rewind (iun)

  found = .true.
  DO WHILE ( found )
      READ(iun,*,end=209) line1, line2
      IF(line1/='begin') CYCLE
      IF(line2/=keyword) CYCLE
      RETURN
  END DO
  209 found=.false.
  rewind (iun)

  END SUBROUTINE scan_to_begin


  ! MPI routines
  subroutine mpi_queue(wsz, asz, aio)
      ! MPI modules
  USE mp,          ONLY : mp_bcast !, mp_barrier
  USE mp_world,    ONLY : world_comm
  USE mp_global,   ONLY : npool, mp_startup, intra_image_comm
  use parallel_include
#if defined(__MPI)
  ! Params
  integer,  intent(in)    :: wsz ! world size
  integer,  intent(in)    :: asz
  real(dp), intent(inout) :: aio(:) ! dim(asz)
  ! Locals
  integer  :: i1, i2!, j1, j2
  integer  :: ii, jj, msent
  ! MPI vars
  integer  :: ierr                 ! error message
  integer  :: wrank                ! world rank (process id)
  integer  :: wsm1                 ! world size, wsz - 1
  ! 1 = starting index / values < 1 for commands
  ! 2 = stopping index
  integer,  allocatable :: mpmsgi(:)        ! MPI message
  real(dp), allocatable :: mpmsgr(:)        ! MPI message
  integer  :: mpilim               ! safety loop limiter
  integer  :: jobsz, jcnt, jmax    ! job size to distribute, count, sent
  integer  :: ircv, jsnt, jrec
  integer, allocatable :: jdistro(:,:)  ! distribution across processes
  integer, allocatable :: request(:)  ! MPI request handles
  integer  :: status(MPI_STATUS_SIZE) ! MPI status

  ! progress bar printing
  integer, parameter :: plim = 50 ! progress bar size
  integer  :: pcurr, pprev, ip    ! counters

  wsm1 = wsz - 1
  jobsz = asz
  jcnt = 0
  jmax = 0

  pprev  = 0
  pcurr  = 0
  write(6,'(5x,"|---------------mpi_queue-progress-----------------|")')
  write(6,'(5x,"|")',advance='no'); call flush(6)

  mpilim = 0
  jsnt = 0
  jrec = 0

  allocate( mpmsgi(mpmsz) )
  allocate( mpmsgr(mpmsz) )
  allocate( jdistro(4,wsm1) )
  allocate( request(wsm1) )

  do i1 = 1, wsm1 ! send out initial jobs

      if ( jmax .lt. jobsz ) then
          mpmsgi(1) = jmax + 1     !(i1-1)*mpmsz + 1
          mpmsgi(2) = jmax + mpmsz !i1*mpmsz
          if ( mpmsgi(2) .gt. jobsz ) mpmsgi(2) = jobsz
          jmax = mpmsgi(2)
          jdistro(1,i1) = mpmsgi(1)
          jdistro(2,i1) = mpmsgi(2)
      else
          mpmsgi(1) = -1
          jdistro(1,i1) = 0
          jdistro(2,i1) = 0
      end if

  !      write(6,'("sending to (",i0,"): ",i0,1x,i0)') i1, mpmsgi(1:2)

      jsnt = jsnt + 1
      call mpi_send(mpmsgi, mpmsz, MPI_INTEGER, &
                      i1, 0, WORLD_COMM, status, ierr)

      if ( mpmsgi(1) .ne. -1 ) then
          jrec = jrec + 1
          call mpi_irecv(mpmsgr, mpmsz, MPI_DOUBLE_PRECISION, &
                          i1, 1, WORLD_COMM, request(i1), ierr)
      end if

  end do
  dompi1: do while ( (jcnt.lt.jobsz) .and. (mpilim.lt.jobsz) ) ! loop til jobs finished

      mpilim = mpilim + 1
      call mpi_waitany(jrec, request, ircv, status, ierr)

      if ( (ircv.lt.1) .or. (ircv.gt.wsz) ) then
          write(6,'(/,"Err, ircv=",i0," jcnt=",i0,", jmax=",i0,&
                  ", request:")') ircv, jcnt, jmax
          write(6,'(i0,1x)',advance='no') (request(i1), i1=1,wsm1)
          write(6,'(/,"Received: ",10(f6.2,1x))') mpmsgr
          exit dompi1
      end if

      i1 = jdistro(1,ircv)
      i2 = jdistro(2,ircv)
      msent = jdistro(2,ircv) - jdistro(1,ircv) + 1
      aio(i1:i2) = mpmsgr(1:msent)
      jcnt = jcnt + msent

      pcurr = nint( real(plim*jcnt,dp)/real(jobsz,dp) ) 
      if ( pcurr .gt. pprev ) then ! update progress bar
          write(6,'(a)',advance='no') repeat('#',pcurr-pprev); call flush(6)
          pprev = pcurr
      end if

      mpmsgr(1:mpmsz) = 0.0_dp

      if ( jmax.lt.jobsz ) then

          mpmsgi(1) = jmax + 1     !jsnt*mpmsz + 1
          mpmsgi(2) = jmax + mpmsz !(jsnt+1)*mpmsz
          if ( mpmsgi(2) .gt. jobsz ) mpmsgi(2) = jobsz
          jmax = mpmsgi(2)

          jdistro(1,ircv) = mpmsgi(1)
          jdistro(2,ircv) = mpmsgi(2)

          jsnt = jsnt + 1
          call mpi_send(mpmsgi, mpmsz, MPI_INTEGER, &
                          ircv, 0, WORLD_COMM, status, ierr)

          ! @ERR: irecv needs a message buffer for each process or it could
          ! possibly be overwritten by another message before it's successfully
          ! read by the master process
          ! or use irecv with the MPI_ANY_SOURCE as the source and query the 
          ! status for who sent the message (in C, does that work in fortran?)
          call mpi_irecv(mpmsgr, mpmsz, MPI_DOUBLE_PRECISION, &
                          ircv, 1, WORLD_COMM, request(ircv), ierr)

      else

          mpmsgi(1) = -1
          jsnt = jsnt + 1
          call mpi_send(mpmsgi, mpmsz, MPI_INTEGER, &
                          ircv, 0, WORLD_COMM, status, ierr)
      end if

  end do dompi1

  deallocate( mpmsgi )
  deallocate( mpmsgr )
  deallocate( request )
  deallocate( jdistro )

  if ( plim .gt. pprev ) then
      write(6,'(a)',advance='no') repeat('#',plim-pprev); call flush(6)
  end if
  write(6,'("|")') ! finish progress bar
  write(6,'(5x,"|--------------------------------------------------|")')
  call flush(6)

#else
  call errore('MPI_QUEUE','attempting call without MPI compile',13)
#endif
  end subroutine mpi_queue


  subroutine mpi_qcalc(metric)!ionode, aio, dim1, dim2, metric)
  ! use an MPI queue to caluclate array aio
  ! should I flatten the array so it's always 1d for the same metric?
  ! it's a matrix so it's ok to keep at least 2d, can flatten higher
  ! dims into one of the 2 dims
  ! idea for new mode: send list of indices that share a V so can keep same V
  ! is the whole idea of reusing V good if I only access it sparsely?
  USE mp_world,    ONLY : world_comm
  use parallel_include
#if defined(__MPI)
  ! Params
  real(dp), external :: metric
  ! tried to do an interface, but it complains kind=dp is not a 
  ! compile time constant
  ! Locals
  integer  :: i1, ii, jj
  ! MPI vars
  integer  :: ierr                 ! error message
  integer  :: wrank                ! world rank (process id)
  integer,  allocatable :: mpmsgi(:)        ! MPI message
  real(dp), allocatable :: mpmsgr(:)        ! MPI message
  integer  :: mpilim               ! safety loop limiter
  integer  :: ijob
  integer  :: status(MPI_STATUS_SIZE) ! MPI status

  allocate( mpmsgi(mpmsz) )
  allocate( mpmsgr(mpmsz) )

  call mpi_comm_rank(WORLD_COMM, wrank, ierr)
  ijob = 0
  mpilim = 0

  do while ( (ijob .ne. -1) )! .and. (mpilim .lt. jobsz) )

      call mpi_recv(mpmsgi, mpmsz, MPI_INTEGER, &
                      0, 0, WORLD_COMM, status, ierr)

      ijob = mpmsgi(1)
      if ( ijob .ne. -1 ) then

          do i1 = mpmsgi(1), mpmsgi(2)
              mpmsgr(i1-mpmsgi(1)+1) = metric(i1)
          end do

          call mpi_send(mpmsgr, mpmsz, MPI_DOUBLE_PRECISION, &
                          0, 1, WORLD_COMM, ierr)
          mpilim = mpilim + 1
      end if

  end do

  deallocate( mpmsgi )
  deallocate( mpmsgr )
#else
  call errore('MPI_QCALC','attempting call without MPI compile',13)
#endif
  end subroutine mpi_qcalc

END MODULE utils