! Copyright (C) Quantum ESPRESSO, 2021. This file is distributed under the terms
! of the GNU General Public License, https://www.gnu.org/copyleft/gpl.txt

!!! ------------------------------------------------------------------------ !!!
!!!                     Coulomb part of LOSC curvature                       !!!
!!! ------------------------------------------------------------------------ !!!
MODULE coulomb
    ! Calculates the Coulomb repulsion between two local densities using
    !   kernel-based (erfc, Yukawa screening and spherical cutoff) methods.

    USE kinds,                  ONLY : dp
    USE constants,              ONLY : pi, tpi, fpi
    USE sys,                    ONLY : alatlosc, tpiba2losc 
    USE supercell_fft,          ONLY : gglosc, ngmlosc_g, gstart_losc

    IMPLICIT NONE

    REAL(dp), ALLOCATABLE           :: kernel(:)
    LOGICAL                         :: lkernel = .FALSE.

    CONTAINS

    SUBROUTINE coulomb_compatibility_check(J_ij)
        ! Not currently set up for gamma_only or noncollinear calculation
        USE control_flags,          ONLY : gamma_only
        USE noncollin_module,       ONLY : npol
        REAL(dp), INTENT(INOUT)     :: J_ij

        IF (gamma_only) THEN
            WRITE(6,*) "Not set up for gamma_only. Exiting..."
            J_ij = -1.0_dp
        ELSE IF (npol > 1) THEN
            WRITE(6,*) "Not set up for noncollinear calculation. Exiting..."
            J_ij = -2.0_dp
        ELSE
            J_ij = 0.0_dp
        END IF

        RETURN   
    END SUBROUTINE coulomb_compatibility_check

    SUBROUTINE init_coulomb(k, type)

      IMPLICIT NONE

      REAL(DP), INTENT(IN)            :: k        ! screening parameter
      CHARACTER(LEN=80), INTENT(IN)   :: type     ! screening type
      INTEGER                         :: icoul    ! screening type int index

      ! compute_kernel() is indexed by an integer
      IF (type .eq. 'pw') THEN            ! No correction to periodic Coulomb
        icoul = 1
      ELSE IF (type .eq. 'sphcut') THEN   ! spherical cutoff
        icoul = 2
      ELSE IF (type .eq. 'yukawa') THEN   ! Yukawa screening, no cutoff
        icoul = 3 
      ELSE IF (type .eq. 'erfc') THEN     ! erfc screening, no cutoff
        icoul = 4
      ELSE IF (type .eq. 'yukcut') THEN   ! Yukawa screening with cutoff
        icoul = 5
      ELSE IF (type .eq. 'erfcut') THEN   ! erfc screening with cutoff
        icoul = 6
      ELSE
        CALL errore('LOSC',"invalid kjtype in inputpp list",1)
      END IF

      CALL compute_kernel(icoul, k)

    END SUBROUTINE init_coulomb 

    SUBROUTINE dealloc_coulomb()

      IF (ALLOCATED(kernel)) DEALLOCATE(kernel)

    END SUBROUTINE dealloc_coulomb 


    function coulomb_planewave(ib1, ib2, iuc, ctyp) result(ret)

        ! need to get LWFs, shuffle according to isc, map to SC, then FFT
        ! ideally the shuffle and map should be done in the same step
        ! to start I'll just ignore the SC index
        
        USE cell_base,       ONLY : alat, omega
        USE sys,             ONLY : idwfsz, dwfft, afft, id2uc, ib2dwf, pscshoff
        USE wannier_density, ONLY : dwf_uc2sc, losc_get_dwf2, losc_get_dwfn0, &
                                   dwf_shift_g, losc_load_dwf_g, losc_get_dwf2_g
        ! Params
        integer, intent(in) :: ib1, ib2 ! LWF band index
        integer, intent(in) :: iuc      ! relative unit cell index
        integer, intent(in) :: ctyp     ! Periodic correction type
        ! Locals
        integer     :: id1, id2
        integer     :: idr1, idr2
        integer     :: idg1, idg2
        integer  :: ig
        real(dp) :: ret
        
        ! 2021-09-13: Currently slow because losc_get_dwf2_g() does not pre-check
        ! whether a DWF is loaded.
        !  call losc_get_dwf2_g(ib1, ib2, iuc, id1, id2)
        !  ret = coulomb_screen(dwfft(:,id1), dwfft(:,id2))
        
        if (ib1 .eq. ib2) then ! diagonal in band
        
            id1 = losc_get_dwfn0(ib1, lG=.TRUE.)
            id2 = MOD ( id1, idwfsz ) + 1
            dwfft(:,id2) = dwfft(:,id1)
            id2uc(id2) = pscshoff
            !ib2dwf(ib2) = id2
            call dwf_shift_g(id2, iuc)
        
        else
            id1 = ib1
            id2 = ib2
            call losc_get_dwf2(id1, id2, lG=.TRUE.)
            call dwf_shift_g(id2, iuc)
        
        end if
        
        !  else if( iuc.eq. pscshoff )then   ! off-diagonal band, diagonal unit cell
        !  
        !    id1 = ib1
        !    id2 = ib2
        !    call losc_get_dwf2(id1, id2, lG=.TRUE.)
        !
        !  else if( ib1.eq.ib2 )then     ! diagonal band, off-diag unit cell
        !  
        !    id1 = losc_get_dwfn0(ib1, lG=.TRUE.)
        !    id2 = MOD( id1, idwfsz) + 1 ! just set to the next valid index
        !    
        !    call dwf_shift_g(id2, iuc)    ! shift rhoj(G) to unit cell offset
        !  
        !  else                          ! off-diag in band and unit cell
        !  
        !    id1 = ib1
        !    id2 = ib2
        !    call losc_get_dwf2(id1, id2, lG=.TRUE.)
        !    call dwf_shift_g(id2, iuc)    ! shift rhoj(G) to unit cell offset
        !  
        !  end if
        !
        ret = coulomb_screen(dwfft(:,id1), dwfft(:,id2))
        
        return
    end function coulomb_planewave

    !!! -------------------------------------------------------------------- !!!
    !!!                     Coulomb integral subroutines                     !!!
    !!! -------------------------------------------------------------------- !!!
    FUNCTION coulomb_screen(rhog1, rhog2) RESULT(J_screen)
        USE sys,       ONLY : omegalosc

        IMPLICIT NONE

        ! Parameters
        COMPLEX(dp)             :: rhog1(:), rhog2(:)
        REAL(dp)                :: J_screen

        INTEGER                 :: ig
        REAL(dp)                :: pwnorm

        CALL start_clock("coulomb_screen")

        pwnorm = ABS(rhog1(1)) * ABS(rhog2(1))

        J_screen = 0.0_dp
        !$OMP PARALLEL DO REDUCTION(+:J_screen)
        DO ig = 1, ngmlosc_g
            J_screen = J_screen + kernel(ig) * ( &
                            REAL(rhog1(ig)) * REAL(rhog2(ig)) + &
                            AIMAG(rhog1(ig)) * AIMAG(rhog2(ig)) )
        END DO
        !$OMP END PARALLEL DO

        ! Correct units
        J_screen = J_screen / (omegalosc * pwnorm)

        CALL stop_clock("coulomb_screen")
        RETURN
    END FUNCTION coulomb_screen
    

    !!! -------------------------------------------------------------------- !!!
    !!!                     Coulomb kernels for screening                    !!!
    !!! -------------------------------------------------------------------- !!!
    SUBROUTINE compute_kernel(ikjtyp, kin, Rin)
        USE sys,               ONLY : atlosc 
        IMPLICIT NONE

        INTEGER, INTENT(IN)             :: ikjtyp   ! screening type
        REAL(dp), OPTIONAL              :: kin
        REAL(dp), OPTIONAL              :: Rin

        REAL(dp)                        :: k        ! screening parameter (a.u.)
        REAL(dp)                        :: R        ! cutoff length (a.u.)
        REAL(dp)                        :: a2min    ! half primitive cell (a.u.)
        REAL(dp)                        :: eps      ! k = (k < eps) ? k : 0

        CALL start_clock("compute_kernel")
        IF (lkernel) RETURN
        IF (.NOT. ALLOCATED(kernel)) ALLOCATE( kernel(ngmlosc_g) )

        ! Default values
        eps = 1.0E-6_dp

        a2min = MIN( SUM(atlosc(:,1) * atlosc(:,1)), &
                     SUM(atlosc(:,2) * atlosc(:,2)), &
                     SUM(atlosc(:,3) * atlosc(:,3))  &
                   )
        R = SQRT(a2min) * alatlosc / 2.0_dp
        k = 0.15_dp

        IF (PRESENT(Rin)) R = Rin
        IF (PRESENT(kin)) k = kin

        SELECT CASE (ikjtyp)
        CASE (2)                                    ! Spherical cutoff only
            CALL kernel_sphcut(R, kernel)
        CASE (3)                                    ! Yukawa
            IF (k < eps) THEN
                kernel = fpi / (gglosc * tpiba2losc)
                kernel(1) = 0.0_dp
            ELSE
                CALL kernel_yukawa(k, kernel)
            END IF
        CASE (4)                                    ! erfc
            IF (k < eps) THEN
                kernel = fpi / (gglosc * tpiba2losc)
                kernel(1) = 0.0_dp
            ELSE
                CALL kernel_erfc(k, kernel)
            END IF
        CASE (5)                                   ! Yukawa + cutoff
            IF (k < eps) THEN
                CALL kernel_sphcut(R, kernel)
            ELSE
                CALL kernel_yukcut(k, R, kernel)
            END IF
        CASE (6)                                    ! erfc + cutoff    
            IF (k < eps) THEN
                CALL kernel_sphcut(R, kernel)
            ELSE
                CALL kernel_erfcut(k, R, kernel)
            END IF
        CASE DEFAULT                                ! No screening or cutoff
            kernel = fpi / (gglosc * tpiba2losc)    ! a.u.
            kernel(1) = 0.0_dp                      ! default: skip G=0 term
        END SELECT

        lkernel = .TRUE.
        CALL stop_clock("compute_kernel")
        RETURN
    END SUBROUTINE compute_kernel 


    SUBROUTINE kernel_sphcut(R, ker)
        ! Cut off the Coulomb kernel at R, so that
        !   kernel(r) = HeavisideTheta(r - R)/r. 
        ! Ref: doi:10.1103/PhysRevB.73.205119
        IMPLICIT NONE

        REAL(dp), INTENT(IN)        :: R
        REAL(dp), INTENT(OUT)       :: ker(ngmlosc_g)

        REAL(dp)                    :: G, G2
        INTEGER                     :: ig

        ker(1) = tpi * R**2.0_dp                    ! G = 0 term 

        DO ig = gstart_losc, ngmlosc_g
            G2 = gglosc(ig) * tpiba2losc
            G = SQRT(G2)
            ker(ig) = fpi * (1.0_dp - COS(G * R)) / G2
        END DO

        RETURN
    END SUBROUTINE kernel_sphcut

    SUBROUTINE kernel_yukawa(k, ker)
        ! Yukawa-type Coulomb kernel: kernel(r) = exp(-kr)/r
        IMPLICIT NONE

        REAL(dp), INTENT(IN)        :: k
        REAL(dp), INTENT(OUT)       :: ker(ngmlosc_g)
        REAL(dp)                    :: G2
        INTEGER                     :: ig

        DO ig = 1, ngmlosc_g
            G2 = gglosc(ig) * tpiba2losc            ! a.u.
            ker(ig) = fpi / (G2 + k**2.0_dp)
        END DO

        RETURN
    END SUBROUTINE kernel_yukawa

    SUBROUTINE kernel_erfc(k, ker)
        ! Complementary erf screening: kernel(r) = erfc(kr)/r
        IMPLICIT NONE

        REAL(dp), INTENT(IN)        :: k
        REAL(dp), INTENT(OUT)       :: ker(ngmlosc_g)

        REAL(dp)                    :: G, G2
        INTEGER                     :: ig

        ker(1) = pi / k**2.0_dp                     ! G = 0 term
        
        DO ig = gstart_losc, ngmlosc_g
            G2 = gglosc(ig) * tpiba2losc
            G = SQRT(G2)
            ker(ig) = fpi * ( 1 - EXP(-G2 / (4.0_dp * k**2.0_dp)) ) / G2
        END DO

        RETURN
    END SUBROUTINE kernel_erfc
 
    SUBROUTINE kernel_yukcut(k, R, ker)
      ! Yukawa screening with cutoff: kernel(r) = exp(-kr)*Theta(r-R)/r
      IMPLICIT NONE

      REAL(dp), INTENT(IN)        :: k, R
      REAL(dp), INTENT(OUT)       :: ker(ngmlosc_g)

      REAL(dp)                    :: G, G2, ekR
      INTEGER                     :: ig

      ekR = EXP(-k * R)

      ! G = 0 term
      ker(1) = (fpi / k**2.0_dp) * ( 1.0_dp - ekR - k * R * ekR )

      DO ig = gstart_losc, ngmlosc_g
          G2 = gglosc(ig) * tpiba2losc
          G = SQRT(G2)
          ker(ig) = ( fpi / (G2 + k**2.0_dp) ) * &
              ( 1.0_dp - (ekR * COS(G * R)) - (k * ekR / G) )
      END DO 

      RETURN
    END SUBROUTINE kernel_yukcut

    SUBROUTINE kernel_erfcut(k, R, ker)
        ! Erfc screening with cutoff: kernel(r) = erfc(kr)*Theta(r-R)/r

        ! The code computing the Faddeyeva function w(z) is released under the
        ! ACM license. References:
        !   Algorithm 916; Zaghloul and Ali 2012, doi:10.1145/2049673.2049679
        !   Remark on Algorithm 916; Zaghloul 2016, doi:10.1145/2806884
        USE Faddeyeva_v2_mod_rk,    ONLY : Faddeyeva_v2_rk ! (in, out, digits)
        IMPLICIT NONE

        REAL(dp), INTENT(IN)        :: k, R
        REAL(dp), INTENT(OUT)       :: ker(ngmlosc_g)   ! The kernel

        COMPLEX(dp)                 :: w, z             ! w=Faddeeva(z)
        REAL(dp)                    :: G, G2, V, L      ! w=V+iL
        REAL(dp)                    :: GR, kR, Gb2k
        INTEGER                     :: ig

        kR = k * R

        ! G = 0 term
        ker(1) = ( tpi * R * R ) + &
                      ( pi * ERF(kR) * &
                        ( (1.0_dp / (k * k)) - (2.0_dp * R * R) ) ) - &
                      ( 2.0_dp * SQRT(pi) * EXP(-kR * kR) * R / k )

        DO ig = gstart_losc, ngmlosc_g
            G2 = gglosc(ig) * tpiba2losc
            G = SQRT(G2)
            GR = G * R
            Gb2k = G / (2.0_dp * k)

            z = CMPLX(-Gb2k, kR, KIND=dp)

            CALL Faddeyeva_v2_rk(z, w, sdgts=12)    ! 12 digits precision
            V = REAL(w)
            L = AIMAG(w)

            ker(ig) = ( fpi / G2 ) * &
                   ( 1.0_dp - COS(GR) * ERFC(kR) - EXP(-Gb2k * Gb2k) + &
                     EXP(-kR * kR) * ( V * COS(GR) + L * SIN(GR) ) &
                   )
        END DO

        RETURN
    END SUBROUTINE kernel_erfcut

    SUBROUTINE kernel_trivial(ker)
        ! Trivial kernel (all entries 1)
        IMPLICIT NONE

        REAL(dp), INTENT(OUT)       :: ker(ngmlosc_g)

        ker(:) = 1.0_dp
        RETURN
    END SUBROUTINE kernel_trivial

END MODULE coulomb 