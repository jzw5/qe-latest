!!!--------------------------------------------------------------------------!!!
! Copyright (C) 2022 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!!!--------------------------------------------------------------------------!!!
! This program calculates the LOSC energy correction in bulk materials.
!
! Written by Aaron Mahler and Jacob Williams.
! Last modified: 2022-04-29
! 
! Acronyms: (L)BO: (Localized) Bloch Orbital
!           (L)WF: (Localized) Wannier Function
!!!--------------------------------------------------------------------------!!!

!!!--------------------------------------------------------------------------!!!
PROGRAM losc
!!!--------------------------------------------------------------------------!!!
  !! -- constants
  USE kinds,            ONLY : dp
  !! -- Quantum ESPRESSO variables
  USE environment,      ONLY : environment_start, environment_end
  !! -- I/O modules
  USE io_files,         ONLY : prefix, tmp_dir
  USE io_global,        ONLY : ionode, ionode_id 
  !! -- MPI modules
  USE mp,               ONLY : mp_bcast, mp_barrier
  USE mp_world,         ONLY : world_comm
  USE mp_global,        ONLY : npool, mp_startup 
  use parallel_include
  !! -- LOSC variables
  USE pw2losc,          ONLY : nks, nkstot, nspin, et
  USE wan2losc,         ONLY : nwan, u_wan, u_loc
  USE sys,              ONLY : pscshsz, etd 
  USE sys,              ONLY : nksz, pscshsz, etd, spin_offset 
  USE utils,            ONLY : nodenam
  USE wannier_density,  ONLY : scrfil
  USE occupation,       ONLY : limdei, limdeb, limdet, occupk, occupm, nocc
  USE curvature,        ONLY : kappa2, k2tar, kappam, kmtar
  USE curvature,        ONLY : kjm, kjtar, kxm, kxtar, ovsqrt, ovstar
  !! -- LOSC methods
  USE pw2losc,          ONLY : read_pwscf, pw2losc_print
  USE wan2losc,         ONLY : read_nnkp, read_u_dis, read_u_wan, calc_u_loc, &
                               save_u_loc, read_wannier_centres, &
                               wan2losc_print, dealloc_wannier_unitaries
  USE sys,              ONLY : losc_setup, set_spin_offset, sys_print
  USE utils,            ONLY : set_io_unit 
  USE supercell_fft,    ONLY : supercell_fft_create 
  USE wannier_density,  ONLY : wan_to_home_cell, losc_alloc_dwf, dwf_save_g, &
                               dwf_delete, losc_dealloc_dwf, &
                               supercell_fft_read, supercell_fft_write, &
                               construct_dlwf
  USE coulomb,          ONLY : init_coulomb, dealloc_coulomb
  USE occupation,       ONLY : init_occupation, canonical_occupation_k, &
                               local_occupation_k, local_occupation_r, &
                               occupation_print, curvature_cutoffs, &
                               dealloc_occupation
  USE curvature,        ONLY : init_curvature, dealloc_curvature, &
                               print_cutoffs, compute_curvature, & 
                               fill_coulomb, fill_exchange, fill_overlap
!!! ------------------------------------------------------------------------ !!!
                      
  IMPLICIT NONE

  CHARACTER(LEN=256), external :: trimcheck

  ! &inputpp namelist variables
  character(len=80)  :: seedname   ! wan90 seedname
  CHARACTER(len=256) :: outdir     ! QE ouput dir
  CHARACTER(len=256) :: scrdir     ! QE ouput dir
  character(len=80)  :: kjtype     ! coulomb calc method (pw or gto)
  logical            :: lplot      ! plotting bool
  logical            :: is_metal   ! is it a metal?
  integer            :: iprint     ! printing verbosity
  logical            :: pwxsave    ! save pw.x output (hack for MPI read in)
  logical            :: kapcalc    ! wheter to caclulate curvature
  real(dp)           :: kscreen    ! Coulomb screening parameter
  real(dp)           :: tauscreen  ! Exchange prefactor
  character(len=80)  :: seednam_up ! Up spin seedname
  character(len=80)  :: seednam_dw ! Down spin seedname

  ! I/O variables
  character(len=256) :: filename   ! for opening files
  character(len=32)  :: sjobid     ! slurm job id
  character(len=32)  :: user       ! user name
  logical            :: found      ! seedname bool

  ! MPI variables
  integer            :: ierr       ! error message
  integer            :: wrank      ! world rank (process id)
  integer            :: wsz        ! world size
  integer            :: iarray     ! array size for MPI curvature computation
  LOGICAL            :: ldebug = .FALSE.

  ! Spin polarization variables
  character(len=80)  :: seed_array(2)     ! array of seednames
  integer            :: ispin

  ! LOSC correction variables
  real(dp)                :: detot        ! total energy correction
  real(dp), allocatable   :: detd(:,:)    ! orbital energy change (disentangled)

!!! ------------------------------------------------------------------------ !!!

  namelist / inputpp  / outdir, prefix, seedname, kjtype, iprint, &
                        lplot, pwxsave, scrdir, kscreen, tauscreen, &
                        is_metal, seednam_up, seednam_dw 

  ! inputpp default values
  prefix   = 'pwscf'
  seedname = 'wan'
  kjtype   = 'pw'
  iprint   = 0
  lplot    = .false.
  is_metal = .false.
  pwxsave  = .false.
  !scrdir   = '/scr'
  scrdir   = '.'
  kscreen = 0.15_dp
  tauscreen = 6.0_dp * (1.0_dp - 2.0_dp**(-1.0_dp/3.0_dp))
  seednam_up = 'wan_up'
  seednam_dw = 'wan_dw'

  ! misc. default values
  kapcalc = .true.
  wsz     = 0
  wrank   = -1

#if defined(__DEBUG)
  ldebug = .TRUE.
#endif
  ! MPI startup
#if defined(__MPI)
  CALL mp_startup ()
  call mpi_comm_rank(WORLD_COMM, wrank, ierr)
  call mpi_comm_size(WORLD_COMM, wsz, ierr)
#endif
  if ( ionode ) then
      write(6,'("World size: ",i4," rank: ",i4, " ionode: ", l1)') &
              wsz, wrank, ionode
  end if

  CALL environment_start ( 'LOSC' )
  !IF ( npool > 1 ) CALL errore('losc','pools not implemented',npool)
  CALL get_environment_variable( 'SLURMD_NODENAME', nodenam )
  CALL set_io_unit(filename, wrank, ldebug)

  ! Read in pwscf quantities and print needed ones
  CALL read_pwscf(iprint)

  CALL losc_initialize(outdir, scrdir, sjobid, user, wsz, pwxsave, &
                       kscreen, tauscreen, kapcalc, seed_array)

  ! Print pwscf quantities
  CALL print_cell_ions_base(iprint)

  ! Set up LOSC real and reciprocal grid and Born-von Karman cell
  CALL losc_setup()

  ! Loop over spin components!
  ! Initialize input variables
  !IF ( ionode ) THEN
  !  CALL losc_initialize(outdir, scrdir, sjobid, user, wsz, pwxsave, &
  !                       kscreen, tauscreen, kapcalc, seed_array)
  !END IF

  ! Read pwscf quantities
  !CALL read_from_pwscf(outdir, iprint)

#if defined(__MPI)
  ! Broadcast input variables
  CALL mp_bcast( prefix,   ionode_id, world_comm )
  CALL mp_bcast( outdir,   ionode_id, world_comm )
  CALL mp_bcast( tmp_dir,  ionode_id, world_comm )
  !CALL mp_bcast( scrfil,   ionode_id, world_comm )
  CALL mp_bcast( seedname, ionode_id, world_comm )
  CALL mp_bcast( kapcalc,  ionode_id, world_comm )
  CALL mp_bcast( kjtype,   ionode_id, world_comm )
#endif

  doSpin: DO ispin = 1, nspin
  ! Set seedname properly
  seedname = seed_array(ispin)
  CALL seed_check(seedname)
  
  ! Spin offset for pwscf arrays
  CALL set_spin_offset(nksz, ispin)
  CALL sys_print(iprint)
  
  ! Read wannier90 output
  call read_nnkp()                ! Read wan.nnkp; find excluded bands
  call read_u_dis()               ! Set dbnd and nwan then read u_dis
  call read_u_wan()               ! Set nwan if unset, read u_wan
  call calc_u_loc()               ! Initialize u_loc
  call read_wannier_centres()     ! Read LWF centers

  IF (ionode) THEN              
    CALL save_u_loc(seedname)  ! Print u_loc to a file
  END IF

  !WRITE(6,*) "wannier90 information for spin component", ispin
  !CALL wan2losc_print(iprint)

  ! Load Quantum ESPRESSO output
  !call pw2losc_read( scrfil, outdir, pwxsave, iprint ) 
  !call findset_plotvars(lplot) ! error checking needs nwan...
  !if ( wsz.le.1 ) then ! read from QE output
  !    call findset_plotvars(lplot) ! error checking needs nwan...
  !    call pw2losc_read( scrfil, outdir, pwxsave, iprint ) 
  !else ! open & read a pwxsave (placeholder for MPI calculations)
  !    !call pw2losc_open( nbnd, nks, u_wan, scrfil, outdir )
  !end if

  ! Write real (reciprocal) unit cell at(:) (bg(:)), and atomic positions tau(:)
  !if ( ionode .and. (iprint.gt.1) ) then
  !  CALL print_cell_ions_base()
  !end if

  ! Obtain the disentangled eigenvalues from the wannier90 output
  call eigs_disentangle()   

  ! Ensure the DLWFs are centered in the home supercell
  call wan_to_home_cell(iprint)

  ! Print wannier90 information
  WRITE(6,*) "wannier90 information for spin component", ispin
  CALL wan2losc_print(iprint)

  ! Compute the (possibly fractional) canonical occupations
  CALL init_occupation(nwan, pscshsz, nks)
  CALL canonical_occupation_k()
  
#if defined(__MPI)
    CALL mp_bcast( nocc,   ionode_id, world_comm )
#endif

  ! 2022-04-29: This is replaced by compatibility_check()
  ! Check system's xk vs. xkg from wannier90 input
  !if ( ionode ) then
  !  CALL check_k_mesh(seedname)
  !end if

  ! Ensure pwscf and wannier90 k-meshes are compatible
  CALL compatibility_check()

  ! Make the DLWFs and save to disk
  CALL construct_dlwf(scrfil, outdir)

  ! 2022-04-29: No MPI for the present
  CALL supercell_fft_create()
  CALL supercell_fft_write()
  CALL supercell_fft_read()
  CALL dwf_save_g()                      ! Save rho(G) to disk
  ! Supercell FFT does not work under MPI: call a serial run with pwxsave=.true.
  !     to save rho(G) and dfftlosc variables to disk
  !if (wsz .le. 1)then
  !  CALL supercell_fft_create()
  !  CALL supercell_fft_write()
  !  CALL supercell_fft_read()
  !  CALL dwf_save_g()                      ! Save rho(G) to disk
  !else
  !  CALL supercell_fft_read()              ! read dfftlosc variables from disk
  !end if

  !if( lplot ) CALL supercell_fft_plot()   ! 2022-04-01: not working atm

  ! Check LWF localization (2022-03-02: crashes here for nspin == 2)
  CALL occupation_print(seedname, iprint)

  !! -- Calculate local occupation ------------------------------------------ !!
  ! Compute local occupation matrix of transformed Bloch orbitals in k-space...
  CALL local_occupation_k(occupk, nocc, iprint)

  ! and Wannier functions in real space
  CALL local_occupation_r()

  ! Cutoffs: curvature elements that can be skipped
  CALL curvature_cutoffs()

  ifPrintKFac: if ( ionode .and. (iprint.gt.10) ) then
    CALL print_cutoffs(occupm)
  end if ifPrintKFac

  !! -- Calculate curvature ------------------------------------------------- !!
  !call mp_barrier( world_comm )
  ifKapCalc: if ( kapcalc ) then 

    CALL init_curvature(nwan, pscshsz)

    ! Coulomb calculation uses planewaves
    CALL losc_alloc_dwf(2)
    CALL init_coulomb(kscreen, kjtype)
    CALL fill_coulomb(kjm)
    CALL losc_dealloc_dwf()

    ! Exchange and overlap calculations are on the real grid
    CALL losc_alloc_dwf(2)
    CALL fill_exchange(kxm)
    CALL fill_overlap(ovsqrt)
    CALL losc_dealloc_dwf()
    !ifKapCalcMPI: if ( wsz .le. 1 ) then ! serial calculation

    !else ifKapCalcMPI ! MPI calculate
    !  CALL mpi_fill_curvature(kjtar, kxtar, ovstar, iarray, wsz)
    !end if ifKapCalcMPI

    ! Get LOSC1 and LOSC2 curvature matrices from the J, X, S elements
    CALL compute_curvature(iprint, limdei)
  end if ifKapCalc

  !! -- Calculate energy corrections ---------------------------------------- !!
  IF( ionode .and. kapcalc ) THEN
    ALLOCATE (detd(nwan,nks)) ! eig corrections

    ! Uncorrected band energies
    write(6,'(//5x,"Band energies before correction: ")')
    IF (is_metal) THEN
      CALL print_efermi(et, ip=iprint)
    ELSE
      call losc_print_eband(et,iprint=iprint)
    END IF

    ! LOSC1 energy correction
    !call losc_calc_de(kappam, occupm, u_wan, detd, detot)
    !detd(1:nwan,1:nks) = etd(1:nwan,1:nks) + detd(1:nwan,1:nks)

    !write(6,'(//5x,"Band energies after LOSC1: ")')
    !IF (lgauss) THEN
    !  CALL print_efermi(detd, ip=iprint)
    !ELSE
    !  call losc_print_eband(detd)
    !END IF
    !write(6,'(5x,"Total energy correction (Ry) ", f12.7)') detot

    ! Write LOSC1 eigenvalues to file
    !WRITE(filename,'(2(a))') trim(seedname), '.losc1.eig'
    !CALL write_eigenvalues(detd, filename)

    ! LOSC2 energy correction
    CALL losc_calc_de(kappa2, occupm, u_wan, detd, detot)
    detd(1:nwan,1:nks) = etd(1:nwan,1:nks) + detd(1:nwan,1:nks)

    ! Write LOSC2 eigenvalues to file
    WRITE(filename,'(2(a))') trim(seedname), '.losc.eig'
    CALL write_eigenvalues(detd, filename)

    WRITE(6,'(//5x,"Band energies after LOSC2: ")')
    IF (is_metal) THEN
      CALL print_efermi(detd, ip=iprint)
    ELSE
      CALL losc_print_eband(detd,iprint=iprint) !call print_ks_energies()
    END IF
    WRITE(6,'(5x,"Total energy correction (Ry) ", f12.7)') detot

    DEALLOCATE(detd)
  END IF
 
!! -- Final outputs --------------------------------------------------------- !!
  ifFinalIO: if(ionode) then
    CALL print_final(iprint)
  end if ifFinalIO

!! -- Deallocate memory ----------------------------------------------------- !!
  CALL dealloc_wannier_unitaries()
  CALL dealloc_occupation()
  if( kapcalc )then
    CALL dealloc_coulomb()
    CALL dealloc_curvature()
  end if

!! -- Delete scratch files -------------------------------------------------- !!
  !call dwf_delete()
  !call mp_barrier( world_comm )

!! -- Cleanup and timer output ---------------------------------------------- !! 
  ifTimePr: if ( ionode .and. (iprint.gt.0) ) then
    CALL print_timing(lplot, kapcalc)
  end if ifTimePr

  END DO doSpin

!! -- Delete scratch files -------------------------------------------------- !!
  call dwf_delete()
  !if( mpioun.gt.0 ) close(mpioun)
  CALL environment_end ( 'LOSC' )
  CALL stop_losc

  !! -- End main program ---------------------------------------------------- !!
  STOP

  CONTAINS
  
  SUBROUTINE losc_initialize(dirout, dirscr, jobid, username, iwsz, lsave, &
                             k, tau, lkappa, seedarr)
    USE pw2losc,            ONLY : nspin
    USE sys,                ONLY : losck, losctau
    USE wannier_density,    ONLY : scrfil

    IMPLICIT NONE

    CHARACTER(len=256), INTENT(INOUT)   :: dirout, dirscr
    CHARACTER(len=32), INTENT(INOUT)    :: jobid, username
    INTEGER, INTENT(IN)                 :: iwsz
    LOGICAL, INTENT(IN)                 :: lsave
    REAL(dp), INTENT(IN)                :: k, tau
    LOGICAL, INTENT(INOUT)              :: lkappa
    CHARACTER(LEN=80), INTENT(OUT)      :: seedarr(2) ! wannier90 seedname array


    CHARACTER(len=512)                  :: charbuff
    INTEGER                             :: ios

    CALL get_environment_variable( 'ESPRESSO_TMPDIR', dirout )
    IF ( TRIM( dirout ) == ' ' ) dirout = '.'

    CALL get_environment_variable( 'SLURM_JOBID', jobid )
    IF ( TRIM( jobid ) == ' ' ) jobid = '000'

    CALL get_environment_variable( 'USER', username )
    IF ( TRIM( username ) == ' ' ) username = 'aaa'

    if ( (jobid.eq.'000') .and. (wsz.gt.1) ) then
        charbuff = 'Need Job ID for MPI jobs '
        call errore('LOSC', charbuff, 1)
    end if
    
    CALL input_from_file()  ! handles -in case, connects to unit 5
    READ (5, inputpp, err=601, iostat=ios)
    
    ! Set system variables for later LOSC use
    losck = k               ! screening for Coulomb
    losctau = tau           ! tau: amount of exchange

    if ( lsave .and. (wsz.gt.1) ) then
        write(6,'(5x,"WARN: pwxsave ignored for world size > 1")')
    end if
    if ( lsave .and. (wsz.le.1) ) lkappa = .false.

    tmp_dir = trimcheck (dirout)

    if( trim(dirscr) .eq. '.' )then
       dirscr = trim(dirout)
    end if

    scrfil = trim(dirscr)//'/'//trim(username)//'.'//trim(jobid)

    ! Set seedname defaults
    IF (seedname == '' ) seedname = prefix
    IF (seednam_up == '') seednam_up = TRIM(seedname) // '_up'
    IF (seednam_dw == '') seednam_dw = TRIM(seedname) // '_dw'



    write(6,'(5x,"iprint    : ",i0)') iprint
    write(6,'(5x,"outdir    : ",a)'), trim(dirout)
    write(6,'(5x,"scrdir    : ",a)'), trim(dirscr)
    write(6,'(5x,"scrfile   : ",a)'), trim(scrfil)
    write(6,'(5x,"kjtype    : ",a)') kjtype
    write(6,'(5x,"kapcalc   : ",l)') lkappa 
    write(6,'(5x,"pwxsave   : ",l)') lsave 
    IF (nspin == 1) THEN
      seedarr(1) = seedname
      WRITE(6,*) "There are", nspin, " spin components."
      WRITE(6,'(5x,"seedname  : ",a)') seedname
    ELSE IF (nspin == 2) THEN
      seedarr(1) = seednam_up
      seedarr(2) = seednam_dw
      WRITE(6,*) "There are", nspin, " spin components."
      WRITE(6,'(5x,"spin-up seedname  : ",a)') seednam_up
      WRITE(6,'(5x,"spin-dw seedname  : ",a)') seednam_dw
    ELSE
      charbuff = "ERR(seed_setup): Unsupported nspin"
      CALL errore('LOSC', charbuff, nspin)
    END IF
    if( wsz .gt. 0 )  write(6,'(5x,"MPI JobID : ",a)') jobid
    if( wsz .gt. 0 )  write(6,'(5x,"IO Node   : ",a)') nodenam

    RETURN
  
    601  CALL errore( 'LOSC', 'ERR reading inputpp namelist', ABS (ios) )
  END SUBROUTINE losc_initialize


  SUBROUTINE seed_check(seed)
    USE wan2losc,                   ONLY : set_seed
    IMPLICIT NONE

    CHARACTER(LEN=80), INTENT(INOUT)     :: seed
    CHARACTER(LEN=512)                   :: charbuff

    if (seed == '' ) then
      seed = prefix
    end if
    call set_seed(seed)

    INQUIRE(file=trim(seed)//"_u.mat", exist=found)
    IF (.NOT. found ) then
        charbuff = 'file '//trim(seed)//'_u.mat not found'
        WRITE(6,*) 'ERR(seed_check): '//trim(charbuff)
        CALL ERRORE('LOSC', charbuff,1)
    END IF

    RETURN
  END SUBROUTINE seed_check


  SUBROUTINE compatibility_check()
    ! Check whether variables from pwscf and wannier90 - 
    !   lattice parameters, k-meshes, etc. - 
    ! are compatible
    ! 2022-03-30 Note: Replaces pwcheck=TRUE section of read_nnkp()

    USE cell_base,        ONLY : bg
    USE wan2losc,         ONLY : nwan, nkwan, xkwan
    USE pw2losc,          ONLY : nkstot, nspin, xk
    USE utils,            ONLY : neq_3rt

    IMPLICIT NONE

    INTEGER                   :: nkpw, ik
    REAL(dp)                  :: kdiff, xkcart(3)
    REAL(dp)                  :: tol(3) = 1.0E-6_dp

    nkpw = nkstot / nspin

    ! Check number of k-points
    IF (nkwan /= nkpw) THEN
      WRITE(6,*) "nkwan = ", nkwan, " nkpw = ", nkpw
      CALL errore("compatibility_check", &
                  "Different number of k-points between pwscf and w90!", &
                  nkpw - nkwan)
    END IF

    ! Ensure k-mesh is the same (up-spin k-points from PWSCF)
    DO ik = 1, nkpw
      xkcart(:) = xkwan(:, ik)
      CALL cryst_to_cart(1, xkcart, bg, 1)
      IF (neq_3rt(xkcart(:), xk(:, ik), tol)) THEN
        CALL errore("compatibility_check", &
                    "Mismatch between pwscf and w90 k-point mesh!", &
                    ik)
      END IF
    END DO

    WRITE(6,*) "pwscf and wannier90 k-points are compatible."
    RETURN
  END SUBROUTINE compatibility_check 

  
  SUBROUTINE seed_setup( nsp, seedarr )
    ! Load the up(/down) seednames into seedarr
    IMPLICIT NONE

    INTEGER, INTENT(IN)               :: nsp        ! Number of spin components
    CHARACTER(LEN=80), INTENT(OUT)    :: seedarr(2) ! wannier90 seedname array

    CHARACTER(LEN=512)                :: message    ! for errors
    INTEGER                           :: ios        ! I/O status
    
    CALL input_from_file()
    READ(5, inputpp, err=601, iostat=ios)

    ! Set seedname defaults
    IF (seedname == '' ) seedname = prefix
    IF (seednam_up == '') seednam_up = TRIM(seedname) // '_up'
    IF (seednam_dw == '') seednam_dw = TRIM(seedname) // '_dw'

    IF (nsp == 1) THEN
      seedarr(1) = seedname
      WRITE(6,*) "There are", nsp, " spin components."
      WRITE(6,'(5x,"seedname  : ",a)') seedname
    ELSE IF (nsp == 2) THEN
      seedarr(1) = seednam_up
      seedarr(2) = seednam_dw
      WRITE(6,*) "There are", nsp, " spin components."
      WRITE(6,'(5x,"spin-up seedname  : ",a)') seednam_up
      WRITE(6,'(5x,"spin-dw seedname  : ",a)') seednam_dw
    ELSE
      message = "ERR(seed_setup): Unsupported nspin"
      CALL errore('LOSC', message, nsp)
    END IF

    RETURN

    601 CALL errore('LOSC', 'seed_setup(): Error reading inputpp', ABS(ios))
  END SUBROUTINE seed_setup


  SUBROUTINE print_cell_ions_base(iprint)
    USE cell_base,                  ONLY : at, bg
    USE ions_base,                  ONLY : nat, tau

    IMPLICIT NONE

    ! Parameters
    INTEGER, INTENT(IN)                 :: iprint

    ! Locals
    INTEGER                             :: i

    IF (iprint > 1) THEN
      WRITE(6,'(/,4x,"Unit cell info:",/)')
      WRITE(6,'(3(5x," at(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i,at(1:3,i),i=1,3)
      WRITE(6,'(3(5x," bg(",i1,")=<",2(f7.4,x),f7.4,">",/))') (i,bg(1:3,i),i=1,3)
      DO i = 1, nat
          WRITE(6,'(5x,"tau(",i1,")=<",2(f7.4,x),f7.4,">")') i,tau(1:3,i)
      END DO; WRITE(6,*)
    END IF

    RETURN
  END SUBROUTINE print_cell_ions_base 


  SUBROUTINE write_eigenvalues(eigs, name)
    ! Write corrected eigenvalues to a file
    USE pw2losc,                ONLY : nks, nkstot
    USE wan2losc,               ONLY : nwan
    USE sys,                    ONLY : nksz, spin_offset

    IMPLICIT NONE

    CHARACTER(len=256), INTENT(IN)  :: name                 ! filename
    REAL(dp), INTENT(IN)            :: eigs(nwan,nks)       ! eigenvalue matrix
    INTEGER, EXTERNAL               :: find_free_unit
    INTEGER                         :: iunit, ios, ik, ib1

    iunit = find_free_unit()
    OPEN(unit=iunit, file=trim(name), action='write', form='formatted', &
         status='unknown', err=800, iostat=ios)

    DO ik = 1 + spin_offset, nksz + spin_offset
      DO ib1 = 1, nwan
        WRITE(iunit,'(1x,i4,1x,i4,f17.12)') ib1, ik, min(800.0_dp, eigs(ib1,ik))
      END DO
    END DO

    close(iunit)

    RETURN
    800  CALL errore( 'LOSC', 'ERR opening file '//trim(name), abs(iunit) )
  END SUBROUTINE write_eigenvalues 


  SUBROUTINE print_final(iprint)
    USE wan2losc,             ONLY : nwan
    USE sys,                  ONLY : pscshsz, pscsh, pscshoff
    USE curvature,            ONLY : kappam, kappa2, ovsqrt
    IMPLICIT NONE

    INTEGER, INTENT(IN)           :: iprint
    INTEGER                       :: isc1, i, j, i3
    write(6,'(/)') 

    if ( iprint.gt.1 ) then ! print more info
        do isc1 = 1, pscshsz
            if( isc1.eq.pscshoff ) write(6,1379) isc1, pscsh(1:3,isc1)
            if( isc1.ne.pscshoff ) write(6,1380) isc1, pscsh(1:3,isc1)

            write(6,'(/5x,"Occup Matrix(R): ")',advance='no')
            write(6,'("( j  i \lambda_ij |\lambda_ji| )")')
            do i = 1, nwan
                write(6,*) ! newline
                do j = 1, nwan
                    if ( i.eq.j ) then ! print * to indicate diagonal
                        write(6,1390) j, i, occupm(j,i,isc1), &
                                      abs(occupm(j,i,isc1))
                    else
                        write(6,1391) j, i, occupm(j,i,isc1), &
                                      abs(occupm(j,i,isc1))
                    end if
                end do
            end do

            if ( kapcalc ) then
                write(6,'(/5x,"Curvature Matrix ( j i \kappa_ji )")')
                do i = 1, nwan
                    write(6,*) ! newline
                    do j = 1, nwan
                        if ( i.eq.j ) then
                            write(6,1392) j, i, kappam(j,i,isc1)
                        else
                            write(6,1393) j, i, kappam(j,i,isc1)
                        end if
                    end do
                end do
                write(6,'(/)') ! newline

                write(6,'(/5x,"Curvature Matrix2 ( j i \kappa2_ji )")')
                do i = 1, nwan
                    write(6,*) ! newline
                    do j = 1, nwan
                        if ( i.eq.j ) then
                            write(6,1392) j, i, kappa2(j,i,isc1)
                        else
                            write(6,1393) j, i, kappa2(j,i,isc1)
                        end if
                    end do
                end do
                write(6,'(/)') ! newline

                write(6,'(/5x,"Overlap Sqrt Matrix ( j i \ovsqrt_ji )")')
                do i = 1, nwan
                    write(6,*) ! newline
                    do j = 1, nwan
                        if ( i.eq.j ) then
                            write(6,1392) j, i, ovsqrt(j,i,isc1)
                        else
                            write(6,1393) j, i, ovsqrt(j,i,isc1)
                        end if
                    end do
                end do
                write(6,'(/)') ! newline
            end if
        end do 
    end if 

    RETURN

    1379 format(/,3x,"* Output for cell ",i3,", R-0 = <",2(f8.3,1x),f8.3,">")
    1380 format(/,5x,"Output for cell ",i3,", R-0 = <",2(f8.3,1x),f8.3,">")
    1390 format(3x,'* ',2(i4,x),E18.10,SP,E18.10,"i",2x,'mag:',E18.10)
    1391 format(5x,2(i4,1x),E18.10,SP,E18.10,"i",2x,'mag:',E18.10)
    1392 format(3x,'* ',2(i4,1x),E18.10)
    1393 format(5x,2(i4,1x),E18.10)
  END SUBROUTINE print_final
 

  SUBROUTINE print_timing(lplot, lkappa)
    IMPLICIT NONE

    LOGICAL, INTENT(IN)       :: lplot, lkappa    ! plot/curvature flags
    write(6,'("=",a,"=")') repeat('-',76)
    write(6,'(/5x,"General subroutine run times")')
    call print_clock('calbec')
    call print_clock('fft')
    call print_clock('fftw')
    call print_clock('davcio')
    call print_clock('dwf_uc2lc')

    write(6,'(/5x,"Initialization subroutine run times")')
    call print_clock('init_lbo')
    call print_clock('pw2losc_read')
    call print_clock('pw2losc:LBO')
    call print_clock('pw2losc:SavD')
    call print_clock('read_unkgr')
    call print_clock('make_DWF')
    call print_clock('make_DWF1')
    call print_clock('make_DWF2')
    call print_clock('save_DWF')
    !call print_clock('pw2losc_open')

    !if ( lplot ) then
    !  write(6,'(/5x,"Plotting functions")')
    !  call print_clock('plot_bloch')
    !  call print_clock('plot_wan')
    !end if

    if ( kapcalc ) then
      write(6,'(/5x,"Curvature calculations")')
      call print_clock('dwf_save_g')
      call print_clock('dwf_shift_g')
      call print_clock('kjmetric')
      call print_clock('coulomb_screen')
      call print_clock('compute_kernel')
      call print_clock('wannier_density_fft')
      call print_clock('losc_load_dwf')
      call print_clock('kxmetric')
      call print_clock('ovsmetric')
    end if

    write(6,'(/5x,"Orbitalet density cleanup")')
    call print_clock('dwf_delete')

    write(6,'(/5x,"Total program time")',advance='no')

    RETURN
  END SUBROUTINE print_timing


  subroutine eigs_disentangle()
    ! Compute the eigenvalues of the disentangled Bloch orbitals:
    ! etd = U_dis * et
    USE kinds,      ONLY : DP
    USE pw2losc,    ONLY : et
    USE wan2losc,   ONLY : nwan, nbwan, nkwan, u_dis, ldis
    USE sys,        ONLY : etd, spin_offset

    IMPLICIT NONE

    ! Locals
    INTEGER :: ib1, ib2, ik

    IF (ALLOCATED(etd)) DEALLOCATE(etd)
    ALLOCATE( etd(nwan,nks) ) ! disentangled eigs

    if( ldis )then
      do ik = 1 + spin_offset, nkwan + spin_offset
          do ib1 = 1, nwan
              etd(ib1,ik) = 0.0_dp
              do ib2 = 1, nbwan 
                  etd(ib1,ik) = etd(ib1,ik) + &
                                ABS(u_dis(ib2,ib1,ik))**2 * et(ib2,ik)
              end do
          end do
      end do
    else
      etd(1 : nwan, 1 + spin_offset : nkwan + spin_offset) = &
       et(1 : nwan, 1 + spin_offset : nkwan + spin_offset)
    end if

  end subroutine eigs_disentangle


  subroutine losc_e_front(eigs, eh, el, ehi, eli)
  ! must be in an interface or contains to have optional(out) args
  ! UPDATE 04NOV21 (almahler): since LOSC may switch the ordering
  ! of the eigenvalues the egis array is now sorted before searching,
  ! note this effectively removes the usability of ehi and eli
    USE kinds,        ONLY : dp
    USE pw2losc,      ONLY : nks, nkstot, wk, wg
    USE wan2losc,     ONLY : nwan
    USE sys,          ONLY : nksz, spin_offset
    USE io_global,    ONLY : ionode

    implicit none
    ! Params
    real(dp), intent(in)  :: eigs(:,:)
    real(dp), intent(out) :: eh, el
    integer, optional :: ehi ! homo k-index
    integer, optional :: eli ! lumo k-index
    ! Locals
    real(dp), parameter   :: eps = 1.0e-4_dp
    real(dp), allocatable :: esort(:)
    integer               :: kbnd, ibnd, ik

    el = huge(1.0_dp)
    eh = -el
    if ( present(ehi) ) ehi = -1
    if ( present(eli) ) eli = -1

    allocate( esort(nwan) )

    do ik = 1 + spin_offset, nksz + spin_offset
        if ( abs(wk(ik)) > 1.e-10_dp ) then
            call sort_minloc( eigs(:nwan,ik), esort, nwan )
            kbnd = nwan
            dobndef: do ibnd = 1, nwan ! modify max occ to 0.5 for metals
                if ( (abs(wg(ibnd,ik)) / wk(ik)) < (0.5_dp - eps) ) then
                    kbnd = ibnd - 1
                    exit dobndef
                end if
            end do dobndef
            if ( kbnd > 0 ) then
                if ( esort(kbnd) .gt. eh ) then
                    eh = esort(kbnd)
                    if ( present(ehi) ) ehi = ik
                end if
            end if
            if ( kbnd < nwan ) then
                if ( esort(kbnd+1) .lt. el ) then
                    el = esort(kbnd+1)
                    if ( present(eli) ) eli = ik
                end if
            end if
        end if
    end do

    deallocate( esort )
    return
  end subroutine losc_e_front


  subroutine sort_minloc(ain, aout, isz)
      ! use minloc for easy sort implementation with O(N^2) complexity
      real(dp), intent(in)    :: ain(:)
      real(dp), intent(inout) :: aout(:)
      integer,  intent(in)    :: isz

      integer :: ii, imin
      real(dp) :: rtmp
      logical, allocatable :: amask(:)

      allocate( amask(isz) )
      amask = .true.
      aout(1:isz) = ain(1:isz)

      do ii = 1, isz
          imin = minloc(aout, dim=1, mask=amask)
          rtmp = aout(ii)
          aout(ii) = aout(imin)
          aout(imin) = rtmp
          amask(ii) = .false.
      end do

      deallocate( amask)
      return
  end subroutine sort_minloc


  subroutine losc_print_eband(eout, iprint)
  ! copy of print_ks_energies but uses sys et array
    USE constants,  ONLY : rytoev
    USE io_global,  ONLY : stdout
    USE pw2losc,    ONLY : xk
    USE wan2losc,   ONLY : nwan
    USE sys,        ONLY : nksz, spin_offset
    implicit none
    real(dp) :: eout(:,:)
    integer, optional, intent(in) :: iprint

    integer  :: i, ik, ibnd, ipl
    real(dp) :: ehomo, elumo

    ipl = 0
    if( present(iprint) )then
        ipl = iprint
    end if

    if( ipl.ge.10 )then
        do ik = 1 + spin_offset, nksz + spin_offset
            WRITE( stdout, 9020 ) ( xk(i,ik), i = 1, 3 )
            WRITE( stdout, 9030 ) ( eout(ibnd,ik) * rytoev, ibnd = 1, nwan )
        end do
    end if

    call losc_e_front(eout, ehomo, elumo)
    write(stdout,*) ! newline
    if( elumo < 1.0e6_dp )then
        WRITE( stdout, 9042 ) ehomo*rytoev, elumo*rytoev
    else
        WRITE( stdout, 9043 ) ehomo*rytoev
    end if

  9020 FORMAT(/'          k =',3F7.4,'     band energies (ev):'/ )
  9030 FORMAT( '  ',8F9.4 )
  9042 format('     highest occupied, lowest unoccupied level (eV): ',2F10.4 )
  9043 format('     highest occupied level (eV): ',F10.4 )
  end subroutine losc_print_eband


  subroutine print_efermi(eigs, ip)
    ! Print the Fermi energy
    USE constants,              ONLY : rytoev
    USE io_global,              ONLY : stdout
    USE klist,                  ONLY : lgauss, ltetra
    USE pw2losc,                ONLY : xk
    USE wan2losc,               ONLY : nwan
    USE sys,                    ONLY : nksz, spin_offset

    IMPLICIT NONE

    REAL(dp), INTENT(IN)            :: eigs(:,:)              ! Eigenvalues
    INTEGER, OPTIONAL, INTENT(IN)   :: ip                     ! Print level

    REAL(dp)                        :: ef                     ! Fermi energy
    REAL(dp)                        :: eh, el                 ! just in case
    INTEGER                         :: i, ik, ibnd, iplocal

    iplocal = 0
    IF (PRESENT(ip)) iplocal = iprint

    IF (iplocal .GE. 10) THEN
      DO ik = 1 + spin_offset, nksz + spin_offset
        WRITE(stdout, 9020)(xk(i,ik), i = 1,3)
        WRITE(stdout, 9030)(eigs(ibnd,ik) * rytoev, ibnd = 1, nwan)
      END DO
    END IF

    IF (lgauss) THEN
      CALL fermi_gauss(eigs, ef)
      WRITE(stdout, 9044) ef * rytoev
    ELSE IF (ltetra) THEN
      CALL fermi_tetra(eigs, ef)
      WRITE(stdout, 9044) ef * rytoev
    ELSE
      WRITE(stdout, *) "Unrecognized metal type! Computing VBM/CBM instead."
      CALL losc_e_front(eigs, eh, el)
      WRITE( stdout, 9042 ) eh*rytoev, el*rytoev
    END IF

    RETURN

  9020 FORMAT(/'          k =',3F7.4,'     band energies (ev):'/ )
  9030 FORMAT( '  ',8F9.4 )
  9042 format('     highest occupied, lowest unoccupied level (eV): ',2F10.4 )
  9044 FORMAT('     Fermi energy (eV): ',F10.4 )
  end subroutine print_efermi

 
  subroutine fermi_gauss(eigs, ef)
    ! Compute the Fermi energy for systems with smeared occupation.
    USE constants,          ONLY : rytoev
    USE klist,              ONLY : degauss, ngauss
    USE pw2losc,            ONLY : nbnd, nks, nkstot, nelec, wk, isk
    USE sys,                ONLY : nksz, spin_offset

    IMPLICIT NONE

    REAL(dp), EXTERNAL          :: efermig    ! Actually calculate ef

    REAL(dp), INTENT(IN)        :: eigs(nbnd, nks)
    REAL(dp), INTENT(OUT)       :: ef

    INTEGER                     :: is = 0     ! hardcode no spin for now
    !INTEGER                     :: isk(nks)

    !isk(:) = 1

    ef = efermig(eigs, nbnd, nks, nelec, wk, degauss, ngauss, is, isk)

    RETURN
  end subroutine fermi_gauss


  subroutine fermi_tetra(eigs, ef)
    ! Compute the Fermi energy for systems with smeared occupation.
    USE constants,          ONLY : rytoev
    USE ktetra,             ONLY : ntetra, tetra
    USE pw2losc,            ONLY : nspin, isk, nbnd, wk, nelec, nks, nkstot

    IMPLICIT NONE

    REAL(dp), EXTERNAL          :: efermit    ! Actually calculate ef

    REAL(dp), INTENT(IN)        :: eigs(nbnd, nks)
    REAL(dp), INTENT(OUT)       :: ef

    ! Hardcode no spin for now
    INTEGER                     :: is = 0
    !INTEGER                     :: nspin = 1
    !INTEGER                     :: isk(nks)

    !isk(:) = 1

    ef = efermit(eigs, nbnd, nks, nelec, nspin, ntetra, tetra, is, isk)

    RETURN
  end subroutine fermi_tetra


  subroutine losc_calc_de(kmat, omat, umat, deig, dE)
    ! Calculate the LOSC energy corrections due to curvature kmat
      USE constants,  ONLY : tpi
      USE pw2losc,    ONLY : xk, nks
      USE wan2losc,   ONLY : nwan
      USE sys,        ONLY : nksz, spin_offset, pscshsz, pscshoff, pscsh
      ! Params
      real(dp),    intent(in)    :: kmat(:,:,:) ! curvature of WFs
      complex(dp), intent(in)    :: omat(:,:,:) ! occupation of WFs
      complex(dp), intent(in)    :: umat(:,:,:) ! unitary transforms of BOs
      real(dp),    intent(inout) :: deig(:,:)   ! change in eigenvalues
      real(dp),    intent(out)   :: dE          ! total energy correction
      ! Locals
      integer     :: ik, ibnd, isc1
      integer     :: i, j
      real(dp)    :: r1
      complex(dp) :: eikr

      ! Calculate eigenvalue corrections
      doKptDe: do ik = 1 + spin_offset, nksz + spin_offset
      doBndDe: do ibnd = 1, nwan

          deig(ibnd,ik) = 0.0_dp

          do isc1 = 1, pscshsz ! all non \delta^{(0,R)}_{i,j} terms
              do j = 1, nwan     
              do i = 1, nwan

                  r1 = tpi * sum( xk(1:3,ik) * pscsh(1:3,isc1) )
                  eikr = cmplx(cos(r1), sin(r1))

                  r1 = kmat(i,j,isc1) * real( eikr * omat(i,j,isc1) * &
                      umat(ibnd,i,ik) * conjg(umat(ibnd,j,ik)), kind=dp )

                  deig(ibnd,ik) = deig(ibnd,ik) - r1

              end do
              end do
          end do

          do i = 1, nwan ! diagonal \delta terms
              r1 = 0.5_dp * kmat(i,i,pscshoff) * abs(umat(ibnd,i,ik))**2
              deig(ibnd,ik) = deig(ibnd,ik) + r1
          end do

          deig(ibnd,ik) = deig(ibnd,ik) * 2.0_dp ! Hartree to Rydberg

      end do doBndDe
      end do doKptDe

      ! Calculate total energy correction
      dE = 0.0_dp
      do isc1 = 1, pscshsz ! all non \delta^{(0,R)}_{i,j} terms

          do j = 1, nwan     
          do i = 1, nwan
              r1 = kmat(i,j,isc1) * abs(omat(i,j,isc1))**2
              dE = dE - r1
          end do
          end do

      end do

      do i = 1, nwan      ! diagonal \delta terms
          r1 = real( omat(i,i,pscshoff), kind=dp)
          dE = dE + ( kmat(i,i,pscshoff) * r1 )
      end do

      dE = dE * 2.0_dp ! Hartree to Rydberg

      return
  end subroutine losc_calc_de

END PROGRAM losc