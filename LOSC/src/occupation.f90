MODULE occupation
	! Methods for computing the local occupations between LOSC orbitalets

	USE kinds,			ONLY : dp

	IMPLICIT NONE
	SAVE

  INTEGER,     ALLOCATABLE :: limdei(:,:,:,:)    ! limdeb index
	REAL(dp),    ALLOCATABLE :: limdeb(:,:,:)      ! kappam cutoff for de band
  REAL(dp),    ALLOCATABLE :: limdet(:,:,:)      ! kappam cutoff for dE total
  COMPLEX(dp), ALLOCATABLE :: occupk(:,:,:)      ! occupation matrix for k's
  COMPLEX(dp), ALLOCATABLE :: occupm(:,:,:)      ! occupation matrix
	REAL(dp),		 ALLOCATABLE :: nocc(:)						 ! canonical occupation

	CONTAINS

	SUBROUTINE init_occupation(nwan, nuc, nks)
		USE sys,								ONLY : limde

		IMPLICIT NONE

		INTEGER, INTENT(IN)					:: nwan, nuc, nks

		ALLOCATE(limdei(2, nwan, nwan, nuc))
		ALLOCATE(limdeb(nwan, nwan, nuc))
		ALLOCATE(limdet(nwan, nwan, nuc))
		ALLOCATE(limde(nwan, nwan, nuc))
		ALLOCATE(occupk(nwan, nwan, nks))
		ALLOCATE(occupm(nwan, nwan, nuc))
		ALLOCATE(nocc(nks))

		RETURN
	END SUBROUTINE init_occupation


	SUBROUTINE dealloc_occupation()
		USE sys,							ONLY : limde

		IMPLICIT NONE

		DEALLOCATE(limdeb)
		DEALLOCATE(limdei)
		DEALLOCATE(limdet)
		DEALLOCATE(limde)
		DEALLOCATE(occupk)
		DEALLOCATE(occupm)
		DEALLOCATE(nocc)

		RETURN
	END SUBROUTINE dealloc_occupation


	SUBROUTINE local_occupation_r()
    USE constants,            ONLY : tpi
		USE pw2losc,							ONLY : nks, xk
		USE wan2losc,							ONLY : nwan
    USE sys,                  ONLY : pscsh, pscshsz, nksz, spin_offset
		IMPLICIT NONE

    INTEGER                       :: isc1, ib1, ib2, ik
    COMPLEX(dp)                   :: c1, eikrmt
    REAL(dp)                      :: r1

    do isc1 = 1, pscshsz
      do ib1 = 1, nwan
      do ib2 = 1, nwan
        c1 = cmplx(0.0,kind=dp)
        do ik = 1 + spin_offset, nksz + spin_offset
            r1 = -tpi * sum( xk(1:3,ik) * pscsh(1:3,isc1) )
            eikrmt = cmplx(cos(r1), sin(r1),kind=dp)
            c1 = c1 + eikrmt*occupk(ib2,ib1,ik)
        end do
        occupm(ib2,ib1,isc1) = c1 / cmplx(nksz,kind=dp)
        end do
      end do
    end do
		RETURN
	END SUBROUTINE local_occupation_r

	SUBROUTINE curvature_cutoffs()
    USE constants,              ONLY : tpi
		USE pw2losc,								ONLY : nks, xk
		USE wan2losc,								ONLY : nwan, u_wan
    USE sys,                    ONLY : limde
		USE sys,										ONLY : nksz, spin_offset
    USE sys,                    ONLY : pscshsz, pscshoff, pscsh
		IMPLICIT NONE

    INTEGER                         :: isc1, ib1, ib2, ik, i1
    COMPLEX(dp)                     :: eikrmt
    REAL(dp)                        :: r1, r2, rtmp

    do isc1 = 1, pscshsz
      do ib1 = 1, nwan
        do ib2 = 1, nwan

        ! find max limiting factor for kappam(i,j,isc)
        ! dE : conjg(occupm(i,j,isc)) * 
        !      ( delta^(0,isc)_(i,j) - occupm(i,j,isc) )
        ! de_m,k : Re{ occupm(i,j,isc) * exp(ik.R) * u_wan(m,i,k) *
        !              conjg(u_wan(m,j,k) }

        ! find de band cutoff (max over all bands & k-points)
        rtmp = 0.0_dp
        do ik = 1 + spin_offset, nksz + spin_offset

            do i1 = 1, nwan

                if ( (ib1.eq.ib2) .and. (isc1.eq.pscshoff) ) then
                    r2 = abs( u_wan(i1,ib1,ik)**2 ) * &
                         ( 0.5_dp - occupm(ib1,ib1,isc1) )
                else
                    r1 = tpi * sum( xk(1:3,ik) * pscsh(1:3,isc1) )
                    eikrmt = cmplx(cos(r1), sin(r1))
                    r2 = real( eikrmt * occupm(ib2,ib1,isc1) * &
                               u_wan(i1,ib2,ik) * conjg(u_wan(i1,ib1,ik)), dp)
                end if
                if ( abs(r2) .ge. abs(rtmp) ) then ! must be >= for 0.0 case
                    rtmp = r2
                    limdeb(ib2,ib1,isc1) = abs(r2)
                    limdei(1,ib2,ib1,isc1) = i1
                    limdei(2,ib2,ib1,isc1) = ik
                end if
            end do
          end do

        ! find dE total cutoff
        if ( (ib1.eq.ib2) .and. (isc1.eq.pscshoff) ) then
            r1 = real(occupm(ib1,ib1,isc1))
            r2 = r1 * ( 1.0_dp - r1 )
        else
            r2 = abs( occupm(ib2,ib1,isc1)**2 )
        end if
        limdet(ib2,ib1,isc1) = r2

        ! set limde as the largest of all
        if ( abs(limdet(ib2,ib1,isc1)) .ge. abs(limdeb(ib2,ib1,isc1)) ) then
            ! limdet largest, >= ensures det preference over deb when equivalent
            limde(ib2,ib1,isc1) = abs(limdet(ib2,ib1,isc1))
            limdei(1:2,ib2,ib1,isc1) = 0
        else !limdeb largest
            limde(ib2,ib1,isc1) = abs(limdeb(ib2,ib1,isc1))
        end if

        end do
      end do
    end do

		RETURN
	END SUBROUTINE curvature_cutoffs


	SUBROUTINE canonical_occupation_k()
		USE pw2losc,						ONLY : nbnd, wg, wk
    USE sys,                ONLY : nksz, spin_offset

		IMPLICIT NONE

    INTEGER                     :: ik, ibnd
    REAL(dp)                    :: rtmp

		do ik = 1 + spin_offset, nksz + spin_offset ! calculate occupations per k-point
			nocc(ik) = 0.0_dp      
			do ibnd = 1, nbnd
					rtmp = abs(wg(ibnd,ik) / wk(ik) )
					nocc(ik) = nocc(ik) + rtmp
!        if ( abs( 1.0_dp - rtmp ).lt.1.0e-8_dp ) then
!            nocc(ik) = nocc(ik) + 1
!        else if ( ionode .and. (rtmp.gt.1.0e-8_dp) ) then
!            write(6,'("ERR(losc): unsupported occupancy @ k=(",&
!                    f7.3,x,f7.3,x,f7.3,"), band: ", i3,"::", f8.4)')&
!                    xkw(:), ibnd, rtmp
!            call errore('LOSC', 'unsupported occupancy', 1)
!        end if
			end do
		end do 

	RETURN
	END SUBROUTINE canonical_occupation_k

	SUBROUTINE local_occupation_k(occupk, nocc, iprint)
		USE io_global,					ONLY : ionode
		USE pw2losc,						ONLY : nks, nkstot, wg, xk, wk
		USE wan2losc,						ONLY : nwan, u_loc
		USE sys,								ONLY : nksz, spin_offset, xkg
		! 2022-04-28: xkg was read from wannier90, now we use xk from pwscf

		IMPLICIT NONE

		COMPLEX(dp), INTENT(INOUT)	:: occupk(nwan, nwan, nks)	! TBO occupation (k)
		REAL(dp), INTENT(IN)				:: nocc(nks)								! CBO occupation (k)
		INTEGER, INTENT(IN)					:: iprint										! printing flag

		REAL(dp)										:: rtmp
		INTEGER											:: ik, ib1, ib2, ibnd
		do ik = 1 + spin_offset, nksz + spin_offset

			do ib1 = 1, nwan         ! find local occpuation from overlaps
					occupk(:,ib1,ik) = cmplx(0.0_dp)
					do ib2 = 1, nwan !nocc(ik)
							do ibnd = 1, nwan 
									rtmp = abs( wg(ibnd,ik) / wk(ik) ) ! abs?
									occupk(ib2,ib1,ik) = occupk(ib2,ib1,ik) + cmplx(rtmp,kind=dp) * &
																			conjg(u_loc(ibnd,ib2,ik)) * u_loc(ibnd,ib1,ik)
					!do ibnd = 1, nocc(ik)
					!    do ib2 = 1, nwan !ib1, nwan
					!        occupk(ib2,ib1,ik) = occupk(ib2,ib1,ik) + &
					!                             conjg(u_loc(ibnd,ib2,ik)) * u_loc(ibnd,ib1,ik)
							end do
					end do
			end do
			
			if ( (iprint.ge.10) .and. ionode ) then

					write(6,'(/5x,"Output for k-point",i3," (",f8.3,","f8.3,",",&
										f8.3,"), nocc =",f8.3)') ik, xkg(:,ik), nocc(ik)
					write(6,'(/5x,"Occup Matrix(k): ( j  i \lambda_ij |\lambda_ij| )")')
					do ib1 = 1, nwan
							write(6,*) ! newline
							do ib2 = 1, nwan
									if ( ib1.eq.ib2 ) then ! print * to indicate diagonal
											write(6,1390) ib2, ib1, occupk(ib2,ib1,ik), abs(occupk(ib2,ib1,ik))
									else
											write(6,1391) ib2, ib1, occupk(ib2,ib1,ik), abs(occupk(ib2,ib1,ik))
									end if
							end do
					end do
					write(6,*) ! newline

			else if ( (iprint.ge.11) .and. ionode ) then

					write(6,'(5x,"Occup Marix diags: ( i \lambda_ii |\lambda_ii| ) ")')
					do ib2 = 1, nwan
							write(6,'(5x,i2,x,f11.7,sp,f11.7,"i",ss,f12.8)') &
											ib2, occupk(ib2,ib2,ik), abs(occupk(ib2,ib2,ik))
					end do

			end if

		end do
		
		RETURN

		1390 format(3x,'* ',2(i4,x),E18.10,SP,E18.10,"i",2x,'mag:',E18.10)
		1391 format(5x,2(i4,1x),E18.10,SP,E18.10,"i",2x,'mag:',E18.10)
	END SUBROUTINE local_occupation_k 


	function losc_local_check( ib1, nibox ) result(ret)
		! Localization check by integrating half the unfolded BvK supercell
		! in each dimension, so 1/8th in 3D
		! can override integration limit with nibox
		use wan2losc, only: lwfcen
		use sys, only: nkdm, pscshoff
		use overlap,  only: overlap_pow_v11
		! Params
		integer,            intent(in) :: ib1
		real(dp), optional, intent(in) :: nibox(3) ! numerical integration limits
																							! in unit cell units
		! Locals
		real(dp) :: ret

		if( present( nibox) )then
				ret = overlap_pow_v11( ib1, ib1, lwfcen(:,ib1), lwfcen(:,ib1), &
														pscshoff, 0.5_dp, nibox )
		else
				ret = overlap_pow_v11( ib1, ib1, lwfcen(:,ib1), lwfcen(:,ib1), &
														pscshoff, 0.5_dp, 0.5_dp*real(nkdm,dp) )
		end if

		return 
	end function losc_local_check


	SUBROUTINE check_k_mesh(seedname)
		USE pw2losc,								ONLY : xk, nks
		USE sys,                    ONLY : nksz, spin_offset
		USE utils,                  ONLY : losc_cart2frac_rec, neq_3r

		IMPLICIT NONE

  	REAL(dp), EXTERNAL 							:: get_clock
		CHARACTER(len=80), INTENT(IN)   :: seedname     ! from wannier90
		character(len=512) 							:: charbuff   	! buffer
		REAL(dp)                        :: xkw(3)
		INTEGER                         :: ik

		DO ik = 1 + spin_offset, nksz + spin_offset !  check pwscf matching k-vec
			xkw = losc_cart2frac_rec(xk(:,ik))
			IF ( neq_3r( xkw, xk(:,ik) ) ) THEN
					WRITE(6,'("ERR(occupation::check_k_mesh): mismatch @ k-point ",i5)') ik
					WRITE(6,'(2x,"wan: ", 3(g20.10,x))') xk(:,ik)
					WRITE(6,'(2x,"q-e: ", 3(g20.10,x))') xkw
					charbuff = 'reading input from '//trim(seedname)//'_u.mat, k-point mismatch'
					CALL errore('LOSC', charbuff, 1)
			END IF
		END DO
    
		WRITE(6, 9000 ) get_clock( 'LOSC' )

		RETURN

		9000 FORMAT(' total cpu time is ',F10.1,' secs' )
	END SUBROUTINE check_k_mesh


	SUBROUTINE occupation_print(seedname, iprint)
		USE constants,							ONLY : ANGSTROM_AU
		USE wan2losc,								ONLY : lwfcen, nwan
		USE wannier_density,        ONLY : losc_alloc_dwf, losc_dealloc_dwf, &
																			 exp_pos_pbc, exp_var_pbc

		IMPLICIT NONE

		CHARACTER(len=80), INTENT(IN)   :: seedname     ! from wannier90
		INTEGER, INTENT(IN)             :: iprint       ! print level
		REAL(dp)                        :: rsc1(3), rsc2(3)
		INTEGER                         :: iloc, iwan
		REAL(dp)                        :: r1, r2

		call losc_alloc_dwf(1) ! alloc for 1 LWF
		write(6,'(/,1x,a)') "Localization check: "
		write(6,'(/,3x,a)') "iw, (\int_SC/2 dx)^3,       (\int_UC dx)^3"
		rsc1(1:3) = 1.0_dp
		iloc = 0
		do iwan = 1, nwan
			r1 = losc_local_check( iwan )
			r2 = losc_local_check( iwan, rsc1 )
			write(6,'(2x,i3,2(2x,g20.12))') iwan, r1, r2
			if( abs(1.0_dp-r1) .gt. 0.5e-02_dp ) iloc = iloc + 1
		end do
		if( iloc.gt.0 )then
			write(6,'(1x,a,i0,a)') 'WARN: ', iloc,' LWFs are not well localized!'
		end if
		write(6,*)

		if( iprint.ge.1 )then
			write(6,'(1x,a)') "Position check: (alat units)"
			write(6,'(2x,a3,3(2x,a10),2x,3(2x,a10))') "iw","<r_x>","<r_y>","<r_z>", &
																									"<WF_x>","<WF_y>","<WF_z>"
			do iwan = 1, nwan
					rsc1 = exp_pos_pbc(iwan)
					write(6,'(2x,i3,3(2x,f10.6),2x,3(2x,f10.6))') iwan, rsc1, lwfcen(:,iwan)
			end do

			!write(6,'(/,1x,a,f15.12)') "ANGSTROM_AU = ", ANGSTROM_AU
			write(6,'(/,1x,a)') "Spatial variance check: (Ang^2 units)"
			write(6,'(2x,a3,4(2x,a15))') "iw","<r_x^2>","<r_y^2>","<r_z^2>","<r^2>"
			do iwan = 1, nwan
					rsc2 = exp_var_pbc(iwan)/(ANGSTROM_AU**2)
					write(6,'(2x,i3,4(2x,f15.8))') iwan, rsc2, sum(rsc2)
			end do
			write(6,*)
		end if

		CALL losc_dealloc_dwf

		RETURN
	END SUBROUTINE occupation_print 


	SUBROUTINE print_diagonal_occupation(isc1)
		USE wan2losc,							ONLY : nwan
		USE sys,									ONLY : pscshoff, pscsh

		IMPLICIT NONE

		INTEGER, INTENT(IN)				:: isc1						! Born-von Karman cell index
		INTEGER										:: i

		if( isc1.eq.pscshoff ) write(6,1379) isc1, pscsh(1:3,isc1)
		if( isc1.ne.pscshoff ) write(6,1380) isc1, pscsh(1:3,isc1)
		write(6,'(5x,"Diagonal occupations:")',advance='no')
		write(6,1397) ( real(occupm(i,i,isc1)), i=1,nwan )
		write(6,1394) "iband1", "iband0", "KappaJ", "KappaX", &
									"limde", "de_m", "de_k", ">SC/2"
		RETURN
		1379 format(/,3x,"* Output for cell ",i3,", R-0 = <",2(f8.3,1x),f8.3,">")
		1380 format(/,5x,"Output for cell ",i3,", R-0 = <",2(f8.3,1x),f8.3,">")
		1394 format(5x,a6,1x,a6,4x,2(a6,14x),a6,4x,3(1x,a))
		1397 format( '     ',6g14.5 )
	END SUBROUTINE print_diagonal_occupation

END MODULE occupation