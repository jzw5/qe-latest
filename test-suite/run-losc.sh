#!/bin/bash
#
# Copyright (C) 2001 Quantum ESPRESSO
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License. See the file `License' in the root directory
# of the present distribution.
shopt -s extglob

if [ $QE_USE_MPI == 1 ]; then
  export PARA_PREFIX="mpirun -np ${TESTCODE_NPROCS}"
  export PARA_POSTFIX=" "
else
  unset PARA_PREFIX
  unset PARA_POSTFIX
fi

if test -f setmeup.sh; then

    ompnum=$(bash setmeup.sh "$@")
    echo "setmeup output: '$ompnum'" >>setmeup.out
    case $ompnum in [1-9]*([0-9]))
	    export OMP_NUM_THREADS=$ompnum
        var1=$(grep -c 'model name' /proc/cpuinfo)
	    if [ $ompnum -gt $var1 ]; then echo "WARN! CPU only has $var1 threads"; fi;;
    esac

else
 
    ${ESPRESSO_ROOT}/bin/losc.x -in $1 >$2 2>$3

fi

if test -f cleanup.sh; then
    bash cleanup.sh "$@"
fi

rm -f input_tmp.in

# Pre-running PW now; only LOSC executable needed
#if test -f setmeup.sh; then
#
#    ompnum=$(bash setmeup.sh "$@")
#    echo "setmeup output: '$ompnum'" >>setmeup.out
#    case $ompnum in [1-9]*([0-9]))
#	export OMP_NUM_THREADS=$ompnum
#	var1=$(grep -c 'model name' /proc/cpuinfo)
#	if [ $ompnum -gt $var1 ]; then echo "WARN! CPU only has $var1 threads"; fi;;
#    esac
#
#
#else
#
#    # hard-code serial for now
#    ${ESPRESSO_ROOT}/bin/pw.x -in scf.in >scf1.out 2>err
#    ${ESPRESSO_ROOT}/bin/pw.x -in nscf.in >nscf1.out 2>>err
#    cp inp.wan wan.win # this is so cleaning all wan* ouput is easy without removing input file
#    ${ESPRESSO_ROOT}/bin/wannier90.x -pp wan
#    ${ESPRESSO_ROOT}/bin/pw2wannier90.x -in pw2wan.in >pw2wan.out 2>>err
#    ${ESPRESSO_ROOT}/bin/wannier90.x wan
#    if test -f lsave.in; then
#	${ESPRESSO_ROOT}/bin/losc.x -in lsave.in >lsave.out 2>>err
#    fi
#
#fi

#(EOF)
